const util = require('util');
const entities = require("../lib/entities");

const testString = 'September Management Accounting Practices 3120, Cash Flow Management. September 10, 2007';
let result = entities.get_grouped(testString, ['Date']);
console.log('get_grouped:\n', util.inspect(result, {depth: 3}));

result = entities.get(testString, ['Date']);
console.log('get:\n', util.inspect(result, {depth: 3}));

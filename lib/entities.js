const path = require('path');
const glob = require('glob');

const EntityGrouping = require('../helpers/entity-grouping');

// Entities
let entities_js = glob.sync(path.join(__dirname, '../entities/*.js'))
const EntityTypes = entities_js.map(e => require(e));

/**
 * 
 * @param {String or piece of text that will be parsed} text 
 * @param {Types of entities you want to detect (default is all types)} types 
 */

function get(text, types, custom) {
    const potentials = [];

    types = (types) ? types.map(requestedType => EntityTypes.find(type => type.type_name() === requestedType)).filter(type => type) : EntityTypes;

    types.forEach(entityType => {
        if (entityType.type_name() === 'Custom' && custom)
            potentials.push(...entityType.getPotentialsForText(text, custom));
        else
            potentials.push(...entityType.getPotentialsForText(text));
    });

    return potentials.map(item => item.export());
}

/**
 * 
 * @param {String or piece of text that will be parsed} text 
 * @param {Types of entities you want to detect (default is all types)} types 
 */

function get_grouped(text, types, custom) {
    let potentials = get(text, types, custom);
    // ENTITY SORT DIAGRAM
    // *-----A------*
    // *---B----*
    // *--C--*
    //    *------D----------*
    //    *---E--*
    //        *--F--*
    //                 *---G---*
    //                 *-H-*
    // YEILDS
    // (ABCEF),(DH),(G)
    potentials = potentials.sort(function (item1, item2) {
        if (item1.firstIndex === item2.firstIndex) {
            return item1.lastIndex > item2.lastIndex ? -1 : item1.lastIndex < item2.lastIndex ? 1 : 0;
        }
        return item1.firstIndex - item2.firstIndex;
    });

    // Merge Overlapping potentials
    var groupings = [];
    var arrayMap = [];
    var currentGrouping;
    // For each index position
    for (var i = 0; i < potentials.length; i++) {
        // IF Item has not been added to grouping yet
        if (!arrayMap[i]) {
            // START NEW GROUP WITH THIS ENTITY
            var leadItem = potentials[i];
            currentGrouping = new EntityGrouping(leadItem);
            // CLAIM THIS INDEX POSTION IN GROUPING INDEX
            arrayMap[i] = true;
            // Find intersections For each index position greater than this
            for (var j = i + 1; j < potentials.length; j++) {
                var tailItem = potentials[j];
                // IF tailItem at this index has a start before the leadItem end
                // *-----A------*
                // *-----B------*
                //    *------C----------*
                //    *---D--*
                //                 *---E--*
                if (leadItem.lastIndex > tailItem.firstIndex) {
                    // IF tailItem has an END postion before or equal to the leadItem end
                    if (tailItem.lastIndex <= leadItem.lastIndex) {
                        // *-----A------*
                        // *-----B------*
                        //    *---D--*
                        // ADD THIS TO THE CURRENT GROUPING
                        currentGrouping.add(tailItem);
                        // CLAIM THIS INDEX POSTION IN GROUPING INDEX
                        arrayMap[j] = true;
                    } else {
                        // *-----A------*
                        //    *------C----------*
                        // CLAIM THIS INDEX POSTION AS FALSE IN GROUPING INDEX
                        arrayMap[j] = false;
                    }
                } else {
                    // We don't need to go farther down this path
                    // *-----A------*
                    //                 *---E--*
                    break;
                }
            }
            // END FOR
            // Collect this current grouping
            groupings.push(currentGrouping.export());
        } else {
            // Continue to next index position
        }
    }
    // Return groupings array
    return groupings;
}

function version() {
    return require(path.join(__dirname, '../package.json')).version
}

function getTypes() {
    return EntityTypes.map(type => type.type_name())
}

/**
 * 	
 * Export
 */
module.exports = {
    get: get,
    get_grouped: get_grouped,
    version: version,
    getTypes: getTypes,
};
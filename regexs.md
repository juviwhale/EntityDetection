# DATES REGEX'S

#######  
//# Month, optional Day, optional Year, optional time => March 25 2015 | April 10, 2008 | April 06, 2016 | April 1, 1976 7:30 am  
//# https://regex101.com/r/8z4wK9/10
```
/(?:^|[^\S\r\n])(?:\b(?:Mon|Tue(s)?|Wed(nes)?|Thu(rs)?|Fri|Sat(ur)?|Sun)(?:day)?\b)?[ ,]{0,2}(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)[\:\-,\/\ ]{1,3}(\d{1,2})?\ ?(?:st|nd|rd|th)?[- ,\/]{0,3}(\d{2,4})?[ ]?(((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?)\ ?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)?[,.]?(?!\S)/gim
```

```
match: 
Aug-03-2016
March 25 2015
April 10, 2008
April 06, 2016
Apr 06, 2016
full date and time: April 6, 1972 7:30 am
date: April 6, 1972
optional date: April 2002
optional am/pm: april 1, 1976 7:30 am
April 1, 1976 7:30
April 1976 7:00:57 AM
April 1976 7:00:57AM
April 1976 7:00:57A.M.
April 1976 7:00:57 A.M.
June 15, 2009
optional year:
June 15
Jun 16
jul 20

june 1st
june 2nd
june 3rd
june 15 th
April 1st, 1976 7:30
5th July 5th

April: 1st, 1976 7:30

Aug/2006
Jun/16
June/29/2008
June - 29 - 2008
June 29 2008
Tue June, 2008
Tues June, 2008
Tues June 1st, 2008

optional date:
June 2015
June, 2002
Monday, June 15, 2009
Monday, June 15
Mon, June 15
optional comma/period ending:
Monday, June 15.
Monday, June 15,
Tue Nov 04 20:14:00
Tue Nov 04 20:14:00

will match incorrect values too:
April 35, 1999


```



#######  
//# Day/month name/ year (optinal)/ time => 3-Aug-06 | 3-August-2006 | 11 April, 1999 | 3-August-2006 6:55:30 pm | 15 Mar | 8 apr | 15th of august
//# https://regex101.com/r/ZS3BGj/6
```
/(?:^|[^\S\r\n])((?:[0]?[0-9]|[12][0-9]|3[01])\ ?(?:st|nd|rd|th)?)?[-, ]{0,3}(?:of )?(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)[\:\-,\ ]{0,2}(\d{2,4})?[ ]?(((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?)\ ?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)?[,.]?(?!\S)/gim
```

```
match:
3-Aug-06
03-Aug-06
6 Apr, 1977
3-August-2006

3-August-2006 6:55:30 pm
6 Apr, 1977 6:55:30 pm
3-August-2006 6:55:30 pm

15th of august
1st of april, 2017
3rd of jan, 2018

Jan 
Dec
Dec:
Dec: 150,000 x 50%
1st of april: 2017
5th July: 5th

5th July 5th

what happens with 2 dates in 1 sentence?
15 december 2018 adsfdsa 15 december 2028

just month: 
15 Mar
8 apr
08 - apr
15 march

not match:
invalid day 31+:
35 April, 1999
77th of feb, 2019
```

#######  
//# time => 6:55:30 pm | 6:55 | 6:55 am
//# https://regex101.com/r/0r0ukg/4
```
/(?:^|[^\S\r\n])(\ ?((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?)\s?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)(?![\b])/gmi
```
```
6:55:30 pm
6:55
6:55 am
6:55 AM
6:55AM
6:55 A.M. 
the time is 6:55 A.M. def

6:55.


6:55 am,
6:55 AM,
```


#######  
//# day/month/year (optional) => 10/09/2017 | 10-09-2017 | 10/9/2017 | 10/9/17 | 10 - 9 - 17
//# https://regex101.com/r/0bUssw/3
```
/(?:^|[^\S\r\n])([0-9]?[0-9])[\.\-\/ ]{1,3}([0-3]?[0-9])([\.\-\/ ]{1,3}[0-9]{2,4}[,.]?)?(?!\S)/gim
```

```
match:
10/09/2017
10-09-2017
10/9/2017
10/9/17
10 - 9 - 17
05 - 19
10 9 2227
5-5-05
5/5/0005
10/09/2017,
10-09-2017,
10/9/2017,
10/9/17,

should not match:
99/11/111122
5/5/5
```

#######  
//# month/year => 04 / 2012 | 04/2012 | 9 / 2022  
//# https://regex101.com/r/2FpKcM/5
```
/(?:^|[^\S\r\n])((0?[1-9]|1[0-2])([\.\-\/ ]{1,3}[0-9]{2,4}))(?!\S)/gim
```
```
should match:
04 / 2012
04/2012
9 / 2022
11 / 2012
12 - 2012

shoul not match:
72/2012
22/2012
```

#######  
//# year >=1900 <=2199  
//# https://regex101.com/r/ELcgqp/13
```
/(?:^|[^\S\r\n])(?:19|2[01])\d{2}?\b/gm
```
```
2088
1992,

   1992 

1992, 1993 1992 -  1993.

Should match:
aaa 1994 3434 312312 2014 4944 aaa
1991
1924
1900
1901

1992,abc
1992 def

Since 1997. 
The meeting date is set! Since 1992, Add it to you calendar
11 / 2012
## 2199 ##

Not Match:
1992and
1901def
19993
1
12
123
1234
12345
123456
19111
1838
383811938423
12/32/2017
### 12/32/2017 ###

```


#######  
//# timestamp => 2013-04-05 17:59:59 | 2009-06-15T13:45:30  
//# https://regex101.com/r/qzIlH9/3
```
/(^|[^\S\r\n])((((19|20)([2468][048]|[13579][26]|0[48])|2000)-02-29|((19|20)[0-9]{2}-(0[4678]|1[02])-(0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}-(0[1359]|11)-(0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}-02-(0[1-9]|1[0-9]|2[0-8])))[\sT]([01][0-9]|2[0-3]):([012345][0-9]):([012345][0-9]))\b/gm
```
```
match:
2013-04-05 17:59:59,
2009-06-15T13:45:30


2013-04-05 17:59:59.
  2013-07-30 01:22:42 123
1915-02-28 00:00:00	

not match:
2099-12-30 23:59:59abc
ee2009-06-15T13:45:30
2009-06-15T13:45:3033
```

Currency:
```
/(?:^|[^\S\r\n])(?:\(?-?[^\S\r\n]*\d+\b(?:[,]\d{3})*[.]\d{2}\b\)?|\(?-?[^\S\r\n]*\d+\b(?:[.]\d{3})*[,]\d{2}\b\)?|\([^\S\r\n]*(?:[$€£]|\d+(?:\.\d+)+,|\d+(?:,\d+)+\.)(?:[^\S\r\n]*)?\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?[^\S\r\n]*\)|\([^\S\r\n]*(?:[$€£]|\d+(?:\.\d+)+,|\d+(?:,\d+)+\.)(?:[^\S\r\n]*)?\d{1,3}\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?[^\S\r\n]*\)|[$€£][^\S\r\n]*-?[^\S\r\n]*\d+\b(?:[,]\d{3}\b)*[.]\d{2}\b|[$€£][^\S\r\n]*-?[^\S\r\n]*\d+\b(?:[.]\d{3}\b)*[,]\d{2}\b|[^\S\r\n]*[$€£][^\S\r\n]*-?\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?|[^\S\r\n]*-?[$€£][^\S\r\n]*\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?|-?[^\S\r\n]*[$€£][^\S\r\n]*\d+\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?|-?[^\S\r\n]*\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?[^\S\r\n]*[€¢£]|-?[^\S\r\n]*\d+\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?[^\S\r\n]*[€¢£]|-?[^\S\r\n]*\d+(?:[.]\d{3})+|(?:-?[^\S\r\n]*\(?\d{1,3}\b(?:[,]\d{3}\b)+(?:[.]\d{2}\b)?\)?))(?![\d$€¢£()%\/-]|[$€¢£.,()]\d)/gm;
```
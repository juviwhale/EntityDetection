const validIndividualTaxpayerIdentificationNumberTests = {
    title: "IndividualTaxpayerIdentificationNumber valid cases",
    items: [{
        description: " - should find and validate a IndividualTaxpayerIdentificationNumber like 900700000",
        types: ["IndividualTaxpayerIdentificationNumber"],
        text: "Hey go to 900700000 let me know",
        expectedResult: [{
            match: "900700000",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "IndividualTaxpayerIdentificationNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a IndividualTaxpayerIdentificationNumber like 900 70 0000",
        types: ["IndividualTaxpayerIdentificationNumber"],
        text: "Hey go to 900 70 0000 let me know",
        expectedResult: [{
            match: "900 70 0000",
            firstIndex: 10,
            lastIndex: 21,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "IndividualTaxpayerIdentificationNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a IndividualTaxpayerIdentificationNumber like 900-70-0000",
        types: ["IndividualTaxpayerIdentificationNumber"],
        text: "Hey go to 900-70-0000 let me know",
        expectedResult: [{
            match: "900-70-0000",
            firstIndex: 10,
            lastIndex: 21,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "IndividualTaxpayerIdentificationNumber",
            isValid: true
        }]
    }
    ]
};




const invalidIndividualTaxpayerIdentificationNumberTests = {
    title: "IndividualTaxpayerIdentificationNumber invalid cases",
    items: [{
        description: " - should find and validate a IndividualTaxpayerIdentificationNumber like 556-43-7432",
        types: ["IndividualTaxpayerIdentificationNumber"],
        text: "Hey go to 556-43-7432 let me know",
        expectedResult: []
    }]
};


const tests = [
    validIndividualTaxpayerIdentificationNumberTests,
    invalidIndividualTaxpayerIdentificationNumberTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
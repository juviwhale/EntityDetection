const entities = require("../lib/entities"),
                chai = require("chai");
chai.should();

chai.use(require("chai-things"));


function evalExpected(item, done) {
    const result = entities.get(item.text, item.types, item.custom);

    result.should.be.json;

    if (item.expectedResult[0] && item.expectedResult[0].stub == "should have match here") {
        // checking just isValid in the result
        result.should.have.length.above(0);
        result[0].isValid.should.be.true;
    } else {
        result.should.deep.equal(item.expectedResult);
    }

    done();
}


function run(entityTypes) {
    entityTypes.forEach(function(type) {
        describe(type.title, function() {
            type.items.forEach(function(item) {
                if (item.only) {
                    it.only(item.description, function(done) {
                        evalExpected(item, done);
                    });
                } else if (item.skip) {
                    it.skip(item.description, function(done) {
                        evalExpected(item, done);
                    });
                } else {
                    it(item.description, function(done) {
                        evalExpected(item, done);
                    });
                }
            });
        });
    });
}

// PRINT OUT OF FAILING TEST EXPECTIONS
// USED TO CAPTURE FOR FIXING TESTS
afterEach(function() {
    if (this.currentTest.state === "failed") {
        console.log("MISMATCH IN TEST:", this.currentTest.title); // eslint-disable-line
        if (this.currentTest.err && this.currentTest.err.actual && this.currentTest.err.expected) {
            console.log("EXPECTS-----------"); // eslint-disable-line
            console.log(this.currentTest.err.expected); // eslint-disable-line
            console.log("FOUND-------------"); // eslint-disable-line
            console.log(this.currentTest.err.actual); // eslint-disable-line
        }
    }
});

module.exports.run = run;
const validCurrencyNumberTests = {
    title: "Currency Number valid cases",
    items: [{
        description: " - should find and validate currency number $3.99",
        types: ["Currency"],
        text: "Hey you owe me $3.99. When are you going to pay me",
        expectedResult: [{
            match: "$3.99",
            firstIndex: 15,
            lastIndex: 20,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "$3.99" },
            type: "Currency",
            isValid: true
        }]
    },
    {
        description: " - should find and validate currency number $3",
        types: ["Currency"],
        text: "Hey you owe me $3 When are you going to pay me",
        expectedResult: [{
            match: "$3",
            firstIndex: 15,
            lastIndex: 17,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$3" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $300",
        types: ["Currency"],
        text: "Hey you owe me $300 When are you going to pay me",
        expectedResult: [{
            match: "$300",
            firstIndex: 15,
            lastIndex: 19,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$300" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $ 300",
        types: ["Currency"],
        text: "Hey you owe me $ 300 When are you going to pay me",
        expectedResult: [{
            match: "$ 300",
            firstIndex: 15,
            lastIndex: 20,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$ 300" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $ 300.99",
        types: ["Currency"],
        text: "Hey you owe me $ 300.99 When are you going to pay me",
        expectedResult: [{
            match: "$ 300.99",
            firstIndex: 15,
            lastIndex: 23,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$ 300.99" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number €1,00",
        types: ["Currency"],
        text: "Hey you owe me €1,00 When are you going to pay me",
        expectedResult: [{
            match: "€1,00",
            firstIndex: 15,
            lastIndex: 20,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "€1,00" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should consider *1,00 as potential currency",
        types: ["Currency"],
        text: "Hey you owe me *1,00 When are you going to pay me",
        expectedResult: [{
            match: "*1,00",
            firstIndex: 15,
            lastIndex: 20,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "*1,00" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number €1,00",
        types: ["Currency"],
        text: "Hey you owe me €1,00. When are you going to pay me",
        expectedResult: [{
            match: "€1,00",
            firstIndex: 15,
            lastIndex: 20,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "€1,00" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number €1,00",
        types: ["Currency"],
        text: "Hey you owe me € 1,00 When are you going to pay me",
        expectedResult: [{
            match: "€ 1,00",
            firstIndex: 15,
            lastIndex: 21,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "€ 1,00" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number 23€",
        types: ["Currency"],
        text: "Hey you owe me 23€ When are you going to pay me",
        expectedResult: [{
            match: "23€",
            firstIndex: 15,
            lastIndex: 18,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "23€" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $1,000,000.90",
        types: ["Currency"],
        text: "Hey you owe me $1,000,000.90 When are you going to pay me",
        expectedResult: [{
            match: "$1,000,000.90",
            firstIndex: 15,
            lastIndex: 28,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$1,000,000.90" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $1,000,000.90.",
        types: ["Currency"],
        text: "Hey you owe me $1,000,000.90. When are you going to pay me",
        expectedResult: [{
            match: "$1,000,000.90",
            firstIndex: 15,
            lastIndex: 28,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "$1,000,000.90" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $ 1,000,000",
        types: ["Currency"],
        text: "Hey you owe me $ 1,000,000 When are you going to pay me",
        expectedResult: [{
            match: "$ 1,000,000",
            firstIndex: 15,
            lastIndex: 26,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "$ 1,000,000" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number $ 1,000,000.",
        types: ["Currency"],
        text: "Hey you owe me $ 1,000,000. When are you going to pay me",
        expectedResult: [{
            match: "$ 1,000,000",
            firstIndex: 15,
            lastIndex: 26,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "$ 1,000,000" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number 23¢",
        types: ["Currency"],
        text: "Hey you owe me 23¢. When are you going to pay me",
        expectedResult: [{
            match: "23¢",
            firstIndex: 15,
            lastIndex: 18,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "23¢" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number ($1.99)",
        types: ["Currency"],
        text: "Hey you owe me ($1.99). When are you going to pay me",
        expectedResult: [{
            match: "($1.99)",
            firstIndex: 15,
            lastIndex: 22,
            leadText: "ou owe me ",
            tailText: ". When are",
            meta: { value: "($1.99)" },
            isValid: true,
            type: "Currency"
        }]
    },
    {
        description: " - should find and validate currency number 1,432,234",
        types: ["Currency"],
        text: "Hey you owe me 1,432,234 When are you going to pay me",
        expectedResult: [{
            match: "1,432,234",
            firstIndex: 15,
            lastIndex: 24,
            leadText: "ou owe me ",
            tailText: " When are ",
            meta: { value: "1,432,234" },
            type: "Currency",
            isValid: true,
            isPotential: true
        }]
    }
    ]
};

const invalidCurrencyNumberTests = {
    title: "Currency Number invalid cases",
    items: [{
        description: " - should NOT find and validate currency number 2342.",
        types: ["Currency"],
        text: "Hey you owe me 2342. When are you going to pay me",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number 1/18/99",
        types: ["Currency"],
        text: "Hey you owe me 1/18/99. When are you going to pay me",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number 1.12.77",
        types: ["Currency"],
        text: "Hey you owe me 1.12.77. When are you going to pay me",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number  1-(800)-555-555",
        types: ["Currency"],
        text: "Call me 1-(800)-555-555. When are you going to pay me",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number (800)",
        types: ["Currency"],
        text: "Call me (800) 555-555. When are you going to pay me",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number 95.95%",
        types: ["Currency"],
        text: "Some 95.95% agree.",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency number (3)",
        types: ["Currency"],
        text: "Some (3) agree.",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate currency: Section 2079.13",
        types: ["Currency"],
        text: "Check out Section 2079.13",
        expectedResult: []
    },
        {
            description: " - should NOT find and validate currency: Figure 7.14",
            types: ["Currency"],
            text: "Check out Figure 7.14",
            expectedResult: []
        }
    ]
};

//Should match:
const shouldMatchArray = [
    "1,000",
    "(1,000)",
    "(181.39)",
    "1,000,000",
    "(1,000,000)",
    "($1,000,000)",
    "($1,000,000.00)",
    "$-1,000,000",
    "-$1,000,000",
    "$-1,000,000.00",
    "1,000,000.00",
    "-100,000",
    "-100,000,000.00",
    "23€",
    "-$1999,99",
    "$-1999,99",
    "(1,999.99)",
    "(1.999,99)",
    "($1,999,999)",
    "1.999,00",
    "13.00,",
    "13.00",
    "$1",
    "1.99",
    "$300",
    "$250",
    "$25",
    "$7000",
    "$1.99",
    "$1,000,000.90",
    "$ 1,000,000",
    "23¢",
    "€1,00",
    "$71,503.40",
    "123,123.53",
    "-3,134.04",
    "$188,383.00",
    "5,100.00",
    "123.44€?",
    //"4.656.489", Not sure we should support this, also not sure how we could without not supporting 3.000
    "99.785,01",
    "-3,134.04",
    "£13.00",
    "£9,999.99",
    "(£0.00)",
    "($100)",
    "£13.00,",
    "item cost: $1.99.",
    "(1.00)",
    "(181,00)",
    "(181.00)",
    "181.00",
    "181,00",
    "(181,000.00)",
];

//Should not match:
const shouldNotMatchArray = [
    "(181.0)",
    "(1.0)",
    "(555) 123 - 1234",
    "(555)-123-1234",
    "1-800-123-1234",
    "1",
    "(1)",
    "5.",
    "555,",
    "100",
    "(100)",
    "-100",
    "[1000]",
    "(1000)",
    "1000000",
    "(1000000)",
    "(1.331.11)",
    "0.05%",
    "99.94%",
    "1/18/99",
    "1.12.77",
    "2008",
    "2342.",
    "5.",
    "555,",
    ",656",
    "99,78,5",
    "1,25,944",
    "--7,994,169",
    "0.0,0",
    ".10",
    "1-125,944.1",
    "-7,994E169",
    "1.99$",
    "asdfsdf 1.99$ asdfsdf",
    "£12,333,333.02)",
    "(-£2.17)",
    "0.000",
    //"*1" todo: fix
];

shouldMatchArray.forEach((element) => {
    validCurrencyNumberTests.items.push({
        description: ` - should find and validate currency number ${element}`,
        types: ["Currency"],
        text: `Hey you owe me ${element} When are you going to pay me`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

shouldNotMatchArray.forEach((element) => {
    invalidCurrencyNumberTests.items.push({
        description: ` - should NOT find and validate currency number ${element}`,
        types: ["Currency"],
        text: `Hey you owe me ${element} When are you going to pay me`,
        expectedResult: []
    });
});


const tests = [
    validCurrencyNumberTests,
    invalidCurrencyNumberTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
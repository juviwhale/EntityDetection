const validTests = {
    title: "valid AwsAccessKeyTests cases",
    items: [{
        description: " - should find and validate a AwsAccessKey like Access Key ID: AKIAJUOVCJYVSNAPKZZZ",
        types: ["AwsAccessKey"],
        text: "access-key-id: AKIA56789012345ffff6",
        expectedResult: [{
                "firstIndex": 0,
                "isValid": true,
                "lastIndex": 35,
                "leadText": "",
                "match": "access-key-id: AKIA56789012345ffff6",
                "meta": {},
                "tailText": "",
                "type": "AwsAccessKey",
            }]
    },
    ]
};




const invalidTests = {
    title: "invalid Aws Access Key cases",
    items: [{
        description: " - should not find AHJSJ738594GD3JG",
        types: ["AwsAccessKey"],
        text: "Hey AHJSJ738594GD3JG is ready.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "access-key-id: AKIA56789012345ffff6",
    "accesskeyid AKIA56789012345ffff6",
    "access_id AKIA56789012345ffff6",
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate potential Aws Access Key ${element}`,
        types: ["AwsAccessKey"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "access-key-id: AKIA56789012345",
    "Access Key ID: AKIA56789012345",
    "accesskeyid AKIA56789012345",
    "access_id AKIA56789012345",
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find Aws Access Key: ${element}`,
        types: ["AwsAccessKey"],
        text: `This is ${element}.`,
        expectedResult: []
    });
});

const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
// PO BOX 123 ANYTOWN, PA 10000
// PO BOX 1234 FORT LAUDERDALE, FL 10009
// SUSAN SAMPLE\ n1234 ANY STREET, SUITE 5678\ nADDRESS LINE #2\nCITY, PROVINCE A1B 2C3
// Royal Bank of Canada 5879 ROULE JEAN-BAPTISTE, MONTREAL, PQ H3C 3B8
// One Eastern Place Lynn, MA 01901-1508
// 1000 Walnut\nKansas City MO 64106-3686
// Main and Elm\n100 Main Street\nMetropolis, AA\n012341

const validAddressTests = {
    title: "StreetAddress valid cases",
    items: [{
        description: " - should find and validate an address like 1000 Walnut St. Kansas City MO 64106",
        types: ["StreetAddress"],
        text: "Hey send me a letter at \n1000 Walnut St.\n Kansas City MO 64106",
        expectedResult: [{
            match: "1000 Walnut St.\n Kansas City MO 64106",
            firstIndex: 25,
            lastIndex: 62,
            leadText: "etter at \n",
            tailText: "",
            meta: {
                number: "1000",
                street: "Walnut",
                type: "St",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            },
            type: "StreetAddress",
            isValid: true,
        }]
    },
    {
        description: " - should find and validate an address like PO BOX 1234 FORT LAUDERDALE, FL 10009",
        types: ["StreetAddress"],
        skip: true,
        skip_reason: "no known way to merge in FORT LAUDERDALE as the city name",
        text: "Hey send me a letter at PO BOX 1234\nFORT LAUDERDALE, FL 10009",
        expectedResult: [{
            match: "Hey send me a letter at PO BOX 1234\nFORT LAUDERDALE, FL 10009",
            firstIndex: 0,
            lastIndex: 61,
            leadText: "",
            tailText: "",
            meta: {
                street1: "PO BOX 1234",
                city: "FORT LAUDERDALE",
                state: "FL",
                zip: "10009",
            },
            type: "StreetAddress",
            isValid: true
        }]
    },
    {
        description: " - should find and validate an address like with apt in name",
        types: ["StreetAddress"],
        text: "\n\nJEAN WANG \n\nAddress of Covered Property: \n1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106 \n\nHome Seller: \n\nTOMMY & MAGGIE TURNER \n\nCLOSING/ESCROW INFORMATION \n\n",
        expectedResult: [{
            match: "1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106",
            firstIndex: 44,
            lastIndex: 88,
            leadText: "roperty: \n",
            tailText: " \n\nHome Se",
            label: "Address of Covered Property",
            meta: {
                number: "1097",
                street: "BLANCHE",
                type: "ST",
                sec_unit_type: "APT",
                sec_unit_num: "309",
                city: "PASADENA",
                state: "CA",
                zip: "91106"
            },
            type: "StreetAddress",
            isValid: true
        }]
    },
    {
        description: " - should find and validate an address with long zips",
        types: ["StreetAddress"],
        text: "1000 Walnut\n Kansas City MO 64106-3686\n Jane Customer",
        expectedResult: [{
            match: "1000 Walnut\n Kansas City MO 64106-3686",
            firstIndex: 0,
            lastIndex: 38,
            leadText: "",
            tailText: "\n Jane Cus",
            meta: {
                number: "1000",
                street: "Walnut",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            },
            type: "StreetAddress",
            isValid: true
        }]
    },
    /*{
        description: " - should find and validate an address with long extra text",
        types: ["StreetAddress"],
        text: "Control Board. 2005 Evenraen Slree!, Suite 1500. CallfM0is 95815\n NOTE; Questloru or ptobbms comerning the above veport should be dtected to the manager of tie company. UnreB0ived questions or problems with services performed may De atecl-ed to rte Structurat\n Pest Cmtrol board (916) 561 •6708, (300) 737-8188 m.pestboard.ca.gw,\n 43M41 (Rev. 1001)\n",
        expectedResult: [{
            match: "Control Board. 2005 Evenraen Slree!, Suite 1500. CallfM0is 95815",
            firstIndex: 0,
            lastIndex: 64,
            leadText: "",
            tailText: "\n NOTE; Qu",
            meta: { street: "Control" },
            type: "StreetAddress",
            isValid: true
        }]
    },*/
    {
        description: " - should find and validate an address with long extra text",
        types: ["StreetAddress"],
        text: "\nJohn N. Kitta & Associates \n39560 Stevenson Place, Ste 217 \nFremont, CA 94539 \nTelephone: (510) 797-7990 \n",
        expectedResult: [{
            match: "39560 Stevenson Place, Ste 217 \nFremont, CA 94539",
            firstIndex: 29,
            lastIndex: 78,
            leadText: "sociates \n",
            tailText: " \nTelephon",
            meta: {
                number: "39560",
                street: "Stevenson",
                type: "Place",
                sec_unit_type: "Ste",
                sec_unit_num: "217",
                city: "Fremont",
                state: "CA",
                zip: "94539"
            },
            type: "StreetAddress",
            isValid: true
        }]
    },
    ]
};







const invalidAddressTests = {
    title: "StreetAddress invalid cases",
    items: [{
        description: " - should NOT find and validate an address like 5 Street",
        types: ["StreetAddress"],
        text: "Hey come over to 5 Street today",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate an address like case 1",
        types: ["StreetAddress"],
        text: "THIS IS A SEPARATED REPORT WHICH IS DEFINED AS SECTION I/SECTION 2 CONDITIONS EVIDENT ON THE DATE OF INSPECTION. SECTION 1 CONTAINS ITEMS WHERE THERE IS VISIBLE EVIDENCE OF ACTIVE INFESTATION, INFECTION OR CONDITIONS THAT HAVE RESULTED IN OR FROM INFESTATION OR INFECTION. SECTION 2 ITEMS ARE CONDITIONS DEEMED LIKELY TO LEAD TO INFESTATION OR INFECTION, BUT WHERE NO VISIBLE EVIDENCE OF SUCH WAS FOUND. FURTHER INSPECTION ITEMS ARE DEFINED AS",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate an address like case 2",
        types: ["StreetAddress"],
        text: " This is a Section 1 Item * ",
        expectedResult: []
    }
    ]
};

const tests = [
    validAddressTests,
    invalidAddressTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
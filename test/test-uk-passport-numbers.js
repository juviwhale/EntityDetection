const validUKPassportNumberTests = {
    title: "UKPassportNumber valid cases",
    items: [{
        description: " - should find and validate a UKPassportNumber like 7086493126GBR6510204M150224602",
        types: ["UKPassportNumber"],
        text: "Hey go to 7086493126GBR6510204M150224602 let me know",
        expectedResult: [{
            match: "7086493126GBR6510204M150224602",
            firstIndex: 10,
            lastIndex: 40,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "UKPassportNumber",
            isValid: true
        }]
    }]
};




const invalidUKPassportNumberTests = {
    title: "UKPassportNumber invalid cases",
    items: []
};


const tests = [
    validUKPassportNumberTests,
    invalidUKPassportNumberTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
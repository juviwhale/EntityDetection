const validURLTests = {
    title: "URL valid cases",
    items: [{
        description: " - should find and validate a URL like http://google.com",
        types: ["URL"],
        text: "Hey go to http://google.com on the web",
        expectedResult: [{
            match: "http://google.com",
            firstIndex: 10,
            lastIndex: 27,
            leadText: "Hey go to ",
            tailText: " on the we",
            meta: {},
            type: "URL",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a URL like www.foo.com",
        types: ["URL"],
        text: "Hey go to www.foo.com the web",
        expectedResult: [{
            match: "www.foo.com",
            firstIndex: 10,
            lastIndex: 21,
            leadText: "Hey go to ",
            tailText: " the web",
            meta: {},
            type: "URL",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a URL like ftp://foo.com/file",
        types: ["URL"],
        text: "Hey go to ftp://foo.com/file on the web",
        expectedResult: [{
            match: "ftp://foo.com/file",
            firstIndex: 10,
            lastIndex: 28,
            leadText: "Hey go to ",
            tailText: " on the we",
            meta: {},
            type: "URL",
            isValid: true
        }]
    }, {
        description: " - should find and validate a URL like accounts.google.com",
        types: ["URL"],
        text: "Hey go to accounts.google.com on the web",
        expectedResult: [{
            match: "accounts.google.com",
            firstIndex: 10,
            lastIndex: 29,
            leadText: "Hey go to ",
            tailText: " on the we",
            meta: {},
            type: "URL",
            isValid: true
        }]
    }, {
        description: " - should find and validate a URL like accounts.google.com/login?res=sdfs",
        types: ["URL"],
        text: "Hey go to accounts.google.com on the web",
        expectedResult: [{
            match: "accounts.google.com",
            firstIndex: 10,
            lastIndex: 29,
            leadText: "Hey go to ",
            tailText: " on the we",
            meta: {},
            type: "URL",
            isValid: true
        }]
    }
    ]
};




const invalidURLTests = {
    title: "URL invalid cases",
    items: [{
        description: " - should NOT find and validate a URL like alex@me.com",
        types: ["URL"],
        text: "Hey follow me alex@me.com on twitter",
        expectedResult: []
    }, ]
};

//Should match:
const shouldMatchArray = [
    "http://www.google.com",
    "ftp://example.com",
    "google.com",
    "a.google.com",
    "a.google.com/b/~user",
    "http://www.google.com/~user",
    "www.google.com/sdfa/asdf?f93+3234",
    "accounts.google.com",
    "valid.domain.com/foasdf/~23423lj",
    "accounts.google.com/login?res=sdfs sdf",
    "accounts.google.com",
    "you.name",
    "you.name/~joe",
    "google.com/+joe",
    "https://joe:test@test.com",
    "ftp://example",
];

//Should not match:
const shouldNotMatchArray = [
    "@joe",
    "@twitter.com",
    "joe@twitter.com",
    "joe@me.com",
    "not://a-url",
    "something.asfdasjdlf;",
    "abc.def@#$90fai",
    "something.like.this",
    "abc.def",
];




shouldMatchArray.forEach((element) => {
    validURLTests.items.push({
        description: ` - should find and validate URL ${element}`,
        types: ["URL"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

shouldNotMatchArray.forEach((element) => {
    invalidURLTests.items.push({
        description: ` - should NOT find and validate url ${element}`,
        types: ["URL"],
        text: `This is for in context ${element} as a result`,
        expectedResult: []
    });
});


const tests = [
    validURLTests,
    invalidURLTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
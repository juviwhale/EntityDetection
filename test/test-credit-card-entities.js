// NOTE

// Decent online validator at https: //www.tools4noobs.com/online_tools/credit_card_validate/
// Used this to generate valid fake numbers http: //www.getcreditcardnumbers.com

// ticket and reply for some of the invalid cards mentioned below from card-validator; https://github.com/braintree/card-validator/issues/52#issuecomment-337090534

const validCreditCardNumbersTests = {
    title: "Credit Card Number valid cases",
    items: [{
        description: " - should find and validate a credit card 4571661027381607",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 4571661027381607 your welcome!",
        expectedResult: [{
            match: "4571661027381607",
            firstIndex: 48,
            lastIndex: 64,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Visa",
                formatted: "4571 6610 2738 1607",
                code: { name: "CVV", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a credit card 4571-6610-2738-1607",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 4571-6610-2738-1607 your welcome!",
        expectedResult: [{
            match: "4571-6610-2738-1607",
            firstIndex: 48,
            lastIndex: 67,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Visa",
                formatted: "4571 6610 2738 1607",
                code: { name: "CVV", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a credit card 4571 6610 2738 1607",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 4571 6610 2738 1607 your welcome!",
        expectedResult: [{
            match: "4571 6610 2738 1607",
            firstIndex: 48,
            lastIndex: 67,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Visa",
                formatted: "4571 6610 2738 1607",
                code: { name: "CVV", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a credit card 377220804353992",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 377220804353992 your welcome!",
        expectedResult: [{
            match: "377220804353992",
            firstIndex: 48,
            lastIndex: 63,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "American Express",
                formatted: "3772 2080 4353 992",
                code: { name: "CID", "size": 4 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a credit card 30247633814091",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 30247633814091 your welcome!",
        expectedResult: [{
            match: "30247633814091",
            firstIndex: 48,
            lastIndex: 62,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Diners Club",
                formatted: "3024 7633 8140 91",
                code: { name: "CVV", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a credit card 6011496748607144",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 6011496748607144 your welcome!",
        expectedResult: [{
            match: "6011496748607144",
            firstIndex: 48,
            lastIndex: 64,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Discover",
                formatted: "6011 4967 4860 7144",
                code: { name: "CID", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
        // { // enroute card discontinued
        //     description: ' - should find and validate a credit card 214930167048081',
        //     types: ['CreditCard'],
        //     text: "Please buy something with my credit card, it is 214930167048081 your welcome!",
        //     expectedResult: [{
        //         stub: 'should have match here',
        //     }]
        // },
        // { // JCB invalid: JCB cards all start with 2131, 1800, or 35
        //     description: ' - should find and validate a credit card 3112904154980561',
        //     types: ['CreditCard'],
        //     text: "Please buy something with my credit card, it is 3112904154980561 your welcome!",
        //     expectedResult: [{
        //         stub: 'should have match here',
        //     }]
        // },
    {
        description: " - should find and validate a credit card 30247633814091",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 30247633814091 your welcome!",
        expectedResult: [{
            match: "30247633814091",
            firstIndex: 48,
            lastIndex: 62,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Diners Club",
                formatted: "3024 7633 8140 91",
                code: { name: "CVV", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
        // { // JCB, JCB cards must be at least 16 characters, so this 15 digit card is potentially valid, but not valid.
        //     description: ' - should find and validate a credit card 180088790420118',
        //     types: ['CreditCard'],
        //     text: "Please buy something with my credit card, it is 180088790420118 your welcome!",
        //     expectedResult: [{
        //         stub: 'should have match here',
        //     }]
        // },
    {
        description: " - should find and validate a credit card 5412474435246465",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 5412474435246465 your welcome!",
        expectedResult: [{
            match: "5412474435246465",
            firstIndex: 48,
            lastIndex: 64,
            leadText: "rd, it is ",
            tailText: " your welc",
            meta: {
                cardType: "Mastercard",
                formatted: "5412 4744 3524 6465",
                code: { name: "CVC", "size": 3 }
            },
            type: "CreditCard",
            isValid: true
        }]
    },
        {
            description: " - should find and validate a credit card 5412474435246465",
            types: ["CreditCard"],
            text: "Please buy something with my credit card, it is 5412474435246465.",
            expectedResult: [{
                "firstIndex": 48,
                "isValid": true,
                "lastIndex": 64,
                "leadText": "rd, it is ",
                "match": "5412474435246465",
                "meta": {
                    "cardType": "Mastercard",
                    "code": {
                        "name": "CVC",
                        "size": 3,
                    },
                    "formatted": "5412 4744 3524 6465",
                },
                "tailText": ".",
                "type": "CreditCard",
            }]
        },
        // { // 13 length cards are no longer being issued or supported by card-validator lib
        //     description: ' - should find and validate a credit card 4716707626401',
        //     types: ['CreditCard'],
        //     text: "Please buy something with my credit card, it is 4716707626401 your welcome!",
        //     expectedResult: [{
        //         match: '4716707626401',
        //         firstIndex: 48,
        //         lastIndex: 61,
        //         meta: { cardType: 'Visa', formatted: '4716 7076 2640 1' },
        //         type: 'CreditCard',
        //         isValid: true
        //     }]
        // },
        // { // voyager? (fuel card not applicable for online payments.) not supported by card-validator
        //     description: ' - should find and validate a credit card 869939653329358',
        //     types: ['CreditCard'],
        //     text: "Please buy something with my credit card, it is 869939653329358 your welcome!",
        //     expectedResult: [{
        //         stub: 'should have match here',
        //     }]
        // }
    ]
};



const invalidCreditCardNumbersTests = {
    title: "Credit Card Number invalid cases",
    items: [{
        description: " - should NOT find and validate a credit card 4571661027381607Appeles",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 4571661027381607Appeles your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a credit card with a bad visa checksum 4916-1396-4100-8118 ",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 4916-1396-4100-8118 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a credit card with a bad american express checksum 370938973924891 ",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 370938973924891 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a credit card with a bad master card checksum 5208579052959577 ",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 5208579052959577 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a credit card with a bad master card checksum 00000988081483 ",
        types: ["CreditCard"],
        text: "Please buy something with my credit card, it is 00000988081483 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a credit card with url http://www.something/671647679869194/",
        types: ["CreditCard"],
        text: "Please visit http://www.something.com/671647679869194/ your welcome!",
        expectedResult: []
    }


    ]
};

const tests = [
    validCreditCardNumbersTests,
    invalidCreditCardNumbersTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
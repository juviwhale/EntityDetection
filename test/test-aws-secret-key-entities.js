const validTests = {
    title: "valid AwsSecretKey cases",
    items: [{
        description: " - should find and validate a AwsSecretKey like \"Secret Access Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31\",",
        types: ["AwsSecretKey"],
        text: "Secret Access Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31",
        expectedResult: [
            {
                "firstIndex": 0,
                "isValid": true,
                "lastIndex": 59,
                "leadText": "",
                "match": "Secret Access Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31",
                "meta": {},
                "tailText": "",
                "type": "AwsSecretKey",
            }
        ]

    },
    ]
};




const invalidTests = {
    title: "invalid Aws Secret Key cases",
    items: [{
        description: " - should not find AHJSJ738594GD3JG",
        types: ["AwsSecretKey"],
        text: "Hey AHJSJ738594GD3JG is ready.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "Secret Access Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31",
    "Secret-Access-Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31",
    "key: secretAccessKey xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jWZ31",
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate potential AwsSecretKey: ${element}`,
        types: ["AwsSecretKey"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "Secret Access Key: xyza/zzzuO74Ro9QTDaHfDE1RzzWfX31Xw0jW",
    "Secret-Access-Key: xyza/zzzuO74Ro9QTD",
    "secretAccessKey xyza/zzzuO74Ro",
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find Aws Secret Key: ${element}`,
        types: ["AwsSecretKey"],
        text: `This is ${element}.`,
        expectedResult: []
    });
});

const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
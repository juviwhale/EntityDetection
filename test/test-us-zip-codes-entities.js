const validTests = {
    title: "Valid UsZipCodes test cases",
    items: [{
        description: " - should find and validate a US Zip Codes like: 90210",
        types: ["UsZipCodes"],
        text: "I live in 90210.",
        expectedResult: [
            {
                "firstIndex": 10,
                "isValid": true,
                "lastIndex": 15,
                "leadText": "I live in ",
                "match": "90210",
                "meta": {
                    "place": {
                        "city": "Beverly Hills",
                        "lat": 34.0901,
                        "long": -118.4065,
                        "state": "CA",
                        "zip": "90210",
                    },
                },
                "tailText": ".",
                "type": "UsZipCodes"
            }
        ]
    },
        {
            description: " - should find and validate a US Zip Codes like: 49441-3214 Muskegon, MI",
            types: ["UsZipCodes"],
            text: "I live in Muskegon, MI, 49441-3214.",
            expectedResult: [
                {
                    "firstIndex": 24,
                    "isValid": true,
                    "lastIndex": 34,
                    "leadText": "egon, MI, ",
                    "match": "49441-3214",
                    "meta": {
                        "place": {
                            "city": "Muskegon",
                            "lat": 43.1962,
                            "long": -86.2738,
                            "state": "MI",
                            "zip": "49441",
                        },
                    },
                    "tailText": ".",
                    "type": "UsZipCodes",
                }
            ]

        }]
};




const invalidTests = {
    title: "Inalid UsZipCodes test cases",
    items: [{
        description: " - should not find 123456 ",
        types: ["UsZipCodes"],
        text: "I live in Nowhere Place, Maryland, 11115.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "53051 Menomonee Falls, WI",
    "47150 New Albany, IN",
    "21136 Reisterstown, MD",
    "48066 Roseville, MI",
    "30605 Athens, GA",
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate potential ${element}`,
        types: ["UsZipCodes"],
        text: `Hey live in ${element}.`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "11115 Menomonee Falls, WI",
    "543211 New Albany, IN",
    "1234 Reisterstown, MD",
    "11 Roseville, MI",
    "26633 Athens, GA",
    "587669530 608223442 190249018 623064066",
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find and validate ${element}`,
        types: ["UsZipCodes"],
        text: `Hey live in ${element}.`,
        expectedResult: []
    });
});


const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
const validUKDriversLicenseTests = {
    title: "UKDriversLicense valid cases",
    items: [{
        description: " - should find and validate a UKDriversLicense like AHJSJ738594GD3JG",
        types: ["UKDriversLicense"],
        text: "Hey go to AHJSJ738594GD3JG let me know",
        expectedResult: [{
            match: "AHJSJ738594GD3JG",
            firstIndex: 10,
            lastIndex: 26,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "UKDriversLicense",
            isValid: true
        }]
    }]
};




const invalidUKDriversLicenseTests = {
    title: "UKDriversLicense invalid cases",
    items: []
};


const tests = [
    validUKDriversLicenseTests,
    invalidUKDriversLicenseTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
const validPotentialIDTests = {
    title: "PotentialID valid cases",
    items: [{
        description: " - should find and validate potential id RBCPDA0001 - 123456789 - 01 - 100001 - 1 - 1001",
        types: ["PotentialID"],
        text: "This is for in context RBCPDA0001 - 123456789 - 01 - 100001 - 1 - 1001 as a result",
        expectedResult: [{
            match: "RBCPDA0001 - 123456789 - 01 - 100001 - 1 - 1001",
            firstIndex: 23,
            lastIndex: 70,
            leadText: "n context ",
            tailText: " as a resu",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    },
    {
        description: " - should find and validate potential id Please include Lease # 24059 on the check",
        types: ["PotentialID"],
        text: "Please include Lease # 24059 on the check",
        expectedResult: [{
            match: "24059",
            firstIndex: 23,
            lastIndex: 28,
            leadText: "e Lease # ",
            tailText: " on the ch",
            label: "Please include Lease",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    },
    {
        description: " - should find and validate potential id AGREEMENT NUMBER: 227794282",
        types: ["PotentialID"],
        text: "\n\nAGREEMENT NUMBER: 227794282 \n\nIMPORTANT: For service call American Home Shield ONLY at",
        expectedResult: [{
            match: "227794282",
            firstIndex: 20,
            lastIndex: 29,
            leadText: "T NUMBER: ",
            tailText: " \n\nIMPORTA",
            label: "AGREEMENT NUMBER",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    },
    {
        description: " - should find and validate potential id but with text",
        types: ["PotentialID"],
        note: "WE COULD ADD A SPELL ERORR IDENTIFIER HERE",
        text: " Company: OJC82-TALK MOBILE INC",
        expectedResult: [{
            match: "OJC82-TALK",
            firstIndex: 10,
            lastIndex: 20,
            leadText: " Company: ",
            tailText: " MOBILE IN",
            label: "Company",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    }, {
        description: " - should find and validate potential id but without text",
        types: ["PotentialID"],
        skip: true,
        text: " Company: OJC82 - TALK MOBILE INC",
        expectedResult: [{
            match: "OJC82",
            firstIndex: 10,
            lastIndex: 5,
            leadText: " Company: ",
            tailText: " MOBILE IN",
            label: "Company",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    }, {
        description: " - should find and validate potential id with extra dash",
        types: ["PotentialID"],
        skip: true,
        text: "\nCase No. V-012326-2 \n\n",
        expectedResult: [{
            match: "OJC82",
            firstIndex: 10,
            lastIndex: 5,
            leadText: " Company: ",
            tailText: " MOBILE IN",
            label: "Company",
            meta: {},
            type: "PotentialID",
            isValid: true
        }]
    }
    ]
};





const invalidPotentialIDTests = {
    title: "PotentialID invalid cases",
    items: [
        {
            description: " - should NOT find and validate a PotentialID like: in Section 10131.6",
            types: ["PotentialID"],
            text: "contained in Section 10131.61 of the Business and Professions Code",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a PotentialID like: in Section: 10131.6",
            types: ["PotentialID"],
            text: "contained in Section: 10131.61 of the Business and Professions Code",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a PotentialID like: in Sections: 10131.6",
            types: ["PotentialID"],
            text: "contained in Sections: 10131.61 of the Business and Professions Code",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a PotentialID like: in Figure: 7.14",
            types: ["PotentialID"],
            text: "contained in Figure: 7.14 of the Business and Professions Code",
            expectedResult: []
        },
    ]
};

//Should match:
const shouldMatchArray = [
    "Your account number: 12782 - 5094431",
    "RBCPDA0001 - 123456789 - 01 - 100001 - 1 - 1001",
    "to account number 555555555555",
    "Reference Number: PAS1234",
    "Account Number 11 - 123456",
    "Primary Account Number: 000009752",
    "CONNECTIONS CHECKING Account# 000009752",
    "Reference Number: 234ASDF",
    "Reference Number: 234asdf",
    "Reference Number: asdf234",
    "Reference Number: ADF234",
    "Reference Number: 234ASDF234",
    "Reference Number: ASDF234ASD",
    "Reference Number: 234asdf234sdf234sdf234234234dfsdfsd",
    "234-234-2344-432",
    "2-43234-23423-4",
    "234A-234B-2344c-432",
    "Reference Number: 023422",
    "Reference Number: 000234234",
    "Reference Number: N-287392",
    "AccountID: 666666666",
    "Closing File Number: 2358-TAB ",
    " 1 Year Agreement Number: 227794282 ",
    "Account Number:\n 0123456789 Original",
    "Name identification number: 15621",
    "\nCase No. V-012326-2 \n\n",
    //"Janan Hafez, Esq. (SBN 366814) ", //FIXME
    // '',
    // '',
    // '',
    // '',
];

//Should not match:
const shouldNotMatchArray = [
    "1",
    "Reference Number: John Smith",
    "2342000024",
    "243240",
    "",
    "0",
    "01",
    "3D"
];




shouldMatchArray.forEach((element) => {
    validPotentialIDTests.items.push({
        description: ` - should find and validate potential id ${element}`,
        types: ["PotentialID"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

shouldNotMatchArray.forEach((element) => {
    invalidPotentialIDTests.items.push({
        description: ` - should NOT find and validate potential id ${element}`,
        types: ["PotentialID"],
        text: `This is for in context ${element} as a result`,
        expectedResult: []
    });
});

const tests = [
    validPotentialIDTests,
    invalidPotentialIDTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
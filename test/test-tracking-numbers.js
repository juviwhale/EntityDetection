const validTrackingNumberTests = {
    title: "Shipper valid cases",
    items: [{
        description: " - should find and validate a TrackingNumber like 1Z879E930346834440",
        types: ["TrackingNumber"],
        text: "Hey my tracking number is UPS 1Z879E930346834440 check the status.",
        expectedResult: [
            {
                match: '1Z879E930346834440',
                firstIndex: 30,
                lastIndex: 48,
                leadText: 'er is UPS ',
                tailText: ' check the',
                meta: {shipper_name: 'UPS'},
                type: 'TrackingNumber',
                isValid: true
            }
        ]
    },
        {
            description: " - should find and validate a TrackingNumber like 1ZAW14290350433989",
            types: ["TrackingNumber"],
            text: "Hey my tracking number is UPS 1ZAW15290350433989 check the status.",
            expectedResult: [
                {
                    match: '1ZAW15290350433989',
                    firstIndex: 30,
                    lastIndex: 48,
                    leadText: 'er is UPS ',
                    tailText: ' check the',
                    meta: { shipper_name: 'UPS' },
                    type: 'TrackingNumber',
                    isValid: true
                }
            ]
        }]
};




const invalidTrackingNumberTests = {
    title: "Shipper invalid cases",
    items: [{
        description: " - should NOT find and validate a TrackingNumber like ABCDEF338383",
        types: ["TrackingNumber"],
        text: "Hey follow me alex@me.com on twitter",
        expectedResult: []
    }, ]
};


const tests = [
    validTrackingNumberTests,
    invalidTrackingNumberTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
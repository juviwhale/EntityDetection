const chrono = require("chrono-node");

function toString(val) {
    return JSON.parse(JSON.stringify(val));
}

const validDateTests = {
    title: "Date valid cases",
    items: [{
        description: " - should find and validate date 10/09/2017",
        types: ["Date"],
        text: "The meeting date is set! 10/09/2017 Add it to you calendar",
        expectedResult: [{
            match: "10/09/2017",
            firstIndex: 25,
            lastIndex: 35,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("10/09/2017")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 10-09-2017",
        types: ["Date"],
        text: "The meeting date is set! 10-09-2017 Add it to you calendar",
        expectedResult: [{
            match: "10-09-2017",
            firstIndex: 25,
            lastIndex: 35,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("10-09-2017")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 10-9-2017",
        types: ["Date"],
        text: "The meeting date is set! 10-9-2017 Add it to you calendar",
        expectedResult: [{
            match: "10-9-2017",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("10-9-2017")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 10/9/2017",
        types: ["Date"],
        text: "The meeting date is set! 10/9/2017 Add it to you calendar",
        expectedResult: [{
            match: "10/9/2017",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("10/9/2017")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 10/9/17",
        types: ["Date"],
        text: "The meeting date is set! 10/9/17 Add it to you calendar",
        expectedResult: [{
            match: "10/9/17",
            firstIndex: 25,
            lastIndex: 32,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("10/9/17")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 6/09",
        types: ["Date"],
        text: "The meeting date is set! 6/09 Add it to you calendar",
        expectedResult: [{
            match: "6/09",
            firstIndex: 25,
            lastIndex: 29,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6/09")) },
            type: "Date",
            isValid: true
        }]

    },
    {
        description: " - should find and validate date Nov 4, 2003",
        types: ["Date"],
        text: "The meeting date is set! Nov 4, 2003 Add it to you calendar",
        expectedResult: [{
            match: "Nov 4, 2003",
            firstIndex: 25,
            lastIndex: 36,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Nov 4, 2003")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date  Tue Nov 04 20:14:00",
        types: ["Date"],
        text: "The meeting date is set! Tue Nov 04 20:14:00 Add it to you calendar",
        expectedResult: [{
            match: "Tue Nov 04 20:14:00",
            firstIndex: 25,
            lastIndex: 44,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Tue Nov 04 20:14:00")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date November 4, 2003 8:14 AM",
        types: ["Date"],
        text: "The meeting date is set! November 4, 2003 8:14 AM Add it to you calendar",
        expectedResult: [{
            match: "November 4, 2003 8:14 AM",
            firstIndex: 25,
            lastIndex: 49,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("November 4, 2003 8:14 AM")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date Mar 25 2015",
        types: ["Date"],
        text: "The meeting date is set! Mar 25 2015 Add it to you calendar",
        expectedResult: [{
            match: "Mar 25 2015",
            firstIndex: 25,
            lastIndex: 36,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Mar 25 2015")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date Wednesday March 25 2015",
        types: ["Date"],
        text: "The meeting date is set! Wednesday March 25 2015 Add it to you calendar",
        expectedResult: [{
            match: "Wednesday March 25 2015",
            firstIndex: 25,
            lastIndex: 48,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Wednesday March 25 2015")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 3-Aug-06",
        types: ["Date"],
        text: "The meeting date is set! 3-Aug-06 Add it to you calendar",
        expectedResult: [{
            match: "3-Aug-06",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("3-Aug-06")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 03-Aug-06",
        types: ["Date"],
        text: "The meeting date is set! 03-Aug-06 Add it to you calendar",
        expectedResult: [{
            match: "03-Aug-06",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("03-Aug-06")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 6:55:30 pm",
        types: ["Date"],
        text: "The meeting date is set! 6:55:30 pm Add it to you calendar",
        expectedResult: [{
            match: "6:55:30 pm",
            firstIndex: 25,
            lastIndex: 35,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55:30 pm")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 6:55",
        types: ["Date"],
        text: "The meeting date is set! 6:55 Add it to you calendar",
        expectedResult: [{
            match: "6:55",
            firstIndex: 25,
            lastIndex: 29,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 6:55 a.m.",
        types: ["Date"],
        text: "The meeting date is set! 6:55 a.m. Add it to you calendar",
        expectedResult: [{
            match: "6:55 a.m.",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55 a.m.")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 6:55 am",
        types: ["Date"],
        text: "The meeting date is set! 6:55 am Add it to you calendar",
        expectedResult: [{
            match: "6:55 am",
            firstIndex: 25,
            lastIndex: 32,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55 am")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 6:55 A.M.",
        types: ["Date"],
        text: "The meeting date is set! 6:55 A.M. Add it to you calendar",
        expectedResult: [{
            match: "6:55 A.M.",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55 A.M.")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 6:55 AM",
        types: ["Date"],
        text: "The meeting date is set! 6:55 AM Add it to you calendar",
        expectedResult: [{
            match: "6:55 AM",
            firstIndex: 25,
            lastIndex: 32,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55 AM")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 6:55AM",
        types: ["Date"],
        text: "The meeting date is set! 6:55AM Add it to you calendar",
        expectedResult: [{
            match: "6:55AM",
            firstIndex: 25,
            lastIndex: 31,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("6:55AM")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 3-August-2006",
        types: ["Date"],
        text: "The meeting date is set! 3-August-2006 Add it to you calendar",
        expectedResult: [{
            match: "3-August-2006",
            firstIndex: 25,
            lastIndex: 38,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("3-August-2006")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date August-2006",
        types: ["Date"],
        text: "The meeting date is set! August-2006 Add it to you calendar",
        expectedResult: [{
            match: "August-2006",
            firstIndex: 25,
            lastIndex: 36,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("August-2006")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date Aug-2006",
        types: ["Date"],
        text: "The meeting date is set! Aug-2006 Add it to you calendar",
        expectedResult: [{
            match: "Aug-2006",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Aug-2006")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date Aug/2006",
        types: ["Date"],
        text: "The meeting date is set! Aug/2006 Add it to you calendar",
        expectedResult: [{
            match: "Aug/2006",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Aug/2006")) },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date  June 15",
        types: ["Date"],
        text: "The meeting date is set!  June 15 Add it to you calendar",
        expectedResult: [{
            match: "June 15",
            firstIndex: 26,
            lastIndex: 33,
            leadText: " is set!  ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("June 15")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date  June 15th",
        types: ["Date"],
        text: "The meeting date is set!  June 15th Add it to you calendar",
        expectedResult: [{
            match: "June 15th",
            firstIndex: 26,
            lastIndex: 35,
            leadText: " is set!  ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("June 15th")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 5th June 15th => 5th June",
        types: ["Date"],
        text: "The meeting date is set! 5th June 15th Add it to you calendar",
        expectedResult: [{
            match: "5th June",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " 15th Add ",
            meta: { formatted: toString(chrono.parseDate("5th June")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 15th July, June 5th",
        types: ["Date"],
        text: "The meeting date is set! 15th July, June 5th Add it to you calendar",
        expectedResult: [{
            match: "June 5th",
            firstIndex: 36,
            lastIndex: 44,
            leadText: "5th July, ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("June 5th")) },
            type: "Date",
            isValid: true
        },
        {
            match: "15th July,",
            firstIndex: 25,
            lastIndex: 35,
            leadText: "e is set! ",
            tailText: " June 5th ",
            meta: { formatted: toString(chrono.parseDate("15th July")) },
            type: "Date",
            isValid: true
        }
        ]
    },
    {
        description: " - should find and validate date 15th July, 1st June, 5th 2008",
        types: ["Date"],
        text: "The meeting date is set! 15th July, 1st June 5th 2008 Add it to you calendar",
        expectedResult: [{
            match: "15th July,",
            firstIndex: 25,
            lastIndex: 35,
            leadText: "e is set! ",
            tailText: " 1st June ",
            meta: { formatted: toString(chrono.parseDate("15th July")) },
            type: "Date",
            isValid: true
        },
        {
            match: "1st June",
            firstIndex: 36,
            lastIndex: 44,
            leadText: "5th July, ",
            tailText: " 5th 2008 ",
            meta: { formatted: toString(chrono.parseDate("1st June")) },
            type: "Date",
            isValid: true
        },
        {
            match: "2008",
            firstIndex: 49,
            lastIndex: 53,
            leadText: " June 5th ",
            tailText: " Add it to",
            meta: { formatted: "2008" },
            type: "Date",
            isValid: true
        }
        ]
    },
    {
        description: " - should find and validate date Thursday, April 10, 2008 6:30 AM",
        types: ["Date"],
        text: "The meeting date is set! Thursday, April 10, 2008 6:30 AM Add it to you calendar",
        expectedResult: [{
            match: "Thursday, April 10, 2008 6:30 AM",
            firstIndex: 25,
            lastIndex: 57,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Thursday, April 10, 2008 6:30 AM")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 2017",
        types: ["Date"],
        text: "The meeting date is set! 2017 Add it to you calendar",
        expectedResult: [{
            match: "2017",
            firstIndex: 25,
            lastIndex: 29,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: "2017" },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 15 Mar",
        types: ["Date"],
        text: "The meeting date is set! 15 Mar Add it to you calendar",
        expectedResult: [{
            match: "15 Mar",
            firstIndex: 25,
            lastIndex: 31,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("15 Mar")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 15 Mar as single",
        types: ["Date"],
        text: "\n\n15 Mar\n\n",
        expectedResult: [{
            match: "15 Mar",
            firstIndex: 2,
            lastIndex: 8,
            leadText: "\n\n",
            tailText: "\n\n",
            meta: { formatted: toString(chrono.parseDate("15 Mar")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 11 / 2012",
        types: ["Date"],
        text: "The meeting date is set! 11 / 2012 Add it to you calendar",
        expectedResult: [{
            match: "11 / 2012",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: "2012-11" },
            isValid: true,
            type: "Date"
        }]
    },
    {
        description: " - should find and validate date 12 - 2012",
        types: ["Date"],
        text: "The meeting date is set! 12 - 2012 Add it to you calendar",
        expectedResult: [{
            match: "12 - 2012",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: "2012-12" },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 04 / 2012",
        types: ["Date"],
        text: "The meeting date is set! 04 / 2012 Add it to you calendar",
        expectedResult: [{
            match: "04 / 2012",
            firstIndex: 25,
            lastIndex: 34,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: "2012-04" },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Apr 8",
        types: ["Date"],
        text: "The meeting date is set! Apr 8 Add it to you calendar",
        expectedResult: [{
            match: "Apr 8",
            firstIndex: 25,
            lastIndex: 30,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Apr 8")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date 05 - 19",
        types: ["Date"],
        text: "The meeting date is set! 05 - 19 Add it to you calendar",
        expectedResult: [{
            match: "05 - 19",
            firstIndex: 25,
            lastIndex: 32,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("05 - 19")) }, // chrono accepts this string
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Dec:",
        types: ["Date"],
        text: "The meeting date is set! Dec: 150,000 x 50% Add it to you calendar",
        expectedResult: [{
            match: "Dec:",
            firstIndex: 25,
            lastIndex: 29,
            leadText: "e is set! ",
            tailText: " 150,000 x",
            meta: { formatted: toString(chrono.parseDate("Dec:")) }, // chrono accepts this string
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Jan2008",
        types: ["Date"],
        text: "The meeting date is set! Jan2008 Add it to you calendar",
        expectedResult: [{
            match: "Jan2008",
            firstIndex: 25,
            lastIndex: 32,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Jan2008")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Jan:2008",
        types: ["Date"],
        text: "The meeting date is set! Jan:2008 Add it to you calendar",
        expectedResult: [{
            match: "Jan:2008",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Jan:2008")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Jan-2008",
        types: ["Date"],
        text: "The meeting date is set! Jan-2008 Add it to you calendar",
        expectedResult: [{
            match: "Jan-2008",
            firstIndex: 25,
            lastIndex: 33,
            leadText: "e is set! ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("Jan-2008")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Dec: and March 11, 2008 5:00 AM",
        types: ["Date"],
        text: "The meeting date is set! Dec: and March 11, 2008 5:00 AM Add it to you calendar",
        expectedResult: [{
            match: "Dec:",
            firstIndex: 25,
            lastIndex: 29,
            leadText: "e is set! ",
            tailText: " and March",
            meta: { formatted: toString(chrono.parseDate("Dec:")) },
            type: "Date",
            isValid: true
        },
        {
            match: "March 11, 2008 5:00 AM",
            firstIndex: 34,
            lastIndex: 56,
            leadText: " Dec: and ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("March 11, 2008 5:00 AM")) },
            type: "Date",
            isValid: true
        }
        ]
    },
    {
        description: " - should find and validate date Since 1992,",
        types: ["Date"],
        text: "The meeting date is set! Since 1992, Add it to you calendar",
        expectedResult: [{
            match: "1992",
            firstIndex: 31,
            lastIndex: 35,
            leadText: "et! Since ",
            tailText: ", Add it t",
            meta: { formatted: "1992" },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date May",
        types: ["Date"],
        text: "The meeting date is set! This may be set in May, Add it to you calendar",
        expectedResult: [{
            match: "May,",
            firstIndex: 44,
            lastIndex: 48,
            leadText: "be set in ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("May")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date may 16",
        types: ["Date"],
        text: "The meeting date is set! This may be set in may 16, Add it to you calendar",
        expectedResult: [{
            match: "may 16,",
            firstIndex: 44,
            lastIndex: 51,
            leadText: "be set in ",
            tailText: " Add it to",
            meta: { formatted: toString(chrono.parseDate("may 16")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date Since 12/15/2015",
        types: ["Date"],
        text: "The meeting date is set! Since\n\n12/15/2015 \n\nAdd it to you calendar",
        expectedResult: [{
            match: "12/15/2015",
            firstIndex: 32,
            lastIndex: 42,
            leadText: "t! Since\n\n",
            tailText: " \n\nAdd it ",
            meta: { formatted: toString(chrono.parseDate("12/15/2015")) },
            type: "Date",
            isValid: true
        }]
    },
        // WHAT THE HECK is this test all about??
        // {
        //     description: " - should find and validate date Jan: 103,300",
        //     types: ["Date"],
        //     text: "Based on the following information Jan: 103,300 dogs ",
        //     expectedResult: [{
        //         match: "Jan:",
        //         firstIndex: 35,
        //         lastIndex: 39,
        //         leadText: "formation ",
        //         tailText: " 103,300 d",
        //         meta: { formatted: "2018-01-01T20:00:00.000Z" },
        //         type: "Date",
        //         isValid: true
        //     }]
        // },
    {
        description: " - should find and validate date September 10, 2007 ",
        types: ["Date"],
        text: "Management Accounting Practices 3120, Cash Flow Management. September 10, 2007  ",
        expectedResult: [{
            match: "September 10, 2007",
            firstIndex: 60,
            lastIndex: 78,
            leadText: "nagement. ",
            tailText: "  ",
            meta: { formatted: toString(chrono.parseDate("September 10, 2007")) },
            type: "Date",
            isValid: true
        }]
    },
    {
        description: " - should find and validate date complex date parts",
        types: ["Date"],
        skip: true, // a bit complex
        text: "Cancellation: A full refund will be issued if the ride is canceled at least two days in advance (e.g. by 11:59pm on Tuesday for a 3pm Thursday trip",
        expectedResult: [{
            fix_me: true
        }]
    },
    ]
};



const invalidDateTests = {
    title: "Date invalid cases",
    items: [{
        description: " - should NOT find and validate date sometime in May",
        types: ["Date"],
        text: "The meeting date is set! sometime in May. Add it to you calendar",
        expectedResult: []
    },
        // { // note: its not our place to say if its right or wrong
        //     description: ' - should NOT find and validate date 12/9/0017',
        //     types: ['Date'],
        //     text: "The meeting date is set! 12/9/0017 Add it to you calendar",
        //     expectedResult: []
        // },
    {
        description: " - should NOT find and validate date 12/32/2017",
        types: ["Date"],
        text: "The meeting date is set! 12/32/2017 Add it to you calendar",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date 1746",
        types: ["Date"],
        text: "The meeting date is set! 1746 Add it to you calendar",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: lowercase may",
        types: ["Date"],
        text: "This may or may not be a month",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: may",
        types: ["Date"],
        text: "The judgment upon the award rendered by the arbitrator may be entered in any court having jurisdiction.",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: may case 2",
        types: ["Date"],
        text: "In addition, the Board may, in accordance",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: Section 2079.13",
        types: ["Date"],
        text: "Check out Section 2079.13",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: Section 2079.13",
        types: ["Date"],
        text: "Check out Section: 2079.13",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: Section 2079.13",
        types: ["Date"],
        text: "Check out Sections 2079",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate date: Figure: 2079.13",
        types: ["Date"],
        text: "Check out Figure: 2079.12",
        expectedResult: []
    }
    ]
};

const tests = [
    validDateTests,
    invalidDateTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
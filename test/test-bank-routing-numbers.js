const validBankRoutingNumberTests = {
    title: "BankRoutingNumber valid cases",
    items: [{
        description: " - should find and validate a Wells Fargo Routing Number like 121042882",
        types: ["BankRoutingNumber"],
        text: "Hey go to 121042882 let me know",
        expectedResult: [{
            match: "121042882",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "Wells Fargo" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a USBank Routing Number like 121122676",
        types: ["BankRoutingNumber"],
        text: "Hey go to 121122676 let me know",
        expectedResult: [{
            match: "121122676",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "US Bank" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a United Bank Routing Number like 122243350",
        types: ["BankRoutingNumber"],
        text: "Hey go to 122243350 let me know",
        expectedResult: [{
            match: "122243350",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "United Bank" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a Bank of America Routing Number like 026000358",
        types: ["BankRoutingNumber"],
        text: "Hey go to 026000358 let me know",
        expectedResult: [{
            match: "026000358",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "Bank of America" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a Bank of America Routing Number like 026009593",
        types: ["BankRoutingNumber"],
        text: "Hey go to 026009593 let me know",
        expectedResult: [{
            match: "026009593",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "Bank of America" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a BBVA Compass Routing Number like 062001186",
        types: ["BankRoutingNumber"],
        text: "Hey go to 062001186 let me know",
        expectedResult: [{
            match: "062001186",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "BBVA Compass" },
            type: "BankRoutingNumber",
            isValid: true
        }]

    },
    {
        description: " - should find and validate a Chase Routing Numberslike 322271627",
        types: ["BankRoutingNumber"],
        text: "Hey go to 322271627 let me know",
        expectedResult: [{
            match: "322271627",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "Chase" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a Citibank Routing Numbers like 321171184",
        types: ["BankRoutingNumber"],
        text: "Hey go to 321171184 let me know",
        expectedResult: [{
            match: "321171184",
            firstIndex: 10,
            lastIndex: 19,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: { bankName: "Citibank" },
            type: "BankRoutingNumber",
            isValid: true
        }]
    }
    ]
};




const invalidBankRoutingNumberTests = {
    title: "BankRoutingNumber invalid cases",
    items: []
};


const tests = [
    validBankRoutingNumberTests,
    invalidBankRoutingNumberTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
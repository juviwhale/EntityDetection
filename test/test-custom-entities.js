const validCustomEntitiesTests = {
    title: "Custom keywords samples",
    items: [{
        description: " - should find keywords pay and me",
        types: ["Custom"],
        custom: [{
            keywords: "pay,me"
        }],
        text: "Hey you owe me $3.99. When are you going to pay me",
        expectedResult: [{
            match: 'me',
            firstIndex: 12,
            lastIndex: 14,
            leadText: 'y you owe ',
            tailText: ' $3.99. Wh',
            meta: {value: 'me'},
            type: 'Custom',
            isValid: true
        },
            {
                match: 'pay',
                firstIndex: 44,
                lastIndex: 47,
                leadText: ' going to ',
                tailText: ' me',
                meta: {value: 'pay'},
                type: 'Custom',
                isValid: true
            },
            {
                match: 'me',
                firstIndex: 48,
                lastIndex: 50,
                leadText: 'ng to pay ',
                tailText: '',
                meta: {value: 'me'},
                type: 'Custom',
                isValid: true
            }]
    },
        {
            description: ' - should find keyword "Detached porches"',
            types: ["Custom"],
            custom: [{
                keywords: "Detached porches"
            }],
            text: 'WOOD DESTROYh.G PESTS AND ORGANISMS\n SPECTION REPORT\n BUILDING NO.\n 13274\n STREET. CITY, STATE, ZIP\n Sundance Avenue, San Diego CA\n 92129\n Date of Inspection\n 7/29/2013\n Escrow NO.\n Report Sent To:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn; eddie@brokereddie.com\n 858-525-3258\n 858-876-1922\n REINSPECTION REPORT O\n No. ot Pages\n 6\n KENNEDY PEST CONTROL, INC.\n 1518 Sterling Court, Escondido, CA 92029\n Tel: (800) 420-7378 (760) 233-2090 Fax\n Firm Registration No. PR 1932\n Ordered By:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn: eddie@brokereddie.com\n 858-525-3258 858-876-1922\n COMPLETE REPORT\n General Description:\n Report No. 26519\n : Property Owner/Patty of Interest\n Ben Reginier\n 13274 Sundance Avenue\n San Diego, CA 92129\n Attn: bregnier@gmail.com\n • 310-902-0714\n LIMITED REPORT CJ\n SUPPLEMENTAL REPORT\n Inspection Tag Posted:\n Occupied and fumished one-storyt single family residence with attached garage and Garage\n garage.\n Other Tags Posted:\n None noted\n An inspection has been made to the struäure(s) shown on ihe diagram in accordance wiih thd Structural Pest Controi Act. Detached porches, detached\n steps, detached decks end any other structures not on the diagram were not inspected.\n Subterranean Termites C]\n Drywood Termites [3 Fungus/Dryrot O Other Findings O\n Further Inspection C]\n If any of.above boxes are checked it indicates that there were visible problems In accessb!e areas. for details on itemg.„\n Inspected by Richard Martinez\n 274\n State License No. FR 8762\n Signature\n You are entibd to obteln copies of all tepons and completion noUees on this property reported to the Structural Pest Control Board durh9 the pnceeding two yean. To obleh copies contact: S\n Control Board. 2005 Evergreen Strut, Suite 1500. Sanmento. Callfomia 95815\n -pest\n NOTE: Questions or problems corurning thø above report should be dtected to the menagerof ute Unreeolved questlons Of problems wlth gorvices performed may be direcled ta Structurat\n Pest Cmtrol board at (916) 561-8708. or (BOO) 737-8188 m.pestboard.ca.goy\n 43M41 (Rev. 10101)\n',
            expectedResult: [{
                match: 'Detached porches',
                firstIndex: 1176,
                lastIndex: 1192,
                leadText: 'troi Act. ',
                tailText: ', detached',
                label: 'Report Sent To',
                meta: {value: 'Detached porches'},
                type: 'Custom',
                isValid: true
            }]
        },
        {
            description: ' - should find custom regex',
            types: ["Custom"],
            custom: [{
                regexes: [{
                    regex: "/organism(\\w+)/gmi"
                }]
            }],
            text: 'WOOD DESTROYh.G PESTS AND ORGANISMS\n SPECTION REPORT\n BUILDING NO.\n 13274\n STREET. CITY, STATE, ZIP\n Sundance Avenue, San Diego CA\n 92129\n Date of Inspection\n 7/29/2013\n Escrow NO.\n Report Sent To:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn; eddie@brokereddie.com\n 858-525-3258\n 858-876-1922\n REINSPECTION REPORT O\n No. ot Pages\n 6\n KENNEDY PEST CONTROL, INC.\n 1518 Sterling Court, Escondido, CA 92029\n Tel: (800) 420-7378 (760) 233-2090 Fax\n Firm Registration No. PR 1932\n Ordered By:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn: eddie@brokereddie.com\n 858-525-3258 858-876-1922\n COMPLETE REPORT\n General Description:\n Report No. 26519\n : Property Owner/Patty of Interest\n Ben Reginier\n 13274 Sundance Avenue\n San Diego, CA 92129\n Attn: bregnier@gmail.com\n • 310-902-0714\n LIMITED REPORT CJ\n SUPPLEMENTAL REPORT\n Inspection Tag Posted:\n Occupied and fumished one-storyt single family residence with attached garage and Garage\n garage.\n Other Tags Posted:\n None noted\n An inspection has been made to the struäure(s) shown on ihe diagram in accordance wiih thd Structural Pest Controi Act. Detached porches, detached\n steps, detached decks end any other structures not on the diagram were not inspected.\n Subterranean Termites C]\n Drywood Termites [3 Fungus/Dryrot O Other Findings O\n Further Inspection C]\n If any of.above boxes are checked it indicates that there were visible problems In accessb!e areas. for details on itemg.„\n Inspected by Richard Martinez\n 274\n State License No. FR 8762\n Signature\n You are entibd to obteln copies of all tepons and completion noUees on this property reported to the Structural Pest Control Board durh9 the pnceeding two yean. To obleh copies contact: S\n Control Board. 2005 Evergreen Strut, Suite 1500. Sanmento. Callfomia 95815\n -pest\n NOTE: Questions or problems corurning thø above report should be dtected to the menagerof ute Unreeolved questlons Of problems wlth gorvices performed may be direcled ta Structurat\n Pest Cmtrol board at (916) 561-8708. or (BOO) 737-8188 m.pestboard.ca.goy\n 43M41 (Rev. 10101)\n',
            expectedResult: [{
                match: 'ORGANISMS',
                firstIndex: 26,
                lastIndex: 35,
                leadText: 'PESTS AND ',
                tailText: '\n SPECTION',
                meta: {value: 'ORGANISMS'},
                type: 'Custom',
                isValid: true
            }]
        },
        {
            description: ' - should find custom regex',
            types: ["Custom"],
            custom: [{
                regexes: [
                    {
                        regex: "/pest(\\w+)/gmi"
                    },
                    {
                        regex: "/cit(\\w+)/gmi"
                    }
                ]
            }],
            text: 'WOOD DESTROYh.G PESTS AND ORGANISMS\n SPECTION REPORT\n BUILDING NO.\n 13274\n STREET. CITY, STATE, ZIP\n Sundance Avenue, San Diego CA\n 92129\n Date of Inspection\n 7/29/2013\n Escrow NO.\n Report Sent To:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn; eddie@brokereddie.com\n 858-525-3258\n 858-876-1922\n REINSPECTION REPORT O\n No. ot Pages\n 6\n KENNEDY PEST CONTROL, INC.\n 1518 Sterling Court, Escondido, CA 92029\n Tel: (800) 420-7378 (760) 233-2090 Fax\n Firm Registration No. PR 1932\n Ordered By:\n Coast Valley Properties-Eddie\n 216 North Coast Highway 101\n Encinitas, CA 92024\n Attn: eddie@brokereddie.com\n 858-525-3258 858-876-1922\n COMPLETE REPORT\n General Description:\n Report No. 26519\n : Property Owner/Patty of Interest\n Ben Reginier\n 13274 Sundance Avenue\n San Diego, CA 92129\n Attn: bregnier@gmail.com\n • 310-902-0714\n LIMITED REPORT CJ\n SUPPLEMENTAL REPORT\n Inspection Tag Posted:\n Occupied and fumished one-storyt single family residence with attached garage and Garage\n garage.\n Other Tags Posted:\n None noted\n An inspection has been made to the struäure(s) shown on ihe diagram in accordance wiih thd Structural Pest Controi Act. Detached porches, detached\n steps, detached decks end any other structures not on the diagram were not inspected.\n Subterranean Termites C]\n Drywood Termites [3 Fungus/Dryrot O Other Findings O\n Further Inspection C]\n If any of.above boxes are checked it indicates that there were visible problems In accessb!e areas. for details on itemg.„\n Inspected by Richard Martinez\n 274\n State License No. FR 8762\n Signature\n You are entibd to obteln copies of all tepons and completion noUees on this property reported to the Structural Pest Control Board durh9 the pnceeding two yean. To obleh copies contact: S\n Control Board. 2005 Evergreen Strut, Suite 1500. Sanmento. Callfomia 95815\n -pest\n NOTE: Questions or problems corurning thø above report should be dtected to the menagerof ute Unreeolved questlons Of problems wlth gorvices performed may be direcled ta Structurat\n Pest Cmtrol board at (916) 561-8708. or (BOO) 737-8188 m.pestboard.ca.goy\n 43M41 (Rev. 10101)\n',
            expectedResult: [{
                match: 'PESTS',
                firstIndex: 16,
                lastIndex: 21,
                leadText: 'ESTROYh.G ',
                tailText: ' AND ORGAN',
                meta: {value: 'PESTS'},
                type: 'Custom',
                isValid: true
            },
                {
                    match: 'pestboard',
                    firstIndex: 2103,
                    lastIndex: 2112,
                    leadText: '37-8188 m.',
                    tailText: '.ca.goy\n 4',
                    label: 'Report Sent To',
                    meta: {value: 'pestboard'},
                    type: 'Custom',
                    isValid: true
                },
                {
                    match: 'CITY',
                    firstIndex: 83,
                    lastIndex: 87,
                    leadText: '\n STREET. ',
                    tailText: ', STATE, Z',
                    meta: {value: 'CITY'},
                    type: 'Custom',
                    isValid: true
                }]
        }],
};

const tests = [
	validCustomEntitiesTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
const validProductNumbersTests = {
    title: "ProductNumbers valid cases",
    items: [{
        description: " - should find and validate a ISBN like ISBN 978-0-596-52068-7",
        types: ["ProductNumbers"],
        text: "Hey buy this ISBN 978-0-596-52068-7 at the store",
        expectedResult: [{
            match: '978-0-596-52068-7',
            firstIndex: 18,
            lastIndex: 35,
            leadText: 'this ISBN ',
            tailText: ' at the st',
            meta:
                {
                    isIsbn10: false,
                    isIsbn13: true,
                    isbn10: '0596520689',
                    isbn10h: '0-596-52068-9',
                    isbn13: '9780596520687',
                    isbn13h: '978-0-596-52068-7'
                },
            type: 'ProductNumbers',
            isValid: true
        }]
    },
        {
            description: " - should find and validate a EAN like EAN 73513537",
            types: ["ProductNumbers"],
            text: "Hey buy this EAN 73513537 at the store",
            expectedResult: [{
                match: '73513537',
                firstIndex: 17,
                lastIndex: 25,
                leadText: ' this EAN ',
                tailText: ' at the st',
                meta: {barcode: '73513537'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },
        {
            description: " - should find and validate a EAN like EAN4006381333931",
            types: ["ProductNumbers"],
            text: "Hey buy this EAN4006381333931 at the store",
            expectedResult: [{
                match: 'EAN4006381333931',
                firstIndex: 13,
                lastIndex: 29,
                leadText: ' buy this ',
                tailText: ' at the st',
                meta: {barcode: 'EAN4006381333931'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },
        {
            description: " - should find and validate barcode like UPC 607710059238",
            types: ["ProductNumbers"],
            text: "Hey buy this UPC 607710059238 at the store",
            expectedResult: [{
                match: '607710059238',
                firstIndex: 17,
                lastIndex: 29,
                leadText: ' this UPC ',
                tailText: ' at the st',
                meta: {barcode: '607710059238'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },
        {
            description: " - should find and validate barcode like UPC607710059238",
            types: ["ProductNumbers"],
            text: "Hey buy this UPC607710059238 at the store",
            expectedResult: [{
                match: 'UPC607710059238',
                firstIndex: 13,
                lastIndex: 28,
                leadText: ' buy this ',
                tailText: ' at the st',
                meta: {barcode: 'UPC607710059238'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },
        {
            description: " - should find and validate ASIN like B074VG2Q3Y",
            types: ["ProductNumbers"],
            text: "Hey buy this B074VG2Q3Y at the store",
            expectedResult: [{
                match: 'B074VG2Q3Y',
                firstIndex: 13,
                lastIndex: 23,
                leadText: ' buy this ',
                tailText: ' at the st',
                meta: {ASIN: 'B074VG2Q3Y'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },
        {
            description: " - should find and validate ASIN like INSPE3CTIO",
            types: ["ProductNumbers"],
            text: "Hey buy this INSPE3CTIO at the store",
            expectedResult: [{
                match: 'INSPE3CTIO',
                firstIndex: 13,
                lastIndex: 23,
                leadText: ' buy this ',
                tailText: ' at the st',
                meta: {ASIN: 'INSPE3CTIO'},
                type: 'ProductNumbers',
                isValid: true
            }]
        },

    ]
};





const invalidProductNumbersTests = {
    title: "ProductNumbers invalid cases",
    items: [
        {
            description: " - should NOT find and validate a ProductNumbers like alex@me.com",
            types: ["ProductNumbers"],
            text: "Hey follow me alex@me.com on twitter",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a EAN like 12345678",
            types: ["ProductNumbers"],
            text: "Hey this EAN 12345678 is not legit.",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a ASIN like B074VG2Q3",
            types: ["ProductNumbers"],
            text: "Hey this ASIN B074VG2Q3 is not legit.",
            expectedResult: []
        },
        {
            description: " - should NOT find and validate a ASIN like INSPECTION",
            types: ["ProductNumbers"],
            text: "Hey this ASIN INSPECTION is not legit.",
            expectedResult: []
        },
    ]


};


const tests = [
    validProductNumbersTests,
    invalidProductNumbersTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
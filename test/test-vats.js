const validVATTests = {
    title: "VAT valid cases",
    items: [{
        description: " - should find and validate a VAT like ATU00000024",
        types: ["VAT"],
        text: "Hey go to ATU00000024 let me know",
        expectedResult: [{
            match: "ATU00000024",
            firstIndex: 10,
            lastIndex: 21,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {
                country: {
                    isoCode: {
                        long: "AUT",
                        numeric: "040",
                        short: "AT",
                    },
                    name: "Austria"
                }
            },
            type: "VAT",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a VAT like BE0414445663",
        types: ["VAT"],
        text: "Hey go to BE0414445663 let me know",
        expectedResult: [{
            match: "BE0414445663",
            firstIndex: 10,
            lastIndex: 22,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {
                country: {
                    isoCode: {
                        long: "BEL",
                        numeric: "056",
                        short: "BE",
                    },
                    name: "Belgium"
                }
            },
            type: "VAT",
            isValid: true
        }]
    }
    ]
};




const invalidVATTests = {
    title: "VAT invalid cases",
    items: [{
        description: " - should NOT find and validate a VAT like alex@me.com",
        types: ["VAT"],
        text: "Hey follow me alex@me.com on twitter",
        expectedResult: []
    }, ]
};


const tests = [
    validVATTests,
    invalidVATTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
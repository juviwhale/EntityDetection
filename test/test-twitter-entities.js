const validTwitterTests = {
    title: "Twitter valid cases",
    items: [{
        description: " - should find and validate a twitter like @joe",
        types: ["Twitter"],
        text: "Hey follow me @joe on twitter",
        expectedResult: [{
            match: "@joe",
            firstIndex: 14,
            lastIndex: 18,
            leadText: "follow me ",
            tailText: " on twitte",
            meta: { url: "twitter.com/joe", username: "joe" },
            type: "Twitter",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a twitter like @joe.",
        types: ["Twitter"],
        text: "Hey follow me @joe. on twitter",
        expectedResult: [{
            match: "@joe",
            firstIndex: 14,
            lastIndex: 18,
            leadText: "follow me ",
            tailText: ". on twitt",
            meta: { url: "twitter.com/joe", username: "joe" },
            type: "Twitter",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a twitter like @joe3",
        types: ["Twitter"],
        text: "Hey follow me @joe3 on twitter",
        expectedResult: [{
            match: "@joe3",
            firstIndex: 14,
            lastIndex: 19,
            leadText: "follow me ",
            tailText: " on twitte",
            meta: { url: "twitter.com/joe3", username: "joe3" },
            type: "Twitter",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a twitter like @joe_mama",
        types: ["Twitter"],
        text: "Hey follow me @joe_mama on twitter",
        expectedResult: [{
            match: "@joe_mama",
            firstIndex: 14,
            lastIndex: 23,
            leadText: "follow me ",
            tailText: " on twitte",
            meta: { url: "twitter.com/joe_mama", username: "joe_mama" },
            type: "Twitter",
            isValid: true
        }]
    }
    ]
};


const invalidTwitterTests = {
    title: "Twitter invalid cases",
    items: [{
        description: " - should NOT find and validate a twitter like alex@me.com",
        types: ["Twitter"],
        text: "Hey follow me alex@me.com on twitter",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a twitter like @joe.smith",
        types: ["Twitter"],
        text: "Hey follow me @joe.smith on twitter",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a twitter like @joe-smith",
        types: ["Twitter"],
        text: "Hey follow me @joe.smith on twitter",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a twitter like @averyveryveryverylongusername",
        types: ["Twitter"],
        text: "Hey follow me @averyveryveryverylongusername on twitter",
        expectedResult: []
    }
    ]
};


const tests = [
    validTwitterTests,
    invalidTwitterTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
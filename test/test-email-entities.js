const validEmailTests = {
    title: "Email valid cases",
    items: [{
        description: " - should find and validate a email like alex@me.com",
        types: ["Email"],
        text: "Hey email me at alex@me.com tonight",
        expectedResult: [{
            match: "alex@me.com",
            firstIndex: 16,
            lastIndex: 27,
            leadText: "ail me at ",
            tailText: " tonight",
            meta: { domain: "me.com" },
            type: "Email",
            isValid: true
        }]
    },
    {
        description: " - should find and validate a email like tom.smith@orle.com",
        types: ["Email"],
        text: "Hey email me at tom.smith@orle.com tonight",
        expectedResult: [{
            match: "tom.smith@orle.com",
            firstIndex: 16,
            lastIndex: 34,
            leadText: "ail me at ",
            tailText: " tonight",
            meta: { domain: "orle.com" },
            type: "Email",
            isValid: true
        }]
    },
    ]
};




const invalidEmailTests = {
    title: "Email invalid cases",
    items: [{
        description: " - should NOT find and validate a email like alex@me",
        types: ["Email"],
        text: "Hey email me at alex@me tonight",
        expectedResult: []
    }]
};

const tests = [
    validEmailTests,
    invalidEmailTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
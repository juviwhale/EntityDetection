const validTests = {
    title: "Username valid cases",
    items: [{
        description: " - should find and validate a Username like username: joey@google.com",
        types: ["Username"],
        text: "Your username: joey@google.com We hope you and your team enjoy",
        expectedResult:[{
                "firstIndex": 5,
                "isValid": true,
                "lastIndex": 30,
                "leadText": "Your ",
                "match": "username: joey@google.com",
                "meta": {
                    "username": "joey@google.com",
                },
                "tailText": " We hope y",
                "type": "Username",
            }]
    },
        {
            description: " - should find and validate a Username like Username: AHJSJ738594GD3JG or Username: temporaryUsername",
            types: ["Username"],
            text: "Hey your Username: AHJSJ738594GD3JG or Username: temporaryUsername is ready.",
            expectedResult: [
                {
                    "firstIndex": 9,
                    "isValid": true,
                    "lastIndex": 35,
                    "leadText": "Hey your ",
                    "match": "Username: AHJSJ738594GD3JG",
                    "meta": {
                        "username": "AHJSJ738594GD3JG",
                    },
                    "tailText": " or Userna",
                    "type": "Username",
                },
                {
                    "firstIndex": 39,
                    "isValid": true,
                    "lastIndex": 66,
                    "leadText": "4GD3JG or ",
                    "match": "Username: temporaryUsername",
                    "meta": {
                        "username": "temporaryUsername",
                    },
                    "tailText": " is ready.",
                    "type": "Username",
                }
            ]

        },
        {
            description: " - should find and validate a Username like username: \"joey5\"",
            types: ["Username"],
            text: "username \"joey5\" is ready",
            expectedResult: [
                {
                    "firstIndex": 0,
                    "isValid": true,
                    "lastIndex": 16,
                    "leadText": "",
                    "match": "username \"joey5\"",
                    "meta": {
                        "username": "joey5\"",
                    },
                    "tailText": " is ready",
                    "type": "Username",
                }
            ]
        },
    ]
};




const invalidTests = {
    title: "Username invalid cases",
    items: [{
        description: " - should not find AHJSJ738594GD3JG",
        types: ["Username"],
        text: "Hey AHJSJ738594GD3JG is ready.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "username: joey@google.com.a",
    "Your username: joey@google.com We",
    "username: asdffd",
    "username:asdff",
    "username:asdff.",
    "username:asdff...?",
    "username: asdff!",
    "username => asdff",
    "username \"blahb\"",
    "username: hello.!",
    "asdf username: hello, asdf",
    "un: amyjj",
    "un: asdff",
    "username: goooo",
    "username: goooo.afsd+lfkf df",
    "username: goooo. afsdlfkf df",
    "a un: asdfsfd a",
    "a un: asdfsadf asf",
    "a username: asdfasdf? asdf",
    "username is suppper",
    "username == asdfdsf",
    "Your username: joey@email.com We hope y",
    "username: joeysm password: sadfgh",
    "un is asdfffff",
    "username: blahf_-=03fafad",
    "username: joey@google.com",
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate a Username ${element}`,
        types: ["Username"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "user: user123", // removing user",
    "username blahf", // forcing : or other matching symbols",
    "username blahf_-=03fafad",
    "a person's username should be unique",
    "username field.",
    "username: password:",
    "Username: Password:",
    "username:password",
    "user: https://groups	",
    "User::load(1)->get",
    "un asdfsadf",
    "usr asdfasdf",
    "un-biased asdfdddd",
    "user=username=asldfjsal;kdjf",
    "un: amyj",
    "how one user cut costs by",
    "username blah",
    "user-friendly",
    "un-friendly",
    "user-group-speaker-guidlines",
    "un matched",
    "user superpunched another",
    "username is a man",
    "user is a match",
    "username' specific",
    "user's specific",
    "user is unseen when",
    "asdf asdfasusR is a registered",
    "fun is to blah",
    "how one user cut costs by",
    "user blah",
    "user-friendly",
    "un-asdf",
    "username then \\ using stop word validation",
    "un:\nunsadflkdf",
    "username: http://www.google.com/register",
    "username: (http://www.google.com/register)",
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find and validate ${element}`,
        types: ["Username"],
        text: `Test this: ${element}`,
        expectedResult: []
    });
});

const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
const validPasswordTests = {
    title: "Password valid cases",
    items: [{
        description: " - should find and validate a Password like password: 1WF6uYTdgpmyt$Uz",
        types: ["Password"],
        text: "password: 1WF6uYTdgpmyt$Uz is ready",
        expectedResult: [
            {
                "firstIndex": 0,
                "isValid": true,
                "lastIndex": 26,
                "leadText": "",
                "match": "password: 1WF6uYTdgpmyt$Uz",
                "meta": {
                    "password": "1WF6uYTdgpmyt$Uz",
                },
                "tailText": " is ready",
                "type": "Password",
            }
        ]
    },
        {
            description: " - should find and validate a Password like password: AHJSJ738594GD3JG or password: temporaryPassword",
            types: ["Password"],
            text: "Hey your pwd: AHJSJ738594GD3JG or password: temporaryPassword is ready.",
            expectedResult: [
                {
                    "firstIndex": 9,
                    "isValid": true,
                    "lastIndex": 30,
                    "leadText": "Hey your ",
                    "match": "pwd: AHJSJ738594GD3JG",
                    "meta": {
                        "password": "AHJSJ738594GD3JG",
                    },
                    "tailText": " or passwo",
                    "type": "Password",
                },
                {
                    "firstIndex": 34,
                    "isValid": true,
                    "lastIndex": 61,
                    "leadText": "4GD3JG or ",
                    "match": "password: temporaryPassword",
                    "meta": {
                        "password": "temporaryPassword",
                    },
                    "tailText": " is ready.",
                    "type": "Password",
                }
            ]

        }]
};




const invalidPasswordTests = {
    title: "Password invalid cases",
    items: [{
        description: " - should not find AHJSJ738594GD3JG",
        types: ["Password"],
        text: "Hey AHJSJ738594GD3JG is ready.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "a password: 1WF6uYTdgpmyt$Uz and then",
    "a Password:1WF6uYTdgpmyt$Uz fasd",
    "a pwd: 1WF6uYTdgpmyt$Uz",
    "a password \'1WF6uYT\'dgpmytUz\'",
    "2. password \"123456\" asdf",
    "2. password `123456` asdf",
    "2. password \'123456\'",
    "a Password is 123445.",
    "a pwd is 123455",
    "password => 123455",
    "password = 123455",
    "pin: 123456",
    "pin: 123455",
    "password: @.-_?!5123",
    "password: 123456.",
    "pin code: 123455",
    "pin: 123456",
    "password: 123467",
    "-- pwd:affsfffF",
    "-- pwd: ffafdfdfdD",
    "password 123456",
    "password helPme",
    "your new password will be: 2ZZGFFAA)gwv5P!'Q!@#$%^&*()<V+;:\?lg2WPiPQ3?k enjoy.",
    "your new password is now: 2ZZGFFAA)gwv5P!'Q!@#$%^&*()<V+;:\?lg2WPiPQ3?k enjoy.",
    "password managerz!!" // double punctuation, could possibly be a password

];

shouldMatchArray.forEach((element) => {
    validPasswordTests.items.push({
        description: ` - should find and validate potential password ${element}`,
        types: ["Password"],
        text: `Hey your ${element}`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "password: 123",
    "Password:123",
    "pwd: abc",
    "pwd is 123",
    "pin: 123",
    "code: ASDF",
    "password-something",
    "pin code: 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",
    "i want pass across",
    // get's corrected by simple word validator:
    "-- pwd:affszzzzzzzzzzzzzzz",
    "-- pwd: ffafdfdfd",
    "-- pass: asfdfg",
    "-- i want pass across",
    "password hacker",
    // removed "pass"
    "-- pass: asfdfgG",
    "-- i want pass acrossS",
    "a 1.pass: 1WF6uYTdgpmyt$Uz",
    "a pass: 1WF6uYTdgpmytUz",
    "password: http://www.google.com/register",
    "password: (http://www.google.com/register)",
    "password managerz", // basic word
    "password manager", // dictionary word
    "password managers", // dictionary word
    "password managers?", // both dictionary and basic word with punctuation
    "password hello?", // basic word with punctuation
];

shouldNotMatchArray.forEach((element) => {
    invalidPasswordTests.items.push({
        description: ` - should NOT find and validate password ${element}`,
        types: ["Password"],
        text: `test case: ${element}`,
        expectedResult: []
    });
});


const tests = [
    validPasswordTests,
    invalidPasswordTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
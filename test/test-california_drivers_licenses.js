const validCaliforniaDriversLicenseTests = {
    title: "CaliforniaDriversLicense valid cases",
    items: [{
        description: " - should find and validate a CaliforniaDriversLicense like B8635645",
        types: ["CaliforniaDriversLicense"],
        text: "Hey go to B8635645 let me know",
        expectedResult: [{ match: "B8635645", firstIndex: 10, lastIndex: 18, leadText: "Hey go to ", tailText: " let me kn", meta: {}, type: "CaliforniaDriversLicense", isValid: true }]
    }]
};




const invalidCaliforniaDriversLicenseTests = {
    title: "CaliforniaDriversLicense invalid cases",
    items: [{
        description: " - should not find and validate a CaliforniaDriversLicense like b8635645",
        types: ["CaliforniaDriversLicense"],
        text: "Hey go to b8635645 let me know",
        expectedResult: []
    },
    {
        description: " - should not find and validate a CaliforniaDriversLicense like asdf- F7890839-asdf",
        types: ["CaliforniaDriversLicense"],
        text: "Hey asdf- F7890839-asdf",
        expectedResult: []
    },
    {
        description: " - should not find and validate a CaliforniaDriversLicense like asdf- F7890839-asdf",
        types: ["CaliforniaDriversLicense"],
        text: "Hey asdf-F7890839-asdf",
        expectedResult: []
    }]
};


const tests = [
    validCaliforniaDriversLicenseTests,
    invalidCaliforniaDriversLicenseTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
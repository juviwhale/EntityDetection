const validTests = {
    title: "Valid UkZipCodes test cases",
    items: [{
        description: " - should find and validate a US Zip Codes like: Address: 83 Aston St, Birmingham B4 7DA, UK",
        types: ["UkZipCodes"],
        text: "I live in Address: 83 Aston St, Birmingham B4 7DA, UK.",
        expectedResult: [
            {
                "firstIndex": 43,
                "isValid": true,
                "lastIndex": 49,
                "leadText": "irmingham ",
                "match": "B4 7DA",
                "meta": {},
                "tailText": ", UK.",
                "type": "UkZipCodes",
            }
        ]
    },
        {
            description: " - should find and validate a US Zip Codes like: 124 Park Row\nEDLASTON\nDE6 2TT",
            types: ["UkZipCodes"],
            text: "124 Park Row\nEDLASTON\nDE6 2TT",
            expectedResult: [
                {
                    "firstIndex": 22,
                    "isValid": true,
                    "lastIndex": 29,
                    "leadText": "\nEDLASTON\n",
                    "match": "DE6 2TT",
                    "meta": {},
                    "tailText": "",
                    "type": "UkZipCodes",
                }
            ]

        }]
};




const invalidTests = {
    title: "Inalid UkZipCodes test cases",
    items: [{
        description: " - should not find 123456 ",
        types: ["UkZipCodes"],
        text: "I live in Nowhere Place, Maryland, 11115.",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    "BD23 3XH",
    "HARKSTEAD IP9 6QB",
    "TWYWELL NN14 1SZ",
    "ILLINGWORTH HX2 8YU",
    "CB5 3SG COMMERCIAL END",
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate potential ${element}`,
        types: ["UkZipCodes"],
        text: `I live in ${element}.`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    "11115 Menomonee Falls, WI",
    "543211 New Albany, IN",
    "1234 Reisterstown, MD",
    "11 Roseville, MI",
    "26633 Athens, GA",
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find and validate ${element}`,
        types: ["UkZipCodes"],
        text: `I live in ${element}.`,
        expectedResult: []
    });
});


const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
"use strict";

const entities = require("../lib/entities"),
    chai = require("chai");

chai.use(require("chai-things"));

describe("get_grouped() method", function() {

    //it.only('base test', function() {
    it("base test", function() {
        //expect(test_line.potentialZipCode).to.be.ok;
        var text = "B8635645";
        const result = entities.get_grouped(text);

        result.should.deep.equal([{
            entities: [{
                match: "B8635645",
                firstIndex: 0,
                lastIndex: 8,
                leadText: "",
                tailText: "",
                meta: {},
                type: "CaliforniaDriversLicense",
                isValid: true
            },
            {
                match: "B8635645",
                firstIndex: 0,
                lastIndex: 8,
                leadText: "",
                tailText: "",
                meta: {},
                type: "PotentialID",
                isValid: true
            }
            ],
            child_types: [],
            match: "B8635645",
            type: "CaliforniaDriversLicense",
            types: ["CaliforniaDriversLicense", "PotentialID"],
            firstIndex: 0,
            lastIndex: 8,
            leadText: "",
            tailText: "",
            meta: { PotentialID: {}, CaliforniaDriversLicense: {} },
            isValid: true
        }]);
    });

    it("phone number test", function() {
        //expect(test_line.potentialZipCode).to.be.ok;
        //var text = "If you have any questions about your statement, please call us at 816-234-2265 ";
        var text = " 816-234-2265 ";
        const results = entities.get_grouped(text);

        results.should.deep.equal([{
            entities: [{
                type: "PhoneNumber",
                match: "816-234-2265",
                isValid: true,
                firstIndex: 1,
                lastIndex: 13,
                meta: {
                    international: "+1 816-234-2265",
                    e164: "+18162342265",
                    possibility: "is-possible",
                    countryCode: 1,
                    regionCode: "US"
                }
            },
            {
                match: "816-234-2265",
                firstIndex: 1,
                lastIndex: 13,
                leadText: " ",
                tailText: " ",
                meta: {},
                type: "PotentialID",
                isValid: true
            }
            ],
            child_types: [],
            match: "816-234-2265",
            type: "PhoneNumber",
            types: ["PhoneNumber", "PotentialID"],
            firstIndex: 1,
            lastIndex: 13,
            meta: {
                PhoneNumber: {
                    international: "+1 816-234-2265",
                    e164: "+18162342265",
                    possibility: "is-possible",
                    countryCode: 1,
                    regionCode: "US"
                },
                PotentialID: {}
            },
            isValid: true
        }]);
    });

});
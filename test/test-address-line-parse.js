"use strict";

var chai = require("chai");
var expect = chai.expect; // we are using the "expect" style of Chai
var TextLine = require("../helpers/line-parsing").TextLine;
var AddressParser = require("../helpers/line-parsing").AddressParser;

describe("TextLine for Zipcodes", function() {

    // POSITIVE ZIP CODES
    it("potentialZipCode() works for valid cases - foo 90210 foo", function() {
        var test_line = new TextLine("foo 90210 foo");
        expect(test_line.potentialZipCode).to.be.ok;
    });

    it("potentialZipCode() works for valid cases - 90210 foo", function() {
        var test_line = new TextLine("90210 foo");
        expect(test_line.potentialZipCode).to.be.ok;
    });

    it("potentialZipCode() works for valid cases - asdf 90210", function() {
        var test_line = new TextLine("asdf 90210");
        expect(test_line.potentialZipCode).to.be.ok;
    });

    it("potentialZipCode() works for valid cases - asdf 90210-3746", function() {
        var test_line = new TextLine("asdf 90210-3746");
        expect(test_line.potentialZipCode).to.be.ok;
    });

    // NEGATIVE ZIP CODES
    it("potentialZipCode() works for invalid cases - foo 9021 foo", function() {
        var test_line = new TextLine("foo 9021 foo");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

    it("potentialZipCode() works for invalid cases - foo90210 foo", function() {
        var test_line = new TextLine("foo90210 foo");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

    it("potentialZipCode() works for invalid cases - foo 90210. foo", function() {
        var test_line = new TextLine("foo 90210. foo");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

    it("potentialZipCode() works for invalid cases - foo .90210 foo", function() {
        var test_line = new TextLine("foo .90210 foo");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

    it("potentialZipCode() works for invalid cases - foo 90210-", function() {
        var test_line = new TextLine("foo 90210-");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

    it("potentialZipCode() works for invalid cases - Code 54954.2 – No action or discussion shall be undertaken on any item not appearing on the", function() {
        var test_line = new TextLine("Code 54954.2 – No action or discussion shall be undertaken on any item not appearing on the");
        expect(test_line.potentialZipCode).to.not.be.ok;
    });

});

describe("STATE ABBREVIATION", function() {

    // POSITIVE STATE ABBREVIATIONS
    it("potentialStateAbbr() works for valid cases - foo CA foo", function() {
        var test_line = new TextLine("foo CA foo");
        expect(test_line.potentialStateAbbr).to.be.ok;
    });

    it("potentialStateAbbr() works for valid cases - foo,CA foo", function() {
        var test_line = new TextLine("foo,CA foo");
        expect(test_line.potentialStateAbbr).to.be.ok;
    });


    // NEGATIVE STATE ABBREVIATIONS
    it("potentialStateAbbr() works for invalid cases - foo-CA foo", function() {
        var test_line = new TextLine("foo-CA foo");
        expect(test_line.potentialStateAbbr).to.not.be.ok;
    });

    it("potentialStateAbbr() works for invalid cases - foo CA- foo", function() {
        var test_line = new TextLine("foo CA- foo");
        expect(test_line.potentialStateAbbr).to.not.be.ok;
    });

    it("potentialStateAbbr() works for invalid cases - foo CA90021 foo", function() {
        var test_line = new TextLine("foo CA90021 foo");
        expect(test_line.potentialStateAbbr).to.not.be.ok;
    });

});

describe("STATE NAMES", function() {

    // POSITIVE STATE NAMES
    it("potentialStateName() works for valid cases - fun Arkansas today", function() {
        var test_line = new TextLine("fun Arkansas today");
        expect(test_line.potentialStateName).to.be.ok;
    });

    it("potentialStateName() works for valid cases - fun arkansas today", function() {
        var test_line = new TextLine("fun arkansas today");
        expect(test_line.potentialStateName).to.be.ok;
    });

    it("potentialStateName() works for valid cases - fun NEW JERSEY today", function() {
        var test_line = new TextLine("fun arkansas today");
        expect(test_line.potentialStateName).to.be.ok;
    });

});

describe("STREET TYPES", function() {

    // POSITIVE STREET TYPES
    it("potentialStreetType() works for valid cases - Sutter St.", function() {
        var test_line = new TextLine("Sutter St.");
        expect(test_line.potentialStreetType).to.be.ok;
    });

    it("potentialStreetType() works for valid cases - 1097 BLANCHE ST, APT 309 ", function() {
        var test_line = new TextLine("1097 BLANCHE ST, APT 309 ");
        expect(test_line.potentialStreetType).to.be.ok;
    });

    it("potentialStreetType() works for valid cases - 2700 E FOOTHILL BLVD, STE 301 ", function() {
        var test_line = new TextLine("2700 E FOOTHILL BLVD, STE 301 ");
        expect(test_line.potentialStreetType).to.be.ok;
    });

    it("potentialStreetType() works for valid cases - 5617 Hollywood Blvd. in LA", function() {
        var test_line = new TextLine("5617 Hollywood Blvd. in LA");
        expect(test_line.potentialStreetType).to.be.ok;
    });

    it("potentialStreetType() works for valid cases - It is on Market Street in San Fran", function() {
        var test_line = new TextLine("It is on Market Street in San Fran");
        expect(test_line.potentialStreetType).to.be.ok;
    });

    // NEGATIVE STREET TYPES
    it("potentialStreetType() works for invalid cases - Sutter StSmither", function() {
        var test_line = new TextLine("Sutter Blvd-.Smither");
        expect(test_line.potentialStreetType).to.not.be.ok;
    });

    it("potentialStreetType() works for invalid cases - Welcome to the meeting of the Cupertino Union School District Board of Education. If you would like to address the Board during Public Comments", function() {
        var test_line = new TextLine("Welcome to the meeting of the Cupertino Union School District Board of Education. If you would like to address the Board during Public Comments");
        expect(test_line.potentialStreetType).to.not.be.ok;
    });

    it("potentialStreetType() works for invalid cases - Individuals who require special accommodation should contact the Superintendent’s Office at (408) 252-3000, ext. 61200 at least two business", function() {
        var test_line = new TextLine("Individuals who require special accommodation should contact the Superintendent’s Office at (408) 252-3000, ext. 61200 at least two business");
        expect(test_line.potentialStreetType).to.not.be.ok;
    });

    it("potentialStreetType() works for invalid cases - Hey come over to 5 Street today", function() {
        var test_line = new TextLine("Hey come over to 5 Street today");
        expect(test_line.potentialStreetType).to.not.be.ok;
    });

});

describe("PROPER WORDS", function() {

    // POSITIVE PROPER WORDS TYPES
    it("numberedProperWord() works for valid cases - 34 Elm St.", function() {
        var test_line = new TextLine("Sutter 34 Elm St.");
        expect(test_line.numberedProperWord).to.be.ok;
    });

    // NEGATIVE PROPER WORDS TYPES

    it("numberedProperWord() works for invalid cases - 34 elm St.", function() {
        var test_line = new TextLine("Sutter 34 elm St.");
        expect(test_line.numberedProperWord).to.not.be.ok;
    });

    it("numberedProperWord() works for invalid cases - -34 elm St.", function() {
        var test_line = new TextLine("Sutter -34 elm St.");
        expect(test_line.numberedProperWord).to.not.be.ok;
    });

});

describe("COUNTRY ABBR", function() {

    // POSITIVE COUNTRY ABBREVIATIONS
    it("potentialCountryAbbr() works for valid cases - foo U.S.A. foo", function() {
        var test_line = new TextLine("foo U.S.A. foo");
        expect(test_line.potentialCountryAbbr).to.be.ok;
    });

    it("potentialCountryAbbr() works for valid cases - foo USA foo", function() {
        var test_line = new TextLine("foo USA foo");
        expect(test_line.potentialCountryAbbr).to.be.ok;
    });

    it("potentialCountryAbbr() works for valid cases - foo USA. foo", function() {
        var test_line = new TextLine("foo USA. foo");
        expect(test_line.potentialCountryAbbr).to.be.ok;
    });

    it("potentialCountryAbbr() works for valid cases - foo CA foo", function() {
        var test_line = new TextLine("foo CA foo");
        expect(test_line.potentialCountryAbbr).to.be.ok;
    });

});

describe("POBoxes", function() {

    // POSITIVE POBOX TYPES
    it("potentialPOBox() works for valid cases - PO BOX 123 ANYTOWN, PA 10000", function() {
        var test_line = new TextLine("PO BOX 123 ANYTOWN, PA 10000");
        expect(test_line.potentialPOBox).to.be.ok;
    });

    var poBoxShouldMatchArray = [
        "PO BOX 123 ANYTOWN, PA 10000",
        "PO BOX 123 ANYTOWN-Hyphons, PA 10000",
        "PO BOX 123 ANYTOWN Multiple Words, PA 10000 3432 a sdf",
        "garbage in front: PO BOX 123 ANYTOWN Multiple Words, PA 10000 3432 garbage in end",
        "po box 3452",
        "PO BOX 2341",
        "P. O. Box",
        "P.O 123",
        "P.O. Box 123",
        "P.O. Box",
        "P....O.... Box....",
        "P   O   Box  #4444",
        "P   O   Box  44444",
        "P.O.B 123,",
        "P.O.B. 123",
        "P.O.B. ",
        "PO 123",
        "PO Box 234",
        "PO Box #234",
        "PO-Box 234",
        "POB 123",
        "Po Box",
        "Post 123",
        "Post Box 123",
        "Post Office Box 123",
        "Post Office Box #123",
        "p box 234",
        "p-o-box 234",
        "p.o box 234",
        "p.o. box 234",
        "p.o.b. #123",
        "p.o.b. 234",
        "p/o box 234",
        "po #123",
        "po box 123",
        "po-box 3452",
        "pobox123",
        "pobox 123",
        "pobox #123",
        "post office box 2342",
        "Post Office Box 2341",
        "Post Office Box #22341",
        "Post Office Box: 2341",
        "P O Box 2341"
    ];

    poBoxShouldMatchArray.forEach((element) => {
        it(`potentialPOBox() works for valid cases - ${element}`, function() {
            var test_line = new TextLine(element);
            expect(test_line.potentialPOBox).to.be.ok;
        });
    });

    var poBoxShouldNotMatchArray = [
        "The Postal Road",
        "Box Hill",
        "123 Some Street",
        "Controller's Office",
        "pollo St.",
        "123 box canyon rd",
        "777 Post Oak Blvd",
        "PSC 477 Box 396",
        "RR 1 Box 1020"
    ];
    poBoxShouldNotMatchArray.forEach((element) => {
        it(`potentialPOBox() works for invalid cases - ${element}`, function() {
            var test_line = new TextLine(element);
            expect(test_line.potentialPOBox).to.not.be.ok;
        });
    });


});

describe("No potential matches", function() {
    // NO MATCH CASES
    it("no matches for - Hey send me a letter at ", function() {
        var test_line = new TextLine("Hey send me a letter at ");
        //console.log("LOOK HERE:", test_line.potentialStateName);
        expect(test_line.potential_types).to.deep.equal([]);
    });
});

describe("Line Parsing", function() {
    // LINE SPLITTING

    it.skip("AddressParser will split lines correct - case 1", function() {
        let text = "Foo balls.\nKansas City MO 64106";
        let addressParser = AddressParser.fromText(text);
        expect(addressParser.lines.length).to.equal(2);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "Kansas City MO 64106",
            firstIndex: 12,
            lastIndex: 32,
            address: {
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            }
        });
    });

    it("AddressParser will split lines correct - case 2", function() {
        let text = "Hey send me a letter at \n1000 Walnut St.\n Kansas City MO 64106";
        let addressParser = AddressParser.fromText(text);
        expect(addressParser.lines.length).to.equal(3);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1000 Walnut St.\n Kansas City MO 64106",
            firstIndex: 25,
            lastIndex: 62,
            address: {
                number: "1000",
                street: "Walnut",
                type: "St",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            }
        });
    });

    it("AddressParser will split lines correct - case 3", function() {
        let text = "Hey send me a letter at \n1000 Walnut St. Kansas City MO 64106";
        let addressParser = AddressParser.fromText(text);
        expect(addressParser.lines.length).to.equal(2);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1000 Walnut St. Kansas City MO 64106",
            firstIndex: 25,
            lastIndex: 61,
            address: {
                number: "1000",
                street: "Walnut",
                type: "St",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            }
        });
    });

    it("AddressParser will split lines correct - case 4", function() {
        let text = "Hey send me a letter at \n1000 Walnut St.\n Kansas City MO\n 64106";
        let addressParser = AddressParser.fromText(text);
        expect(addressParser.lines.length).to.equal(4);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1000 Walnut St.\n Kansas City MO\n 64106",
            firstIndex: 25,
            lastIndex: 63,
            address: {
                number: "1000",
                street: "Walnut",
                type: "St",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            }
        });
    });

    it("AddressParser will split lines correct - case 3b", function() {
        let text = "Foo balls.\n No match here.";
        let addressParser = AddressParser.fromText(text);
        expect(addressParser.lines.length).to.equal(2);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(0);
    });


    it("AddressParser will split lines correct - case 5", function() {
        let text = "\n\nJEAN WANG \n\nAddress of Covered Property: \n1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106 \n\nHome Seller: \n\nTOMMY & MAGGIE TURNER \n\nCLOSING/ESCROW INFORMATION \n\n";
        let addressParser = AddressParser.fromText(text);

        expect(addressParser.lines.length).to.equal(7);
        expect(addressParser.lineGroups.length).to.equal(6);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106 ",
            address: {
                number: "1097",
                street: "BLANCHE",
                type: "ST",
                sec_unit_type: "APT",
                sec_unit_num: "309",
                city: "PASADENA",
                state: "CA",
                zip: "91106"
            },
            firstIndex: 44,
            lastIndex: 89
        });
    });


    it("AddressParser will split lines correct - case 6", function() {
        let text = "\n\nClosing Company (if available): \n\nSHAMROCK ESCROW \n2700 E FOOTHILL BLVD, STE 301 \nPASADENA, CA 91107 - 7108 \n\nClosing Agent or Attorney's Name: \n\n";
        let addressParser = AddressParser.fromText(text);

        expect(addressParser.lines.length).to.equal(5);
        expect(addressParser.lineGroups.length).to.equal(4);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "2700 E FOOTHILL BLVD, STE 301 \nPASADENA, CA 91107 - 7108 ",
            address: {
                number: "2700",
                prefix: "E",
                street: "FOOTHILL",
                type: "BLVD",
                sec_unit_type: "STE",
                sec_unit_num: "301",
                city: "PASADENA",
                state: "CA",
                zip: "91107"
            },
            firstIndex: 53,
            lastIndex: 110
        });
    });

    it("AddressParser will split lines correct - case 7", function() {
        let text = "\n\nJEAN WANG \n\nAddress of Covered Property: \n1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106 \n\nHome Seller: \n\nTOMMY & MAGGIE TURNER \n\nCLOSING/ESCROW INFORMATION \n\n";
        let addressParser = AddressParser.fromText(text);

        //addressParser.debug();
        expect(addressParser.lines.length).to.equal(7);
        expect(addressParser.lineGroups.length).to.equal(6);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1097 BLANCHE ST, APT 309 \nPASADENA, CA 91106 ",
            address: {
                number: "1097",
                street: "BLANCHE",
                type: "ST",
                sec_unit_type: "APT",
                sec_unit_num: "309",
                city: "PASADENA",
                state: "CA",
                zip: "91106"
            },
            firstIndex: 44,
            lastIndex: 89
        });
        let address = addressParser.addresses[0];
        expect(address.text).to.deep.equal(text.slice(address.firstIndex, address.lastIndex));
    });

    it("AddressParser will split lines correct - case 8", function() {
        let text = "1000 Walnut\n Kansas City MO 64106-3686\n Jane Customer";
        let addressParser = AddressParser.fromText(text);

        //addressParser.debug();
        expect(addressParser.lines.length).to.equal(3);
        expect(addressParser.lineGroups.length).to.equal(2);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "1000 Walnut\n Kansas City MO 64106-3686",
            address: {
                number: "1000",
                street: "Walnut",
                city: "Kansas City",
                state: "MO",
                zip: "64106"
            },
            firstIndex: 0,
            lastIndex: 38
        });
    });


    it("AddressParser will split lines correct - case 9", function() {
        let text = "Hey come over to 5 Street today";
        let addressParser = AddressParser.fromText(text);
        //addressParser.debug();

        //addressParser.debug();
        expect(addressParser.lines.length).to.equal(1);
        expect(addressParser.lineGroups.length).to.equal(1);
        expect(addressParser.addresses.length).to.equal(0);
    });


    it("AddressParser will split lines correct - case 9", function() {
        let text = "\nJohn N. Kitta & Associates \n39560 Stevenson Place, Ste 217 \nFremont, CA 94539 \nTelephone: (510) 797-7990 \n";
        let addressParser = AddressParser.fromText(text);
        //addressParser.debug();

        expect(addressParser.lines.length).to.equal(4);
        expect(addressParser.lineGroups.length).to.equal(3);
        expect(addressParser.addresses.length).to.equal(1);
        expect(addressParser.addresses[0]).to.deep.equal({
            text: "39560 Stevenson Place, Ste 217 \nFremont, CA 94539 ",
            address: {
                number: "39560",
                street: "Stevenson",
                type: "Place",
                sec_unit_type: "Ste",
                sec_unit_num: "217",
                city: "Fremont",
                state: "CA",
                zip: "94539"
            },
            firstIndex: 29,
            lastIndex: 79
        });
    });

    it("AddressParser will split lines correct - case 10", function() {
        let text = "THIS IS A SEPARATED REPORT WHICH IS DEFINED AS SECTION I/SECTION 2 CONDITIONS EVIDENT ON THE DATE OF INSPECTION. SECTION 1 CONTAINS ITEMS WHERE THERE IS VISIBLE EVIDENCE OF ACTIVE INFESTATION, INFECTION OR CONDITIONS THAT HAVE RESULTED IN OR FROM INFESTATION OR INFECTION. SECTION 2 ITEMS ARE CONDITIONS DEEMED LIKELY TO LEAD TO INFESTATION OR INFECTION, BUT WHERE NO VISIBLE EVIDENCE OF SUCH WAS FOUND. FURTHER INSPECTION ITEMS ARE DEFINED AS";
        let addressParser = AddressParser.fromText(text);
        //addressParser.debug();

        expect(addressParser.lines.length).to.equal(1);
        expect(addressParser.lineGroups.length).to.equal(1);
        expect(addressParser.addresses.length).to.equal(0);
    });

    it("AddressParser will split lines correct - case 11", function() {
        let text = " This is a Section 1 Item * ";
        let addressParser = AddressParser.fromText(text);
        //addressParser.debug();

        expect(addressParser.lines.length).to.equal(1);
        expect(addressParser.lineGroups.length).to.equal(1);
        expect(addressParser.addresses.length).to.equal(0);
    });


    it("AddressParser will split lines correct - case 12", function() {
        let text = "\n during the course of a normal inspection.\n KENNEDY PEST CONTROL, INC. - License No. PR 1932\n";
        let addressParser = AddressParser.fromText(text);
        //addressParser.debug();

        expect(addressParser.lines.length).to.equal(2);
        expect(addressParser.lineGroups.length).to.equal(1);
        expect(addressParser.addresses.length).to.equal(0);
    });














    // it('example of how to put it all together', function() {
    //     var regions = [{
    //             "lines": [
    //                 ["CUPERTINO", "UNION", "SCHOOL", "DISTRICT"]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["Board", "of", "Education"]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["Special", "Meeting"]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["April", "6,", "2016"],
    //                 ["7:30", "a.m."]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["AGENDA"]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["Cupertino", "Union", "School", "District"],
    //                 ["1309", "S.", "Mary", "Avenue"],
    //                 ["Sunnyvale,", "CA", "94087"]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["Welcome", "to", "the", "meeting", "of", "the", "Cupertino", "Union", "School", "District", "Board", "of", "Education.", "If", "you", "would", "like", "to", "address", "the", "Board", "during", "Public", "Comments"],
    //                 ["on", "any", "agenda", "item", "or", "any", "item", "not", "on", "the", "agenda,", "please", "fill", "out", "a", "comment", "card", "available", "in", "the", "hallway", "and", "give", "it", "to", "the", "Administrative"],
    //                 ["Assistant.", "You", "will", "be", "called", "on", "to", "comment", "during", "this", "time", "and", "comments", "will", "be", "limited", "to", "three", "(3)", "minutes.", "To", "ensure", "that", "all", "speakers", "are"],
    //                 ["provided", "an", "equal", "opportunity", "to", "address", "the", "Board", "during", "Public", "Comments,", "individual", "speakers", "may", "not", "“yield”", "their", "allotted", "time", "to", "address", "the"],
    //                 ["Board", "to", "other", "speakers.", "In", "addition,", "the", "Board", "may,", "in", "accordance", "with", "the", "Brown", "Act", "(section", "54954.3[b]", "of", "the", "Government", "Code),", "limit", "the", "total"],
    //                 ["amount", "of", "time", "allocated", "for", "comment", "on", "a", "particular", "issue.", "The", "Board", "may", "choose", "to", "respond", "to", "agenda", "item", "comments", "or", "reserve", "their"],
    //                 ["responses", "for", "discussion", "and", "action", "when", "the", "agenda", "item", "appears", "during", "the", "course", "of", "the", "meeting.", "It", "should", "be", "noted", "that", "the", "Board"],
    //                 ["discourages", "complaints", "against", "individual", "officers", "or", "employees", "of", "the", "District", "during", "open", "session."],
    //                 ["Individuals", "who", "require", "special", "accommodation", "should", "contact", "the", "Superintendent’s", "Office", "at", "(408)", "252-3000,", "ext.", "61200", "at", "least", "two", "business"],
    //                 ["days", "before", "the", "meeting", "date."]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["As", "a", "courtesy", "to", "others,", "please", "turn", "off", "your", "cell", "phone", "upon", "entering", "the", "meeting."]
    //             ]
    //         },
    //         {
    //             "lines": [
    //                 ["1.", "CALL", "TO", "ORDER/FLAG", "SALUTE"],
    //                 ["2.", "PUBLIC", "COMMENT", "–", "ITEMS", "ON", "THE", "AGENDA"],
    //                 ["The", "public", "may", "address", "the", "Board", "on", "any", "agenda", "item", "and", "any", "item", "not", "on", "the", "agenda.", "The"],
    //                 ["Board", "president", "will", "only", "call", "on", "those", "who", "have", "filled", "out", "comment", "cards", "before", "the", "meeting,", "and"],
    //                 ["each", "speaker", "will", "be", "allotted", "three", "(3)", "minutes.", "The", "Board", "will", "not", "respond", "or", "take", "action", "on", "any"],
    //                 ["non-agenda", "item", "comments", "at", "this", "time,", "although", "the", "item", "may", "be", "agendized", "at", "a", "later", "date", "(Ed."],
    //                 ["Code", "35145.5).", "The", "comments", "shall", "be", "made", "from", "the", "podium.", "In", "accordance", "with", "Government"],
    //                 ["Code", "54954.2", "–", "No", "action", "or", "discussion", "shall", "be", "undertaken", "on", "any", "item", "not", "appearing", "on", "the"],
    //                 ["posted", "agenda.", "The", "Board", "shall", "limit", "the", "total", "time", "to", "30", "minutes.", "Please", "refrain", "from", "any"],
    //                 ["response", "to", "the", "speaker", "including", "negative", "and", "positive", "reactions", "(booing", "and", "clapping).", "This", "will"],
    //                 ["ensure", "that", "all", "members", "of", "the", "public", "feel", "free", "to", "voice", "their", "opinions", "and", "concerns."],
    //                 ["`"],
    //                 ["3.", "CLOSED", "SESSION"],
    //                 ["By", "law,", "closed", "sessions", "are", "not", "open", "to", "the", "public.", "Anyone", "wishing", "to", "address", "the", "Board", "of"],
    //                 ["Education", "on", "closed", "session", "matters", "may", "do", "so", "during", "Agenda", "Item", "2", "–", "Public", "Comments"],
    //                 ["2.1", "Public", "Employee", "Performance", "Evaluation", "–", "Pursuant", "to", "Government", "Code"],
    //                 ["Section", "54957"],
    //                 ["District", "Representatives:", "Board", "President", "Josephine", "Lucey", "and"],
    //                 ["Vice-President", "Anjali", "Kausar"],
    //                 ["Unrepresented", "Employee:", "Superintendent", "Wendy", "Gudalewicz"],
    //                 ["4.", "REPORT", "FROM", "CLOSED", "SESSION"],
    //                 ["5.", "DISCUSSION"],
    //                 ["5.1", "Board", "Governance", "California"],
    //                 ["6.", "ADJOURNMENT"]
    //             ]
    //         }
    //     ];

    //     // merge lines to a single set of stings NOTE: not using regions at this time
    //     // E.G.
    //     // [ { text: 'CUPERTINO UNION SCHOOL DISTRICT' },
    //     //   { text: 'Board of Education' },
    //     //   ...
    //     // ]
    //     var lines = [].concat.apply([], regions.map(function(region) {
    //         return region['lines'].map(function(line) {
    //             return new TextLine(line.join(" "))
    //         });
    //     }));

    //     var addresses = new AddressParser(lines);
    //     //console.log(addresses)


    // });
});
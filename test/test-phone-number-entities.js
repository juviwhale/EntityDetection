const validPhoneTests = {
    title: "Phone numbers valid cases",
    items: [{
        description: " - should find and validate phone numbers in dash format",
        types: ["PhoneNumber"],
        text: "Hey call me at 415-898-5332 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "415-898-5332",
            isValid: true,
            firstIndex: 15,
            lastIndex: 27,
            meta: {
                international: "+1 415-898-5332",
                e164: "+14158985332",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number in US normal format",
        types: ["PhoneNumber"],
        text: "Hey call me at (415) 898-5332. when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "(415) 898-5332",
            isValid: true,
            firstIndex: 15,
            lastIndex: 29,
            meta: {
                international: "+1 415-898-5332",
                e164: "+14158985332",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 555-8768",
        types: ["PhoneNumber"],
        text: "Hey call me at 555-8768. when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "555-8768",
            isValid: true,
            firstIndex: 15,
            lastIndex: 23,
            meta: {
                international: "+1 5558768",
                e164: "+15558768",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like (800)876-2342",
        types: ["PhoneNumber"],
        text: "Hey call me at (800)876-2342 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "(800)876-2342",
            isValid: true,
            firstIndex: 15,
            lastIndex: 28,
            meta: {
                international: "+1 800-876-2342",
                e164: "+18008762342",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like +36(30) 409 787",
        types: ["PhoneNumber"],
        text: "Hey call me at +36(30) 409 787 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "+36(30) 409 787",
            isValid: true,
            firstIndex: 15,
            lastIndex: 30,
            meta: {
                international: "+36 30 409 787",
                e164: "+3630409787",
                possibility: "is-possible",
                countryCode: 36,
                regionCode: "HU"
            }
        }]
    },
    {
        description: " - should find and validate phone number like +386 51 342 561",
        types: ["PhoneNumber"],
        text: "Hey call me at +386 51 342 561 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "+386 51 342 561",
            isValid: true,
            firstIndex: 15,
            lastIndex: 30,
            meta: {
                international: "+386 51 342 561",
                e164: "+38651342561",
                possibility: "is-possible",
                countryCode: 386,
                regionCode: "SI"
            }
        }]
    },
    {
        description: " - should find and validate phone number like +41123456781",
        types: ["PhoneNumber"],
        text: "Hey call me at +41123456781 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "+41123456781",
            isValid: true,
            firstIndex: 15,
            lastIndex: 27,
            meta: {
                international: "+41 123456781",
                e164: "+41123456781",
                possibility: "is-possible",
                countryCode: 41,
                regionCode: "CH"
            }
        }]
    },
    {
        description: " - should find and validate phone number like (555) 555 - 5555",
        types: ["PhoneNumber"],
        text: "Hey call me at (555) 555 - 5555 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "(555) 555 - 5555",
            isValid: true,
            firstIndex: 15,
            lastIndex: 31,
            meta: {
                international: "+1 555-555-5555",
                e164: "+15555555555",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 1 (555) 555 - 5555",
        types: ["PhoneNumber"],
        text: "Hey call me at 1 (555) 555 - 5555 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "1 (555) 555 - 5555",
            isValid: true,
            firstIndex: 15,
            lastIndex: 33,
            meta: {
                international: "+1 555-555-5555",
                e164: "+15555555555",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like +1 (555) 555 - 5555",
        types: ["PhoneNumber"],
        text: "Hey call me at +1 (555) 555 - 5555 when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "+1 (555) 555 - 5555",
            isValid: true,
            firstIndex: 15,
            lastIndex: 34,
            meta: {
                international: "+1 555-555-5555",
                e164: "+15555555555",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 816-234-2265",
        types: ["PhoneNumber"],
        text: "If you have any questions about your statement, please call us at 816-234-2265 ",
        expectedResult: [{ type: "PhoneNumber", match: "816-234-2265", isValid: true, firstIndex: 66, lastIndex: 78, meta: { international: "+1 816-234-2265", e164: "+18162342265", possibility: "is-possible", countryCode: 1, regionCode: "US" } }]
    },
    {
        description: " - should find and validate phone number like (1 - 800 - 769 - 2511)",
        types: ["PhoneNumber"],
        text: "Hey call me at (1 - 800 - 769 - 2511) when you can",
        expectedResult: [{
            type: "PhoneNumber",
            match: "(1 - 800 - 769 - 2511)",
            isValid: true,
            firstIndex: 15,
            lastIndex: 37,
            meta: {
                international: "+1 800-769-2511",
                e164: "+18007692511",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like AZ 85282 (555) 522-2222",
        types: ["PhoneNumber"],
        text: "AZ 85282 (555) 522-2222 Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "(555) 522-2222",
            isValid: true,
            firstIndex: 9,
            lastIndex: 23,
            meta: {
                international: "+1 555-522-2222",
                e164: "+15555222222",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 504.908.9629",
        types: ["PhoneNumber"],
        text: "call me at 504.908.9629 Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "504.908.9629",
            isValid: true,
            firstIndex: 11,
            lastIndex: 23,
            meta: {
                international: "+1 504-908-9629",
                e164: "+15049089629",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 1-800-flowers",
        types: ["PhoneNumber"],
        text: "call me at 1-800-flowers Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "1-800-flowers",
            isValid: true,
            firstIndex: 11,
            lastIndex: 24,
            meta: {
                international: "+1 800-356-9377",
                e164: "+18003569377",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 800-flowers",
        types: ["PhoneNumber"],
        text: "call me at 800-flowers Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "800-flowers",
            isValid: true,
            firstIndex: 11,
            lastIndex: 22,
            meta: {
                international: "+1 800-356-9377",
                e164: "+18003569377",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 800 flowers",
        types: ["PhoneNumber"],
        text: "call me at 800-flowers Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "800-flowers",
            isValid: true,
            firstIndex: 11,
            lastIndex: 22,
            meta: {
                international: "+1 800-356-9377",
                e164: "+18003569377",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like (800) flowers",
        types: ["PhoneNumber"],
        text: "call me at 800-flowers Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "800-flowers",
            isValid: true,
            firstIndex: 11,
            lastIndex: 22,
            meta: {
                international: "+1 800-356-9377",
                e164: "+18003569377",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should find and validate phone number like 1-800-flowers",
        types: ["PhoneNumber"],
        text: "call me at 800-flowers Some words here",
        expectedResult: [{
            type: "PhoneNumber",
            match: "800-flowers",
            isValid: true,
            firstIndex: 11,
            lastIndex: 22,
            meta: {
                international: "+1 800-356-9377",
                e164: "+18003569377",
                possibility: "is-possible",
                countryCode: 1,
                regionCode: "US"
            }
        }]
    },
    {
        description: " - should Notfind and validate phone number like 1500 Sacramento",
        types: ["PhoneNumber"],
        text: "Street Ste. 1500 Sacramento,  conditions",
        expectedResult: []
    }
    ]
};







const invalidPhoneTests = {
    title: "Phone numbers valid cases",
    items: [{
        description: " - should NOT find and validate a phone number like 24 (1997) 7",
        types: ["PhoneNumber"],
        text: "Hey call me at 24 (1997) 7 when you can",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate phone number like 5 (2001) 81",
        types: ["PhoneNumber"],
        text: "Hey call me at 5 (2001) 81. when you can",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate phone number like 5106846434",
        types: ["PhoneNumber"],
        text: "Hey call me at 5106846434 when you can",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate phone number like 15106846434",
        types: ["PhoneNumber"],
        text: "Hey call me at 15106846434 when you can",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate phone number like: Sales (175,000 + 160,000 + 120,000)  $455,000",
        types: ["PhoneNumber"],
        text: "Hey call me at Sales (175,000 + 160,000 + 120,000)  $455,000",
        expectedResult: []
    }

        // todo add tests:
        // "100 billion"
    ]
};

const tests = [
    validPhoneTests,
    invalidPhoneTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
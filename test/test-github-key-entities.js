const validTests = {
    title: "GithubKey valid cases",
    items: [{
        description: " - should find and validate a GithubKey like $ export GITHUB_KEY=\"c6e53fab550fd8aa1523\"",
        types: ["GithubKey"],
        text: "GithubKey: $ export GITHUB_KEY=\"c6e53fab550fd8aa1523\"",
        expectedResult: [
            {
                "firstIndex": 20,
                "isValid": true,
                "lastIndex": 52,
                "leadText": " $ export ",
                "match": "GITHUB_KEY=\"c6e53fab550fd8aa1523",
                "meta": {
                    "key": "c6e53fab550fd8aa1523",
                },
                "tailText": "\"",
                "type": "GithubKey",
            }
        ]
    },
        {
            description: " - should find and validate a GithubKey $ export GITHUB_SECRET=\"a651c99cb4de311795f924a3209984f1d27628e0\"",
            types: ["GithubKey"],
            text: "Hey use $ export GITHUB_SECRET=\"a651c99cb4de311795f924a3209984f1d27628e0\"",
            expectedResult: [
                {
                    "firstIndex": 17,
                    "isValid": true,
                    "lastIndex": 72,
                    "leadText": " $ export ",
                    "match": "GITHUB_SECRET=\"a651c99cb4de311795f924a3209984f1d27628e0",
                    "meta": {
                        "key": "a651c99cb4de311795f924a3209984f1d27628e0",
                    },
                    "tailText": "\"",
                    "type": "GithubKey",
                }
            ]

        },
        {
            description: " - should find and validate a GithubKey like $ export GITHUB_SECRET:a651c99cb4de311795f924a3209984f1d27628e0",
            types: ["GithubKey"],
            text: "$ export GITHUB_SECRET:a651c99cb4de311795f924a3209984f1d27628e0",
            expectedResult: [
                {
                    "firstIndex": 9,
                    "isValid": true,
                    "lastIndex": 63,
                    "leadText": "$ export ",
                    "match": "GITHUB_SECRET:a651c99cb4de311795f924a3209984f1d27628e0",
                    "meta": {
                        "key": "a651c99cb4de311795f924a3209984f1d27628e0",
                    },
                    "tailText": "",
                    "type": "GithubKey",
                }
            ]
        },
    ]
};




const invalidTests = {
    title: "GithubKey invalid cases",
    items: [{
        description: " - should not find github_api_key=sadfffassfsasd",
        types: ["GithubKey"],
        text: "your github_api_key=sadfffassfsasd",
        expectedResult: [ ]
    }]
};

//Should match:
const shouldMatchArray = [
    'GITHUB_KEY="c6e53fab550fd8aa1523ddddddddd";',
    '$ export GITHUB_KEY="c6e53fab550fd8aa1523"',
    '$ export GITHUB_KEY=\'c6e53fab550fd8aa1523\'',
    '$ export GITHUB_SECRET="a651c99cb4de311795f924a3209984f1d27628e0"',
    '$ export github_key="c6e53faddwdwb550fd8aa1523ads"',
    '$ export GITHUB_SECRET "a651c99cb4de311795f924a3209984f1d27628e0"',
    '$ export GITHUB_SECRET:a651c99cb4de311795f924a3209984f1d27628e0',
    'a GITHUB_SECRET="a651c99cb4de334238e0";',
    'GITHUB_SECRET="a651c99cb4de311795f924a3209984f1d27628e0";',
    'github_api_key=sdafafsalksadfjskflk3ljlkjsdfl',
    'github_api_key = sdafafsalksadfjskflk3ljlkjsdfl',
];

shouldMatchArray.forEach((element) => {
    validTests.items.push({
        description: ` - should find and validate a GithubKey ${element}`,
        types: ["GithubKey"],
        text: `This is for in context ${element} as a result`,
        expectedResult: [{
            stub: "should have match here",
        }]
    });
});

//Should match:
const shouldNotMatchArray = [
    'github_api_key 123456',
    'GITHUB_SECRET: a651c99cb4de311795f924a3209984f1d27628e0dsadsa',
];

shouldNotMatchArray.forEach((element) => {
    invalidTests.items.push({
        description: ` - should NOT find and validate ${element}`,
        types: ["GithubKey"],
        text: `This is ${element}.`,
        expectedResult: []
    });
});

const tests = [
    validTests,
    invalidTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
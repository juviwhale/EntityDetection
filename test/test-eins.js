const validEINTests = {
    title: "EIN valid cases",
    items: [{
        description: " - should find and validate a EIN like 52-4352452",
        types: ["EIN"],
        text: "Hey go to 52-4352452 let me know",
        expectedResult: [{
            match: "52-4352452",
            firstIndex: 10,
            lastIndex: 20,
            leadText: "Hey go to ",
            tailText: " let me kn",
            meta: {},
            type: "EIN",
            isValid: true
        }]
    }]
};




const invalidEINTests = {
    title: "EIN invalid cases",
    items: [{
        description: " - should NOT find and validate a EIN like alex@me.com",
        types: ["EIN"],
        text: "Hey follow me alex@me.com on twitter",
        expectedResult: []
    }, ]
};


const tests = [
    validEINTests,
    invalidEINTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
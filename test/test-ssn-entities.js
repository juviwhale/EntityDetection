// NOTE

// REG EX Examples are decent at https: //www.codeproject.com/Articles/651609/Validating-Social-Security-Numbers-through-Regular
// RULES at http://www.lavasurfer.com/info/socialsecurity.html


const validSSNTests = {
    title: "SSN valid cases",
    items: [{
        description: " - should find and validate SSN of standard format 554-87-2432",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 554-87-2432 your welcome!",
        expectedResult: [{
            match: "554-87-2432",
            firstIndex: 30,
            lastIndex: 41,
            leadText: " this SSN ",
            tailText: " your welc",
            meta: { SSN: "554-87-2432" },
            type: "SocialSecurityNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate SSN of standard format 554-87-2432.",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 554-87-2432 your welcome!",
        expectedResult: [{
            match: "554-87-2432",
            firstIndex: 30,
            lastIndex: 41,
            leadText: " this SSN ",
            tailText: " your welc",
            meta: { SSN: "554-87-2432" },
            type: "SocialSecurityNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate SSN of standard format 554:87:2432.",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 554:87:2432 your welcome!",
        expectedResult: [{
            match: "554:87:2432",
            firstIndex: 30,
            lastIndex: 41,
            leadText: " this SSN ",
            tailText: " your welc",
            meta: { SSN: "554:87:2432" },
            type: "SocialSecurityNumber",
            isValid: true
        }]
    },
    {
        description: " - should find and validate SSN of standard format 554-87-2432 that is labeled",
        types: ["SocialSecurityNumber"],
        text: "Social Security Number: 554:87:2432 your welcome!",
        expectedResult: [{
            match: "554:87:2432",
            firstIndex: 24,
            lastIndex: 35,
            leadText: "y Number: ",
            tailText: " your welc",
            label: "Social Security Number",
            meta: { SSN: "554:87:2432" },
            type: "SocialSecurityNumber",
            isValid: true
        }]
    },
    ]
};


const invalidSSNTests = {
    title: "SSN invalid cases",
    items: [{
        description: " - should NOT find and validate a SSN 765654323",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 765654323 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 000-87-2432",
        note: "out of valid range ref: http://www.lavasurfer.com/info/socialsecurity.html",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 000-87-2432 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 666-87-2432",
        note: "wow amazed they cared about the devil! out of valid range ref: http://www.lavasurfer.com/info/socialsecurity.html",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 666-87-2432 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 900-87-2432",
        note: "out of valid range ref: http://www.lavasurfer.com/info/socialsecurity.html",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 900-87-2432 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 554-00-2432",
        note: "Any number that has a middle \"00\" will never be a valid SSN. ref: http://www.lavasurfer.com/info/socialsecurity.html",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 554-00-2432 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 554-00-0000",
        note: "Any number that ends in \"0000\" will never be a valid SSN. ref: http://www.lavasurfer.com/info/socialsecurity.html",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 554-00-0000 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a SSN 078-05-1120",
        note: "Woolworth’s Wallet Fiasco. ref: http: //www.snopes.com/business/taxes/woolworth.asp",
        types: ["SocialSecurityNumber"],
        text: "Steal my credit with this SSN 078-05-1120 your welcome!",
        expectedResult: []
    },
    {
        description: " - should NOT find and validate a Order G-SF-7688-43-3561",
        note: "",
        types: ["SocialSecurityNumber"],
        text: "Order G-SF-7688-43-3561",
        expectedResult: []
    },
    ]
};

const tests = [
    validSSNTests,
    invalidSSNTests
];

// RUN THESE TESTS!
require("./entity-test-runner").run(tests);
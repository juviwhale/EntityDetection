US Zip codes
/(?<=\s|^)(\d\d)(\d\d\d)(?:[-\s]{1}\d{4})?(?=[,.!?\s]|$)/gm

UK Zip codes
/(?<=\s|^)((?:(?:gir)|(?:[a-pr-uwyz])(?:(?:[0-9](?:[a-hjkpstuw]|[0-9])?)|(?:[a-hk-y][0-9](?:[0-9]|[abehmnprv-y])?)))) ?([0-9][abd-hjlnp-uw-z]{2})(?=[,.!?\s]|$)/gim

ssh_rsa_public
/(?<=\s|^)ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} [^@]+@[^@]+(?=[,.!?\s]|$)/gm

Arista network configuration
/(?<=\s|^)via\ \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3},\ \d{2}:\d{2}:\d{2}(?=[,.!?\s]|$)/gim

Cisco Router Config
/(?<=\s|^)service\ timestamps\ [a-z]{3,5}\ datetime\ msec|boot-[a-z]{3,5}-marker|interface\ [A-Za-z0-9]{0,10}[E,e]thernet(?=[,.!?\s]|$)/gm

DSA Private Key
/(?<=\s|^)-----BEGIN DSA PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END DSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm

EC Private Key
/(?<=\s|^)-----BEGIN (?:EC|ECDSA) PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END (?:EC|ECDSA) PRIVATE KEY-----(?=[,.!?\s]|$)/gm

Encrypted DSA Private Key
/(?<=\s|^)-----BEGIN DSA PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END DSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm

Encrypted EC Private Key
/(?<=\s|^)-----BEGIN (?:EC|ECDSA) PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END (?:EC|ECDSA) PRIVATE KEY-----(?=[,.!?\s]|$)/gm

Encrypted Private Key
/(?<=\s|^)-----BEGIN ENCRYPTED PRIVATE KEY-----(?:.|\s)+?-----END ENCRYPTED PRIVATE KEY-----(?=[,.!?\s]|$)/gm

Encrypted PuTTY SSH DSA Key
/(?<=\s|^)PuTTY-User-Key-File-2: ssh-dss\s*Encryption: aes(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gm

Encrypted PuTTY SSH RSA Key
(?:^|\b)/PuTTY-User-Key-File-2: ssh-rsa\s*Encryption: aes(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gm

Encrypted RSA Private Key
/(?<=\s|^)-----BEGIN RSA PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END RSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm

PGP Private Key Block
/(?<=\s|^)-----BEGIN PGP PRIVATE KEY BLOCK-----(?:.|\s)+?-----END PGP PRIVATE KEY BLOCK-----(?=[,.!?\s]|$)/gm

PlainText Private Key
/(?<=\s|^)-----BEGIN PRIVATE KEY-----(?:.|\s)+?-----END PRIVATE KEY-----(?=[,.!?\s]|$)/gm

Public encrypted key
/(?<=\s|^)-----BEGIN PUBLIC KEY-----(?:.|\s)+?-----END PUBLIC KEY-----(?=[,.!?\s]|$)/gm

RSA Private Key
/(?<=\s|^)-----BEGIN RSA PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END RSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm

SSL Certificate
/(?<=\s|^)-----BEGIN CERTIFICATE-----(?:.|\n)+?\s-----END CERTIFICATE-----(?=[,.!?\s]|$)/gm

PuTTY SSH DSA Key
/(?<=\s|^)PuTTY-User-Key-File-2: ssh-dss\s*Encryption: none(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gim

PuTTY SSH RSA Key
/(?<=\s|^)PuTTY-User-Key-File-2: ssh-rsa\s*Encryption: none(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gim

PKCS7 Encrypted Data
/(?<=\s|^)(?:Signer|Recipient)Info(?:s)?\ ::=\ \w+|[D|d]igest(?:Encryption)?Algorithm|EncryptedKey\ ::= \w+(?=[,.!?\s]|$)/gim

Password etc passwd
/(?<=\s|^)[a-zA-Z0-9\-]+:[x|\*]:\d+:\d+:[a-zA-Z0-9/\- "]*:/[a-zA-Z0-9/\-]*:/[a-zA-Z0-9/\-]+(?=[",.!?\s]|$)/gim

Password etc shadow
/(?<=\s|^)[a-zA-Z0-9\-]+:(?:(?:!!?)|(?:\*LOCK\*?)|\*|(?:\*LCK\*?)|(?:\$.*\$.*\$.*?)?):\d*:\d*:\d*:\d*:\d*:\d*:(?=[,.!?\s]|$)/gim

Public Key Cryptography System (PKCS)
/(?<=\s|^)protocol="application/x-pkcs[0-9]{0,2}-signature"(?=[,.!?\s]|$)/gim

SWIFT Codes
/(?<=\s|^)[A-Za-z]{4}(?:GB|US|DE|RU|CA|JP|CN)[0-9a-zA-Z]{2,5}(?=[,.!?\s]|$)/gim

Samba Password config file
/(?<=\s|^)[a-z]*:\d{3}:[0-9a-zA-Z]*:[0-9a-zA-Z]*:\[U\ \]:.*(?=[,.!?\s]|$)/gim

Simple Network Management Protocol Object Identifier
/(?<=\s|^)(?:\d\.\d\.\d\.\d\.\d\.\d{3}\.\d\.\d\.\d\.\d\.\d\.\d\.\d\.\d\.\d{4}\.\d)|[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+(?=[,.!?\s]|$)/gm

CVE Number
/(?<=\s|^)CVE-\d{4}-\d{4,7}(?=[,.!?\s]|$)/gm

HIPAA PHI National Drug Code
/(?<=\s|^)\d{4,5}-\d{3,4}-\d{1,2}(?=[,.!?\s]|$)/gm

Individual Taxpayer Identification Numbers (ITIN)
/(?<=\s|^)9\d{2}(?:[ \-]?)[7,8]\d(?:[ \-]?)\d{4}(?=[,.!?\s]|$)/gm

Slack 2FA Backup Codes
/(?<=\s|^)Two-Factor\s*\S*Authentication\s*\S*Backup\s*\S*Codes(?:.|\n)*[Ss]lack(?:.|\n)*\d{9}(?=[,.!?\s]|$)/gim

aws_access_key
/(?<=\s|^)((access[ -_]?key[ -_]?id)|(ACCESS[ -_]?KEY[ -_]?ID)|([Aa]ccessKeyId)|(access[ _-]?id)).{0,20}AKIA[a-zA-Z0-9+\/]{16}(?=[",.!?\s]|$)/gim

aws_secret_key
/(?<=\s|^)((secret[ -_]?access[ -_]?key)|(SECRET[ -_]?ACCESS [ -_]?KEY|(private[ -_]?key))|([Ss]ecretAccessKey)).{0,20}[^a-zA-Z0-9+\/][a-zA-Z0-9+\/]{40}(?=[",.!?\s]|$)/gim

facebook_secret
/(?<=\s|^)(facebook_secret|FACEBOOK_SECRET|facebook_app_secret|FACEBOOK_APP_SECRET)[a-z_ =\s"'\:]{0,5}[^a-zA-Z0-9][a-f0-9]{32}(?=[",.!?\s]|$)/gim

github_key
/(?<=\s|^)(?:GITHUB_SECRET|GITHUB_KEY|GITHUB_TOKEN|GITHUB_API_KEY)(?:[\s="':]{0,4})(\w{20,40})(?=["';<,.!?\s]|$)/gim
// aws regex: /(?<=\s|^)(GITHUB_SECRET|GITHUB_KEY|github_secret|github_key|github_token|GITHUB_TOKEN|github_api_key|GITHUB_API_KEY)[a-z_ =\s"'\:]{0,10}[^a-zA-Z0-9][a-zA-Z0-9]{40}[^a-zA-Z0-9]/gim

google_two_factor_backup
/(?<=\s|^)BACKUP VERIFICATION CODES\s+\w+@\w+\.\w+(?=[,.!?\s]|$)/gim

heroku_key
/(?<=\s|^)(heroku_api_key|HEROKU_API_KEY|heroku_secret|HEROKU_SECRET)[a-z_ =\s"'\:]{0,10}[^a-zA-Z0-9-]\w{8}(?:-\w{4}){3}-\w{12}[^a-zA-Z0-9\-](?=[",.!?\s]|$)/gim

slack_api_key
/(?<=\s|^)(slack_api_key|SLACK_API_KEY|slack_key|SLACK_KEY)[a-z_ =\s"'\:]{0,10}[^a-f0-9][a-f0-9]{32}[^a-f0-9](?=[",.!?\s]|$)/gim

slack_api_token
/(?<=\s|^)(xox[pb](?:-[a-zA-Z0-9]+){4,})(?=[,.!?\s]|$)/gim

ssh_dss_public
/(?<=\s|^)ssh-dss [0-9A-Za-z+/]+[=]{2}(?=[,.!?\s]|$)/gim

ssh_rsa_public
/(?<=\s|^)ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} [^@]+@[^@]+(?=[,.!?\s]|$)/gim
const Entity = require("../helpers/entity");

class CaliforniaDriversLicense extends Entity {

    get entity_type() {
        return "CaliforniaDriversLicense";
    }

    static potentials_regex() {
        // https://regex101.com/r/ophYmd/10
        return /(?<=^|[>\s])([A-Z]{1}\d{7})(?=[<,.!?\s]|$)/gm;
    }

}

module.exports = CaliforniaDriversLicense;
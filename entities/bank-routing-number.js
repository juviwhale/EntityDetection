const Entity = require("../helpers/entity");

class BankRoutingNumber extends Entity {

    get entity_type() {
        return "BankRoutingNumber";
    }

    format() {
        var regex;
        let banks = BankRoutingNumber.bank_matches();
        for (var bankName in banks) {
            regex = banks[bankName];
            if (this.match.match(regex)) {
                this.meta.bankName = bankName;
            }
        }
    }

    static bank_matches() {
        return {
            "Wells Fargo": /(?<=\s|^)121042882(?=[,.!?\s]|$)/g,
            "US Bank": /(?<=\s|^)12(?:1122676|2235821)(?=[,.!?\s]|$)/g,
            "United Bank": /(?<=\s|^)122243350(?=[,.!?\s]|$)/g,
            "Bank of America": /(?<=\s|^)(?:121|026)00(?:0|9)(?:358|593)(?=[,.!?\s]|$)/g,
            "BBVA Compass": /(?<=\s|^)062001186(?=[,.!?\s]|$)/g,
            "Chase": /(?<=\s|^)322271627(?=[,.!?\s]|$)/g,
            "Citibank": /(?<=\s|^)32(?:11|22)71(?:18|72)4(?=[,.!?\s]|$)/g,
        };
    }

    static potentials_regex() {
        return Object.keys(this.bank_matches()).map((k) => this.bank_matches()[k]);
    }

}

module.exports = BankRoutingNumber;
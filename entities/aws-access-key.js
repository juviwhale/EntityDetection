const Entity = require("../helpers/entity");

class AwsAccessKey extends Entity {

    get entity_type() {
        return "AwsAccessKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)((access[ -_]?key[ -_]?id)|(ACCESS[ -_]?KEY[ -_]?ID)|([Aa]ccessKeyId)|(access[ _-]?id)).{0,20}AKIA[a-zA-Z0-9+\/]{16}(?=[",.!?\s]|$)/gim;
    }

}

module.exports = AwsAccessKey;
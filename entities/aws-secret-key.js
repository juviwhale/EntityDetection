const Entity = require("../helpers/entity");

class AwsSecretKey extends Entity {

    get entity_type() {
        return "AwsSecretKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)((secret[ -_]?access[ -_]?key)|(SECRET[ -_]?ACCESS [ -_]?KEY|(private[ -_]?key))|([Ss]ecretAccessKey)).{0,20}[^a-zA-Z0-9+\/][a-zA-Z0-9+\/]{40}(?=[",.!?\s]|$)/gim;
    }

}

module.exports = AwsSecretKey;
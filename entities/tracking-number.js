const _ = require('lodash');
const Entity = require("../helpers/entity");

class TrackingNumber extends Entity {

    get entity_type() {
        return "TrackingNumber";
    }

    format() {
        //console.log('this.match:', this.match)
        let shippers = TrackingNumber.shipper_matches();
        _.forEach(shippers, (shipper, shipper_name) => {
            _.forEach(shipper, shipper_regex => {
                if (shipper_regex.test(this.match)) {
                    this.meta.shipper_name = shipper_name;
                }
            });
        });
    }

    static shipper_matches() {
        /*
        https://stackoverflow.com/questions/619977/regular-expression-patterns-for-tracking-numbers

        UPS:

        /(?<=\s|^)(1Z ?[0-9A-Z]{3} ?[0-9A-Z]{3} ?[0-9A-Z]{2} ?[0-9A-Z]{4} ?[0-9A-Z]{3} ?[0-9A-Z]|[\dT]\d\d\d ?\d\d\d\d ?\d\d\d)\b/
        FedEX: (3 Different Ones)

        /(\b96\d{20}\b)|(\b\d{15}\b)|(\b\d{12}\b)/
        /(?<=\s|^)((98\d\d\d\d\d?\d\d\d\d|98\d\d) ?\d\d\d\d ?\d\d\d\d( ?\d\d\d)?)\b/
        /^[0-9]{15}$/
        USPS: (4 Different Ones)

        /(\b\d{30}\b)|(\b91\d+\b)|(\b\d{20}\b)/
        /^E\D{1}\d{9}\D{2}$|^9\d{15,21}$/
        /^91[0-9]+$/
        /^[A-Za-z]{2}[0-9]+US$/
        */

        return {
            "UPS": [
                /(?<=\s|^)(1Z ?[0-9A-Z]{3} ?[0-9A-Z]{3} ?[0-9A-Z]{2} ?[0-9A-Z]{4} ?[0-9A-Z]{3} ?[0-9A-Z]|[\dT]\d\d\d ?\d\d\d\d ?\d\d\d)\b/g
            ],
            "USPS": [
                /(\b\d{30}\b)|(\b\d{20}\b)/g,
                /(?<=\s|^)E\D{1}\d{9}\D{2}\b|\b9\d{15,21}\b/g,
                /(?<=\s|^)(91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d|91\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d ?\d\d\d\d)\b/ig,
                /(?<=\s|^)[A-Za-z]{2}[0-9]+US\b/g,
            ],
            "FEDEX": [
                /(\b96\d{20}\b)|(\b\d{15}\b)|(\b\d{12}\b)/g,
                /(?<=\s|^)((98\d\d\d\d\d?\d\d\d\d|98\d\d) ?\d\d\d\d ?\d\d\d\d( ?\d\d\d)?)\b/g,
                /(?<=\s|^)[0-9]{15}\b/g,
            ],
            "ONTRAC": [ // c with 14 digits
                /(?<=\s|^)c\d{14}\b/g
            ],
            "DHL": [ // 10 or 11 digits
                /(?<=\s|^)[0-9]{10,11}\b/g
            ]
        };
    }

    static potentials_regex() {
        //console.log(this.get_potential_regexs(this.shipper_matches()))
        let regexs = [];
        _.forEach(this.shipper_matches(), potential_regex => regexs.push(...potential_regex));
        return regexs;
    }

}

module.exports = TrackingNumber;
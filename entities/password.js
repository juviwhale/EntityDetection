const Entity = require("../helpers/entity");
const util = require('../helpers/utils');

class Password extends Entity {

    get entity_type() {
        return "Password";
    }

    format() {
        const re = /(?<=\s|^)(?:password|pwd|pw|pin code:|pin:) ?(?:is|will be|is now)?(?:[=>: ]{1,4})(?=.{6,64}$)([^\s{}\[\]]{6,64})(?=[<,.!?\s]|$)/im;
        if (this.match.match(re)) {
            this.meta.password = this.match.match(re)[1]
        }
    }

    validate() {
        return (super.validate() && validate_password(this.meta.password) && !util.is_stop_word(this.meta.password) && this.meta.password !== null);
    }

    static potentials_regex() {
        // https://regex101.com/r/9ENoLQ/28
        return /(?<=\s|^)(?:password|pwd|pw|pin code:|pin:) ?(?:is|will be|is now)?(?:[=>: ]{1,4})(?=.{6,64}$)([^\s{}\[\]]{6,64})(?=[<,.!?\s]|$)/gim;
    }
}

function validate_password(str) {
    const min_pass_len = 6;
    return !!(
        str && // str exists
        str.length >= min_pass_len && // minimum length
        !util.is_basic_word(str) &&  // check if its basic word "hello", "hello?"
        !util.contains_url(str) // check if password is not a url
    );
}



module.exports = Password;
const Entity = require("../helpers/entity");

class Currency extends Entity {

    get entity_type() {
        return "Currency";
    }

    get potential() {
        // is this a 1,323,232 like number without a currency symbol or period
        return !!(this.match.match(/(^|\s)\d{1,3}(,\d{3})*\b/) && !this.match.match(/[*€£$¢.]/));
    }

    format() {
        if (this.validate()) {
            this.meta.value = this.match;
        }
    }

    validate() {
        let isValid = true;

        // Black list of formats **We need a better approach to this!**
        let blackListFormats = [
            /\.\d\d\d+$/, // don't match 42.000
        ];
        blackListFormats.forEach(regex => {
            if (regex.exec(this.match)) {
                isValid = false;
            }
        });

        let blackListLeadText = [
            /figure|section/i, // don't match 42.000
        ];
        blackListLeadText.forEach(regex => {
            if (regex.exec(this.leadText)) {
                isValid = false;
            }
        });

        return super.validate() && isValid;
    }

    static potentials_regex() {
        // https: //regex101.com/r/I834dg/45
        return /(?:^|[^\S\r\n])(?:\(?-?[^\S\r\n]*\d+\b(?:[,]\d{3})*[.]\d{2}\b\)?|\(?-?[^\S\r\n]*\d+\b(?:[.]\d{3})*[,]\d{2}\b\)?|\([^\S\r\n]*(?:[*$€£]|\d+(?:\.\d+)+,|\d+(?:,\d+)+\.)(?:[^\S\r\n]*)?\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?[^\S\r\n]*\)|\([^\S\r\n]*(?:[*$€£]|\d+(?:\.\d+)+,|\d+(?:,\d+)+\.)(?:[^\S\r\n]*)?\d{1,3}\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?[^\S\r\n]*\)|[*$€£][^\S\r\n]*-?[^\S\r\n]*\d+\b(?:[,]\d{3}\b)*[.]\d{2}\b|[*$€£][^\S\r\n]*-?[^\S\r\n]*\d+\b(?:[.]\d{3}\b)*[,]\d{2}\b|[^\S\r\n]*[*$€£][^\S\r\n]*-?\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?|[^\S\r\n]*-?[*$€£][^\S\r\n]*\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?|-?[^\S\r\n]*[*$€£][^\S\r\n]*\d+\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?|-?[^\S\r\n]*\d+\b(?:[,]\d{3}\b)*(?:[.]\d{2}\b)?[^\S\r\n]*[€¢£]|-?[^\S\r\n]*\d+\b(?:[.]\d{3}\b)*(?:[,]\d{2}\b)?[^\S\r\n]*[€¢£]|-?[^\S\r\n]*\d+(?:[.]\d{3})+|(?:-?[^\S\r\n]*\(?\d{1,3}\b(?:[,]\d{3}\b)+(?:[.]\d{2}\b)?\)?))(?![\d$€¢£()%\/-]|[$€¢£.,()]\d)/gm;
    }

}

module.exports = Currency;
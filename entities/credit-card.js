const Entity = require("../helpers/entity");
const CardValidator = require("card-validator");

class CreditCard extends Entity {

    get entity_type() {
        return "CreditCard";
    }

    format() {
        const v = this.match.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
        const matches = v.match(/\d{4,16}/g);
        const match = matches && matches[0] || "";
        const parts = [];

        for (let i = 0, len = match.length; i < len; i += 4) {
            parts.push(match.substring(i, i + 4));
        }

        if (parts.length) {
            this.meta.formatted = parts.join(" ");
        } else {
            this.meta.formatted = this.match;
        }

        // Validate match
        const matchValidation = CardValidator.number(this.match);
        this.isValid = matchValidation.isValid;
        if (matchValidation.isValid) {
            this.meta.cardType = matchValidation.card.niceType;
            this.meta.code = matchValidation.card.code;
        }
    }

    static potentials_regex() {
        //https://regex101.com/r/3sMmMK/1  utalizing lookahead and lookbehind
        return /(?<=\s|^)(?:\d[ -]*?){13,16}(?=[,.!?\s]|$)/gim;
    }
}

module.exports = CreditCard;
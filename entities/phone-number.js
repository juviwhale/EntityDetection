const Entity = require("../helpers/entity");

// Require `PhoneNumberFormat`.
const PNF = require("google-libphonenumber").PhoneNumberFormat;
const ValidationResult = require("google-libphonenumber").PhoneNumberUtil.ValidationResult;

// Get an instance of `PhoneNumberUtil`.
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();

class PhoneNumber extends Entity {

    get entity_type() {
        return "PhoneNumber";
    }

    format() {
        // Type specific
        const potentialRegionCode = this.extractRegionCode(this.match);

        try { // catch invalid string error
            this.googleLibPhoneNumber = phoneUtil.parse(this.match, potentialRegionCode || "US");
        } catch (err) { return; }

        this.international = phoneUtil.format(this.googleLibPhoneNumber, PNF.INTERNATIONAL);
        this.e164 = phoneUtil.format(this.googleLibPhoneNumber, PNF.E164);
        //this.rfc3966 = phoneUtil.format(this.googleLibPhoneNumber, PNF.RFC3966),
        //this.significant = phoneUtil.getNationalSignificantNumber(this.googleLibPhoneNumber),
        this.possibility = this.getValidationResult();
        this.countryCode = this.googleLibPhoneNumber.getCountryCode();
        this.regionCode = this.getRegionCodeForCountryCode(this.countryCode);
    }

    validate() {
        let isValid = this.googleLibPhoneNumber && phoneUtil.isPossibleNumber(this.googleLibPhoneNumber);

        if (/^[a-zA-Z0-9]*$/.test(this.match) === true) // Check if the potential phone number is a whole number (no spaces, special chars etc)
            isValid = false;

        // Black list of formats **We need a better approach to this!**
        let blackListFormats = [
            /\d{1,2}\s+\(\d{4}\)\s+\d{1,2}/
        ];
        blackListFormats.forEach(regex => {
            if (regex.exec(this.match)) {
                isValid = false;
            }
        });

        return super.validate() && isValid;
    }

    extractRegionCode(phoneNumber) {
        if (phoneNumber.charAt(0) !== "+" || phoneNumber.length < 5)
            return null;

        const firstOne = phoneNumber.substr(1, 1);
        const firstTwo = phoneNumber.substr(1, 2);
        const firstThree = phoneNumber.substr(1, 3);

        let regionCode;

        regionCode = this.getRegionCodeForCountryCode(firstOne);
        if (regionCode !== "ZZ")
            return regionCode;

        regionCode = this.getRegionCodeForCountryCode(firstTwo);
        if (regionCode !== "ZZ")
            return regionCode;

        regionCode = this.getRegionCodeForCountryCode(firstThree);
        if (regionCode !== "ZZ")
            return regionCode;
    }

    getRegionCodeForCountryCode(countryCode) {
        const regionCode = phoneUtil.getRegionCodeForCountryCode(countryCode);

        if (regionCode.substr(0, 2) === "00")
            return this.getRegionCodeForCountryCode(regionCode.substr(2));

        return regionCode;
    }

    getValidationResult() {
        switch (phoneUtil.isPossibleNumberWithReason(this.googleLibPhoneNumber)) {
        case ValidationResult.IS_POSSIBLE:
            return "is-possible";
        case ValidationResult.INVALID_COUNTRY_CODE:
            return "invalid-country-code";
        case ValidationResult.TOO_LONG:
            return "too-long";
        case ValidationResult.TOO_SHORT:
            return "too-short";
        }

        if (phoneUtil.isPossibleNumber(this.googleLibPhoneNumber))
            return "is-possible";
        return "unknown";
    }

    export () {
        return {
            type: this.type,
            match: this.match,
            isValid: this.isValid,
            firstIndex: this.firstIndex,
            lastIndex: this.lastIndex,
            meta: {
                international: this.international,
                e164: this.e164,
                possibility: this.possibility,
                countryCode: this.countryCode,
                regionCode: this.regionCode,
            }
        };
    }

    static potentials_regex() {
        return [
            // https://regex101.com/r/caLSwo/2
            /(?:^|[^\S\r\n])(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?\b/gim,

            // Vanity Numbers
            // https://regex101.com/r/SnuaBc/1
            /\b(1-|1 )?(\()?[1-9]\d\d(?:-| |\)|\) )[a-zA-Z]{7}\b/g,

            // wide range of number combinations
            /[+(]?[0-9][\d\-() ]{5,20}[0-9]\)?/gi,
        ];
    }
}

module.exports = PhoneNumber;
const Entity = require("../helpers/entity");
const util = require('../helpers/utils');

class Username extends Entity {

    get entity_type() {
        return "Username";
    }

    format() {
        const re = /(?<=\s|^)(?:username|usr|un)(?: is | ?["=>:]{1,4} ?)(?=.{5,64}$)([^\s:;{}\[\]]{5,64})(?=[,.!?\s]|$)/im;
        if (this.match.match(re)) {
            this.meta.username = this.match.match(re)[1]
        }
    }

    validate() {
        return (super.validate() && !util.is_stop_word(this.meta.username) &&
            !util.contains_url(this.meta.username) && this.meta.username !== null);
    }

    static potentials_regex() {
        // https://regex101.com/r/Vyww0N/15
        return /(?<=\s|^)(?:username|usr|un)(?: is | ?["=>:]{1,4} ?)(?=.{5,64}$)([^\s:;{}\[\]]{5,64})(?=[,.!?\s]|$)/gim
    }
}

module.exports = Username;
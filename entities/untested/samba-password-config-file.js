const Entity = require("../helpers/entity");

class SambaPasswordConfigFile extends Entity {

    get entity_type() {
        return "SambaPasswordConfigFile";
    }

    static potentials_regex() {
        return /(?<=\s|^)[a-z]*:\d{3}:[0-9a-zA-Z]*:[0-9a-zA-Z]*:\[U\ \]:.*(?=[,.!?\s]|$)/gim;
    }

}

module.exports = SambaPasswordConfigFile;
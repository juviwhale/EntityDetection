const Entity = require("../helpers/entity");

class EncryptedPrivateKey extends Entity {

    get entity_type() {
        return "EncryptedPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN ENCRYPTED PRIVATE KEY-----(?:.|\s)+?-----END ENCRYPTED PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedPrivateKey;
const Entity = require("../helpers/entity");

class EncryptedDsaPrivateKey extends Entity {

    get entity_type() {
        return "EncryptedDsaPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN DSA PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END DSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedDsaPrivateKey;
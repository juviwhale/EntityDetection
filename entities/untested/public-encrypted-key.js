const Entity = require("../helpers/entity");

class PublicEncryptedKey extends Entity {

    get entity_type() {
        return "PublicEncryptedKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN PUBLIC KEY-----(?:.|\s)+?-----END PUBLIC KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = PublicEncryptedKey;
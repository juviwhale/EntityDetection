const Entity = require("../helpers/entity");

class IndividualTaxpayerIdentificationNumbersItin extends Entity {

    get entity_type() {
        return "IndividualTaxpayerIdentificationNumbersItin";
    }

    static potentials_regex() {
        return /(?<=\s|^)9\d{2}(?:[ \-]?)[7,8]\d(?:[ \-]?)\d{4}(?=[,.!?\s]|$)/gm;
    }

}

module.exports = IndividualTaxpayerIdentificationNumbersItin;
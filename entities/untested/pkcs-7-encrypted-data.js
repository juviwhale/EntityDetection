const Entity = require("../helpers/entity");

class Pkcs7EncryptedData extends Entity {

    get entity_type() {
        return "Pkcs7EncryptedData";
    }

    static potentials_regex() {
        return /(?<=\s|^)(?:Signer|Recipient)Info(?:s)?\ ::=\ \w+|[D|d]igest(?:Encryption)?Algorithm|EncryptedKey\ ::= \w+(?=[,.!?\s]|$)/gim;
    }

}

module.exports = Pkcs7EncryptedData;
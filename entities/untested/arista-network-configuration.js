const Entity = require("../helpers/entity");

class AristaNetworkConfiguration extends Entity {

    get entity_type() {
        return "AristaNetworkConfiguration";
    }

    static potentials_regex() {
        return /(?<=\s|^)via\ \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3},\ \d{2}:\d{2}:\d{2}(?=[,.!?\s]|$)/gim;
    }

}

module.exports = AristaNetworkConfiguration;
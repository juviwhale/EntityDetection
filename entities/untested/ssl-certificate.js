const Entity = require("../helpers/entity");

class SslCertificate extends Entity {

    get entity_type() {
        return "SslCertificate";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN CERTIFICATE-----(?:.|\n)+?\s-----END CERTIFICATE-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = SslCertificate;
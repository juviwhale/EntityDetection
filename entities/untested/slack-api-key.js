const Entity = require("../helpers/entity");

class SlackApiKey extends Entity {

    get entity_type() {
        return "SlackApiKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)(slack_api_key|SLACK_API_KEY|slack_key|SLACK_KEY)[a-z_ =\s"'\:]{0,10}[^a-f0-9][a-f0-9]{32}[^a-f0-9](?=[",.!?\s]|$)/gim;
    }

}

module.exports = SlackApiKey;
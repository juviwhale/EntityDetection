const Entity = require("../helpers/entity");

class SwiftCodes extends Entity {

    get entity_type() {
        return "SwiftCodes";
    }

    static potentials_regex() {
        return /(?<=\s|^)[A-Za-z]{4}(?:GB|US|DE|RU|CA|JP|CN)[0-9a-zA-Z]{2,5}(?=[,.!?\s]|$)/gim;
    }

}

module.exports = SwiftCodes;
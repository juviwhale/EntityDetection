const Entity = require("../helpers/entity");

class EncryptedPuTtySshDsaKey extends Entity {

    get entity_type() {
        return "EncryptedPuTtySshDsaKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)PuTTY-User-Key-File-2: ssh-dss\s*Encryption: aes(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedPuTtySshDsaKey;
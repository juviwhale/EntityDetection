const Entity = require("../helpers/entity");

class PublicKeyCryptographySystemPkcs extends Entity {

    get entity_type() {
        return "PublicKeyCryptographySystemPkcs";
    }

    static potentials_regex() {
        return /(?<=\s|^)protocol="application/x-pkcs[0-9]{0,2}-signature"(?=[,.!?\s]|$)/gim;
    }

}

module.exports = PublicKeyCryptographySystemPkcs;
const Entity = require("../helpers/entity");

class SshRsaPublic extends Entity {

    get entity_type() {
        return "SshRsaPublic";
    }

    static potentials_regex() {
        return /(?<=\s|^)ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} [^@]+@[^@]+(?=[,.!?\s]|$)/gm;
    }

}

module.exports = SshRsaPublic;
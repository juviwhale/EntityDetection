const Entity = require("../helpers/entity");

class EncryptedEcPrivateKey extends Entity {

    get entity_type() {
        return "EncryptedEcPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN (?:EC|ECDSA) PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END (?:EC|ECDSA) PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedEcPrivateKey;
const Entity = require("../helpers/entity");

class HerokuKey extends Entity {

    get entity_type() {
        return "HerokuKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)(heroku_api_key|HEROKU_API_KEY|heroku_secret|HEROKU_SECRET)[a-z_ =\s"'\:]{0,10}[^a-zA-Z0-9-]\w{8}(?:-\w{4}){3}-\w{12}[^a-zA-Z0-9\-](?=[",.!?\s]|$)/gim;
    }

}

module.exports = HerokuKey;
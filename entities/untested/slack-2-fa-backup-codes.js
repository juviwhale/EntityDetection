const Entity = require("../helpers/entity");

class Slack2FaBackupCodes extends Entity {

    get entity_type() {
        return "Slack2FaBackupCodes";
    }

    static potentials_regex() {
        return /(?<=\s|^)Two-Factor\s*\S*Authentication\s*\S*Backup\s*\S*Codes(?:.|\n)*[Ss]lack(?:.|\n)*\d{9}(?=[,.!?\s]|$)/gim;
    }

}

module.exports = Slack2FaBackupCodes;
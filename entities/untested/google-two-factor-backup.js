const Entity = require("../helpers/entity");

class GoogleTwoFactorBackup extends Entity {

    get entity_type() {
        return "GoogleTwoFactorBackup";
    }

    static potentials_regex() {
        return /(?<=\s|^)BACKUP VERIFICATION CODES\s+\w+@\w+\.\w+(?=[,.!?\s]|$)/gim;
    }

}

module.exports = GoogleTwoFactorBackup;
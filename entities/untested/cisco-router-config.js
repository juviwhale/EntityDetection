const Entity = require("../helpers/entity");

class CiscoRouterConfig extends Entity {

    get entity_type() {
        return "CiscoRouterConfig";
    }

    static potentials_regex() {
        return /(?<=\s|^)service\ timestamps\ [a-z]{3,5}\ datetime\ msec|boot-[a-z]{3,5}-marker|interface\ [A-Za-z0-9]{0,10}[E,e]thernet(?=[,.!?\s]|$)/gm;
    }

}

module.exports = CiscoRouterConfig;
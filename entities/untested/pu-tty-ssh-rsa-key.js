const Entity = require("../helpers/entity");

class PuTtySshRsaKey extends Entity {

    get entity_type() {
        return "PuTtySshRsaKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)PuTTY-User-Key-File-2: ssh-rsa\s*Encryption: none(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gim;
    }

}

module.exports = PuTtySshRsaKey;
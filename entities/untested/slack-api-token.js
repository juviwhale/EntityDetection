const Entity = require("../helpers/entity");

class SlackApiToken extends Entity {

    get entity_type() {
        return "SlackApiToken";
    }

    static potentials_regex() {
        return /(?<=\s|^)(xox[pb](?:-[a-zA-Z0-9]+){4,})(?=[,.!?\s]|$)/gim;
    }

}

module.exports = SlackApiToken;
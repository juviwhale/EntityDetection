const Entity = require("../helpers/entity");

class EncryptedPuTtySshRsaKey extends Entity {

    get entity_type() {
        return "EncryptedPuTtySshRsaKey";
    }

    static potentials_regex() {
        return (?:^|\b)/PuTTY-User-Key-File-2: ssh-rsa\s*Encryption: aes(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedPuTtySshRsaKey;
const Entity = require("../helpers/entity");

class EcPrivateKey extends Entity {

    get entity_type() {
        return "EcPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN (?:EC|ECDSA) PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END (?:EC|ECDSA) PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EcPrivateKey;
const Entity = require("../helpers/entity");

class PasswordEtcPasswd extends Entity {

    get entity_type() {
        return "PasswordEtcPasswd";
    }

    static potentials_regex() {
        return /(?<=\s|^)[a-zA-Z0-9\-]+:[x|\*]:\d+:\d+:[a-zA-Z0-9/\- "]*:/[a-zA-Z0-9/\-]*:/[a-zA-Z0-9/\-]+(?=[",.!?\s]|$)/gim;
    }

}

module.exports = PasswordEtcPasswd;
const Entity = require("../helpers/entity");

class HipaaPhiNationalDrugCode extends Entity {

    get entity_type() {
        return "HipaaPhiNationalDrugCode";
    }

    static potentials_regex() {
        return /(?<=\s|^)\d{4,5}-\d{3,4}-\d{1,2}(?=[,.!?\s]|$)/gm;
    }

}

module.exports = HipaaPhiNationalDrugCode;
const Entity = require("../helpers/entity");

class DsaPrivateKey extends Entity {

    get entity_type() {
        return "DsaPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN DSA PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END DSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = DsaPrivateKey;
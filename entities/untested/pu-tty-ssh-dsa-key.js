const Entity = require("../helpers/entity");

class PuTtySshDsaKey extends Entity {

    get entity_type() {
        return "PuTtySshDsaKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)PuTTY-User-Key-File-2: ssh-dss\s*Encryption: none(?:.|\s?)*?Private-MAC:(?=[,.!?\s]|$)/gim;
    }

}

module.exports = PuTtySshDsaKey;
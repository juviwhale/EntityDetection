const Entity = require("../helpers/entity");

class PgpPrivateKeyBlock extends Entity {

    get entity_type() {
        return "PgpPrivateKeyBlock";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN PGP PRIVATE KEY BLOCK-----(?:.|\s)+?-----END PGP PRIVATE KEY BLOCK-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = PgpPrivateKeyBlock;
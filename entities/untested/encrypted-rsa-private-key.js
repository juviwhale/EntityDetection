const Entity = require("../helpers/entity");

class EncryptedRsaPrivateKey extends Entity {

    get entity_type() {
        return "EncryptedRsaPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN RSA PRIVATE KEY-----\s.*,ENCRYPTED(?:.|\s)+?-----END RSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = EncryptedRsaPrivateKey;
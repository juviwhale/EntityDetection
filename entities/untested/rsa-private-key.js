const Entity = require("../helpers/entity");

class RsaPrivateKey extends Entity {

    get entity_type() {
        return "RsaPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN RSA PRIVATE KEY-----(?:[a-zA-Z0-9\+\=\/"']|\s)+?-----END RSA PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = RsaPrivateKey;
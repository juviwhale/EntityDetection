const Entity = require("../helpers/entity");

class SimpleNetworkManagementProtocolObjectIdentifier extends Entity {

    get entity_type() {
        return "SimpleNetworkManagementProtocolObjectIdentifier";
    }

    static potentials_regex() {
        return /(?<=\s|^)(?:\d\.\d\.\d\.\d\.\d\.\d{3}\.\d\.\d\.\d\.\d\.\d\.\d\.\d\.\d\.\d{4}\.\d)|[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z]+[)(0-9]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+\.[a-zA-Z0-9)(]+(?=[,.!?\s]|$)/gm;
    }

}

module.exports = SimpleNetworkManagementProtocolObjectIdentifier;
const Entity = require("../helpers/entity");

class PlainTextPrivateKey extends Entity {

    get entity_type() {
        return "PlainTextPrivateKey";
    }

    static potentials_regex() {
        return /(?<=\s|^)-----BEGIN PRIVATE KEY-----(?:.|\s)+?-----END PRIVATE KEY-----(?=[,.!?\s]|$)/gm;
    }

}

module.exports = PlainTextPrivateKey;
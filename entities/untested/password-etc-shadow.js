const Entity = require("../helpers/entity");

class PasswordEtcShadow extends Entity {

    get entity_type() {
        return "PasswordEtcShadow";
    }

    static potentials_regex() {
        return /(?<=\s|^)[a-zA-Z0-9\-]+:(?:(?:!!?)|(?:\*LOCK\*?)|\*|(?:\*LCK\*?)|(?:\$.*\$.*\$.*?)?):\d*:\d*:\d*:\d*:\d*:\d*:(?=[,.!?\s]|$)/gim;
    }

}

module.exports = PasswordEtcShadow;
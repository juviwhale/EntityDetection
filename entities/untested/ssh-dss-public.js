const Entity = require("../helpers/entity");

class SshDssPublic extends Entity {

    get entity_type() {
        return "SshDssPublic";
    }

    static potentials_regex() {
        return /(?<=\s|^)ssh-dss [0-9A-Za-z+/]+[=]{2}(?=[,.!?\s]|$)/gim;
    }

}

module.exports = SshDssPublic;
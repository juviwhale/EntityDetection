const Entity = require("../helpers/entity");

class FacebookSecret extends Entity {

    get entity_type() {
        return "FacebookSecret";
    }

    static potentials_regex() {
        return /(?<=\s|^)(facebook_secret|FACEBOOK_SECRET|facebook_app_secret|FACEBOOK_APP_SECRET)[a-z_ =\s"'\:]{0,5}[^a-zA-Z0-9][a-f0-9]{32}(?=[",.!?\s]|$)/gim;
    }

}

module.exports = FacebookSecret;
const Entity = require("../helpers/entity");

class Twitter extends Entity {

    get entity_type() {
        return "Twitter";
    }

    format() {
        const url = "twitter.com/" + this.match.replace("@", "");
        this.meta.url = url || null;
        const username = this.match.replace("@", "");
        this.meta.username = username || null;
    }

    validate() {
        let hasValidUserNameFormat = !!((this.meta.username !== null) && this.meta.username.match(/^[a-zA-Z0-9_]{1,15}$/));
        let hasValidTail = this.tailText.match(/^\.?(\s|$)/); // matches @joe. @joe and @joe$
        return !!(super.validate() && hasValidUserNameFormat && hasValidTail);
    }

    static potentials_regex() {
        return /(?<=\s|^)@(\w{1,15})(?=\s|\b)/gim;
    }
}

module.exports = Twitter;
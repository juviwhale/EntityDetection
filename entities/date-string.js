const Entity = require("../helpers/entity");
const chrono = require("chrono-node");

class DateString extends Entity {

    get entity_type() {
        return "Date";
    }

    validate() {
        return !!(super.validate() && this.meta && this.meta.formatted);
    }

    format() {

        function matchInArray(string, expressions) {
            for (var i = 0; i < expressions.length; i++) {
                if (string.match(expressions[i])) {
                    return true;
                }
            }
            return false;
        }

        //todo: 'may,' also invalid. should make this a regex may* (1 char)
        const blacklist = [
            /^may[,.)]?$/,  // the word may appears many times in regular texts, should be blacklisted.
            /figure|section/i      // fixes for string "Section 2078.50"
        ];


        this.match = this.match.trim(); // should always trim, some regex's capture leading space
        const parsedDate = chrono.parseDate(this.match);

        // check for "day / month" (note: '11 / 2012' is not valid in chrono, it doesnt like the spaces)
        const re_month_year = /(?:^|[^\S\r\n])((0?[1-9]|1[0-2])[.\-/ ]{1,3}([0-9]{2,4}))(?!\S)/gim;
        let month_year_match = re_month_year.exec(this.match);

        if (matchInArray(this.match, blacklist)) { // not valid for these cases
            this.isValid = false;
        } else if (matchInArray(this.leadText, blacklist)) { // Section 2078.50
            this.isValid = false;
        } else if (parsedDate && parsedDate.getFullYear() > 1900) { //validate year is > 1900 ?
            this.meta.formatted = parsedDate;
        } else if (month_year_match !== null) {
            //console.log('month_year_match', month_year_match)
            this.meta.formatted = `${month_year_match[3]}-${month_year_match[2]}`;
        } else if (this.match >= 1900 && this.match <= 2199) {
            // for just year matching, ie: 1900
            this.meta.formatted = this.match;
        }
    }

    static potentials_regex() {
        return [
            //# Month, optional Day, optional Year, optional time => March 25 2015 | April 10, 2008 | April 06, 2016 | April 1, 1976 7:30 am
            //# https://regex101.com/r/8z4wK9/9
            /(?:^|[^\S\r\n])(?:\b(?:Mon|Tue(s)?|Wed(nes)?|Thu(rs)?|Fri|Sat(ur)?|Sun)(?:day)?\b)?[ ,]{0,2}(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)[:\-,/ ]{0,3}(\d{1,2})? ?(?:st|nd|rd|th)?[- ,/]{0,3}(\d{2,4})?[,. ]{0,2}(((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?) ?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)?[,.]?(?!\S)/gim

            //# Day/month name/ year (optinal)/ time => 3-Aug-06 | 3-August-2006 | 11 April, 1999 | 3-August-2006 6:55:30 pm
            //# https://regex101.com/r/ZS3BGj/
            , /(?:^|[^\S\r\n])((?:[0]?[0-9]|[12][0-9]|3[01]) ?(?:st|nd|rd|th)?)?[-, ]{0,3}(?:of )?(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)[:\-, ]{0,2}(\d{2,4})?[ ]?(((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?) ?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)?[,.]?(?!\S)/gim

            //# time => 6:55:30 pm | 6:55 | 6:55 am
            //# https://regex101.com/r/0r0ukg/2
            , /(?:^|[^\S\r\n])( ?((0?[0-9]|1[0-2]):([0-5][0-9])(?::([0-5][0-9]))?)\s?([ap]\.?m\.?)?|([0-1][0-9]|[2][0-3]):([0-5][0-9])(?::([0-5][0-9]))?)(?![\b])/gmi


            //# timestamp => 2013-04-05 17:59:59 | 2009-06-15T13:45:30
            //# https://regex101.com/r/qzIlH9/2
            , /(^|[^\S\r\n])((((19|20)([2468][048]|[13579][26]|0[48])|2000)-02-29|((19|20)[0-9]{2}-(0[4678]|1[02])-(0[1-9]|[12][0-9]|30)|(19|20)[0-9]{2}-(0[1359]|11)-(0[1-9]|[12][0-9]|3[01])|(19|20)[0-9]{2}-02-(0[1-9]|1[0-9]|2[0-8])))[\sT]([01][0-9]|2[0-3]):([012345][0-9]):([012345][0-9]))\b/gm

            //# day/month/year (optional) => 10/09/2017 | 10-09-2017 | 10/9/2017 | 10/9/17 | 10 - 9 - 17
            //# https://regex101.com/r/0bUssw/2
            , /(?:^|[^\S\r\n])([0-9]?[0-9])[.\-/ ]{1,3}([0-3]?[0-9])([.\-/ ]{1,3}[0-9]{2,4}[,.]?)?(?!\S)/gim

            //# month/year => 04 / 2012 | 04/2012 | 9 / 2022
            //# https://regex101.com/r/2FpKcM/3
            , /(?:^|[^\S\r\n])((0?[1-9]|1[0-2])[.\-/ ]{1,3}([0-9]{2,4}))(?!\S)/gim

            //# year >=1900 <=2199
            //# https://regex101.com/r/ELcgqp/8
            , /(?:^|[^\S\r\n])(?:19|2[01])\d{2}?\b/gm

        ];
    }

    // Custom name DateString => Date since Date is a reserved name
    static type_name() {
        return "Date";
    }

}

module.exports = DateString;
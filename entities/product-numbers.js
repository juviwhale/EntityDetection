const Entity = require("../helpers/entity");
const ISBNValidator = require("isbn").ISBN;
const Barcoder = require('barcoder');

// VAT (Value-Added Tax)
class ProductNumbers extends Entity {

    get entity_type() {
        return "ProductNumbers";
    }

    validate() {
        return !!(super.validate() && this.meta && Object.keys(this.meta).length > 0);
    }

    format() {
        const ASIN_re = /(?<=\s|^)(?=.*\d)[0-9A-Z]{10}\b/g;

        let isbnValidated = ISBNValidator.parse(this.match);
        if (isbnValidated && isbnValidated.codes && isbnValidated.codes.isValid) { // ISBN
            this.meta.isIsbn10 = isbnValidated.codes.isIsbn10;
            this.meta.isIsbn13 = isbnValidated.codes.isIsbn13;
            this.meta.isbn10 = isbnValidated.codes.isbn10;
            this.meta.isbn10h = isbnValidated.codes.isbn10h;
            this.meta.isbn13 = isbnValidated.codes.isbn13;
            this.meta.isbn13h = isbnValidated.codes.isbn13h;

        } else if (Barcoder.validate(this.match.replace('EAN','').replace('UPC',''))) { // EAN, UPC?
            this.meta.barcode = this.match;

        } else if (ASIN_re.test(this.match)) { // ASIN
            this.meta.ASIN = this.match;
        }
    }

    static potentials_regex() {
        // https://regex101.com/r/ss3jPq/2
        return [
            // ISBN
            /(?<=\s|^)(?:[0-9]{3}-)?[0-9]{1,5}-[0-9]{1,7}-[0-9]{1,6}-[0-9]\b/ig,

            // UPC 12
            /(?<=\s|^)(?:UPC)?(?:\d{12})\b/ig,

            // EAN8, EAN12, EAN13, EAN14, EAN18, GTIN12, GTIN13 and GTIN14, UPC 12?
            /(?<=\s|^)(?:EAN[_]?)?(?:\d{8}|\d{12}|\d{13}|\d{14}|\d{18})\b/ig,

            // ASIN (make sure it has at least 1 digit)
            /(?<=\s|^)(?=.*\d)[0-9A-Z]{10}\b/g,
        ]
    }

}

module.exports = ProductNumbers;
const Entity = require("../helpers/entity");

class IndividualTaxpayerIdentificationNumber extends Entity {

    get entity_type() {
        return "IndividualTaxpayerIdentificationNumber";
    }

    static potentials_regex() {
        return /(?<=^|[>\s])(9\d{2})([ -]?)([7]\d|8[0-8])([ -]?)(\d{4})(?=[<,.!?\s]|$)/ig;
    }

}

module.exports = IndividualTaxpayerIdentificationNumber;
const Entity = require("../helpers/entity");

class UkZipCodes extends Entity {

    get entity_type() {
        return "UkZipCodes";
    }

    static potentials_regex() {
        return /(?<=\s|^)((?:(?:gir)|(?:[a-pr-uwyz])(?:(?:[0-9](?:[a-hjkpstuw]|[0-9])?)|(?:[a-hk-y][0-9](?:[0-9]|[abehmnprv-y])?)))) ?([0-9][abd-hjlnp-uw-z]{2})(?=[,.!?\s]|$)/gim;
    }

}

module.exports = UkZipCodes;
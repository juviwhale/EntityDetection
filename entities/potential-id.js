const Entity = require("../helpers/entity");

class PotentialID extends Entity {

    get entity_type() {
        return "PotentialID";
    }

    validate() {
        this.isValid = this.match.length > 4;

        let blackListLeadText = [
            /figure|section/i, // 'Section 2079.13' is this a valid ID?
        ];
        blackListLeadText.forEach(regex => {
            if (regex.exec(this.leadText)) {
                return this.isValid = false;
            }
        });

        // require labels on numbers   e.g. AccountID: 666066666
        if (this.match.match(/^[1-9]\d{4,10}$/gi)) {
            this.isValid = !!this.label;
        }
        return this.isValid;
    }

    static potentials_regex() {
        return [
            //# Alphanumeric string with at least 1 number and 1 char e.g AF234DSDFED222 AND RBCPDA0001-123456789-01-000001-1-0001
            // https://regex101.com/r/58F1iQ/1
            // https://regex101.com/r/58F1iQ/3
            /(?:^|\b)(?:[0-9]+[a-z]|[a-z]+[0-9])(\w|-|:| - |_)*/gi,

            //# Number Dashed e.g. N-23423
            /(?:^|\b)[A-Z]+-\d+\b/gi,

            //# https://regex101.com/r/718YVE/1
            //# Numbers with dashes e.g. 234-234-2344-432 and 02782 - 5094431
            /(?:^|\b)\d+ ?- ?\d+(-| - |\d)+\d/gi,

            //# Numbers (none zero starting and more than four digits, we filter these out if not label if found. e.g. AccountID: 666666666 is valid )
            /(?: )([1-9]\d{4,})(?:\s|$)/gi,

            //# e.g. 3TAB-2358, 234ASDF, asdf234, ASDF2--34ASD
            // https://regex101.com/r/5fOnMv/2
            /(?:[\.\:\_\-A-Za-z]+[0-9]|[0-9]+[\.\:\_\-A-Za-z])[\.\:\_\-A-Za-z0-9]*/g,

            //# https://regex101.com/r/S3ddMx/2
            //# Zero prefixed numbers e.g. 02343 and 0002342321
            /(?:^|\b)0+\d{2,}/g,

            //# Numbers 12 digits and larger;;
            /(?:^|\s)\d{12,}/g
        ];
    }
}

module.exports = PotentialID;
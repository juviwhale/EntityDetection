const Entity = require("../helpers/entity");
const jsvat = require("jsvat");

// VAT (Value-Added Tax)
class VAT extends Entity {

    get entity_type() {
        return "VAT";
    }

    format() {
        let jsValidated = jsvat.checkVAT(this.match);
        this.isValid = jsValidated.isValid;
        if (jsValidated.country) {
            this.meta.country = jsValidated.country;
        }
    }

    static potentials_regex() {
        let matches = [
            "ATU[0-9]{8}", // Austria
            "BE0[0-9]{9}", // Belgium
            "BG[0-9]{9,10}", // Bulgaria
            "CY[0-9]{8}L", // Cyprus
            "CZ[0-9]{8,10}", // Czech Republic
            "DE[0-9]{9}", // Germany
            "DK[0-9]{8}", // Denmark
            "EE[0-9]{9}", // Estonia
            "EL|GR[0-9]{9}", // Greece
            "ES[0-9A-Z][0-9]{7}[0-9A-Z]", // Spain
            "FI[0-9]{8}", // Finland
            "FR[0-9A-Z]{2}[0-9]{9}", // France
            "GB[0-9]{9}([0-9]{3}|[A-Z]{2}[0-9]{3})", // United Kingdom
            "HU[0-9]{8}", // Hungary
            "IE[0-9]S[0-9]{5}L", // Ireland
            "IT[0-9]{11}", // Italy
            "LT([0-9]{9}|[0-9]{12})", // Lithuania
            "LU[0-9]{8}", // Luxembourg
            "LV[0-9]{11}", // Latvia
            "MT[0-9]{8}", // Malta
            "NL[0-9]{9}B[0-9]{2}", // Netherlands
            "PL[0-9]{10}", // Poland
            "PT[0-9]{9}", // Portugal
            "RO[0-9]{2,10}", // Romania
            "SE[0-9]{12}", // Sweden
            "SI[0-9]{8}", // Slovenia
            "SK[0-9]{10}" // Slovakia
        ];
        return new RegExp("(?:^|\\s)" + matches.join("|") + "\\b", "gi");
    }

}

module.exports = VAT;
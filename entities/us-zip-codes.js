const Entity = require("../helpers/entity");
const zips = require('zips');

class UsZipCodes extends Entity {

    get entity_type() {
        return "UsZipCodes";
    }

    format() {
        this.meta.place = zips.getByZipCode(this.match.match(/^(\d{5})\b/)[1]); // get first 5 digits for location, ignore secondary digits, 12345-1234
    }

    validate() {
        return (super.validate() && this.meta.place !== null);
    }


    static potentials_regex() {
        // https://regex101.com/r/hKEBDK/6
        return /(?<=\s|^)(\d\d)(\d\d\d)(?:[-\s]{1}\d{4})?(?=[,.!?\s]{1,4}|$)/gm;
    }

}

module.exports = UsZipCodes;
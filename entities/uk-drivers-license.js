const Entity = require("../helpers/entity");

class UKDriversLicense extends Entity {

    get entity_type() {
        return "UKDriversLicense";
    }

    static potentials_regex() {
        return /(?<=\s|^)[A-Z]{5}\d{6}[A-Z]{2}\d{1}[A-Z]{2}(?=[,.!?]?\s|$)/gm;
    }

}

module.exports = UKDriversLicense;
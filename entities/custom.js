const Entity = require("../helpers/entity");
const utils = require('../helpers/utils.js');

class Custom extends Entity {

	get entity_type() {
		return "Custom";
	}

	format() {
		if (this.validate()) {
			this.meta.value = this.match;
		}
	}

	validate() {
		let isValid = true;

		// Insert possible validation logic here..

		return super.validate() && isValid;
	}

	static potentials_regex(options) {
		const regexes = [];

		if (options) {

			options.forEach(custom => {
				if (custom.keywords) {
			
					let keywords = custom.keywords.split(',').map(keyword => keyword.trim()).join('|');
					regexes.push(new RegExp(keywords, 'gm'));
			
				} else if (custom.regexes) {
			
					custom.regexes.forEach(regex => {
						// regex string needs to be escaped: "/foo(\\w+)/gmi"
						regexes.push(utils.getRegFromString_v2(regex.regex));
					});
			
				}
			});

			return regexes;
		}

		return null;
	}
}



module.exports = Custom;
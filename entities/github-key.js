const Entity = require("../helpers/entity");

class GithubKey extends Entity {

    get entity_type() {
        return "GithubKey";
    }

    format() {
        const re = /(?<=\s|^)(?:GITHUB_SECRET|GITHUB_KEY|GITHUB_TOKEN|GITHUB_API_KEY)(?:[\s="':]{0,4})(\w{20,40})(?=["';<,.!?\s]|$)/im
        if (this.match.match(re)) {
            this.meta.key = this.match.match(re)[1]
        }
    }

    static potentials_regex() {
        //https://regex101.com/r/DjicyK/4
        return /(?<=\s|^)(?:GITHUB_SECRET|GITHUB_KEY|GITHUB_TOKEN|GITHUB_API_KEY)(?:[\s="':]{0,4})(\w{20,40})(?=["';<,.!?\s]|$)/gim;
    }

}

module.exports = GithubKey;
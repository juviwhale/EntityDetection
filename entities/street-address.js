const Entity = require("../helpers/entity");
const parser = require("parse-address");
const AddressParser = require("../helpers/line-parsing").AddressParser;

class StreetAddress extends Entity {

    get entity_type() {
        return "StreetAddress";
    }

    format() {
        const formatted = parser.parseLocation(this.match);

        //const words = this.match.split(" ");
        //console.log('formatted words', formatted, words);
        // what was this clause? (Object.keys(formatted).length === words.length)
        if (formatted) {
            this.isValid = true;

            // for (let key in formatted) {
            //     if (key === "type")
            //         this["streetType"] = formatted[key];
            //     else
            //         this[key] = formatted[key];
            // }
        } else {
            //console.log("BROKE HERE", Object.keys(formatted));
            this.isValid = false;
        }
    }

    /**
     * 
     * @param {Text to match against} text 
     */

    static getPotentialsForText(text) {
        // console.log("CHECKING TEXT:", text);
        const potentials = [];

        const addresses = AddressParser.fromText(text);
        let data;

        potentials.push(...addresses.addresses.map(address => {
            data = {
                text: text,
                match: address.text,
                meta: address.address,
                firstIndex: address.firstIndex,
                lastIndex: address.lastIndex
            };

            return new this(data);
        }));

        return potentials;
    }
}

module.exports = StreetAddress;
const Entity = require("../helpers/entity");

class UKPassportNumber extends Entity {

    get entity_type() {
        return "UKPassportNumber";
    }

    static potentials_regex() {
        return /(?<=\s|^)\d{10}GB[RP]\d{7}[UMF]{1}\d{9}(?=[,.!?]?\s|$)/igm;
    }

}

module.exports = UKPassportNumber;
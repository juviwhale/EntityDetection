const Entity = require("../helpers/entity");

// Employer Identification Number (EIN)
class EIN extends Entity {

    get entity_type() {
        return "EIN";
    }

    static potentials_regex() {
        return /(?<=\s|^)[1-9]\d?-\d{7}(?=[,.!?\s]|$)/gi;
    }

}

module.exports = EIN;
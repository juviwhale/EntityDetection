const Entity = require("../helpers/entity");

class SocialSecurityNumber extends Entity {

    get entity_type() {
        return "SocialSecurityNumber";
    }

    format() {
        this.meta.SSN = this.match || null;
    }

    validate() {
        return (super.validate() && this.meta.SSN !== null);
    }

    static potentials_regex() {
        // https://regex101.com/r/kWcVTb/3
        return /(?<=\s|^)(?!\b(\d)\1+-(\d)\1+-(\d)\1+\b)(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}[-:](?!00)\d{2}[-:](?!0{4})\d{4}(?=[,.!?]?\s|$)/gim;
    }
}

module.exports = SocialSecurityNumber;
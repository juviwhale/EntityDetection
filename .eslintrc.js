module.exports = {
    "env": {
        "es6": true,
        "node": true
    },
    "extends": "eslint:recommended",
    "rules": {
        "indent": [2, 4, { "VariableDeclarator": 4 }],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "no-control-regex": 0,
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ]
    }
};
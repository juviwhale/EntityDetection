const parser = require("parse-address");
const LOOKUP_CONSTANTS = require("./lookup-constants");

class GroupedAddressLines {
    constructor(line, isNotAddress = false) {
        this.lines = [];
        if (line) {
            this.append(line);
        }
        this.isNotAddress = isNotAddress;
    }

    get firstIndex() {
        return (this.lines[0]) ? this.lines[0].firstIndex : 0;
    }

    get lastIndex() {
        return (this.lines[0]) ? this.lines[this.lines.length - 1].lastIndex : 0;
    }

    get address() {
        return parser.parseLocation(this.text);
    }

    // Text of lines merged
    get text() {
        return this.lines.map((line) => {
            return line.text;
        }).join(" ");
    }

    // Text of lines merged with line breaks
    get textWithLineBreaks() {
        return this.lines.map((line) => {
            return line.text;
        }).join("\n");
    }

    get hasValidAddress() {
        // ADD SOME VALIDATIONS HERE
        return this.isPotentialAddress;
    }

    // Does the group have 2 or more potential types
    get isPotentialAddress() {
        const wordCount = this.text.split(" ").length;
        const addressItems = (this.address) ? Object.keys(this.address).length : 0;
        //return this.potential_types.length > 0 && (addressItems > 0 && (addressItems > 5 || wordCount === addressItems));
        return !this.isNotAddress && this.potential_types.length >= 2 && addressItems > 2;
    }

    // Unique set of potential_types contained in sub lines
    get potential_types() {
        return Array.from(new Set([].concat.apply([], this.lines.map((line) => {
            return line.potential_types;
        }))));
    }

    get hasLines() {
        return this.lines.length > 0;
    }

    canBeAppendedWithLine(line) {
        // ONLY ALLOW SPECIFIC TYPES TO BE DUPLICATE
        let sharedTypes = line.potential_types.filter(val => this.potential_types.includes(val));
        const ALLOWED_DUP_TYPES = [
            "zipcode", // NOTE: we need zipcode for LA addresses like 43253 Main St.
        ];
        for (var type of ALLOWED_DUP_TYPES) {
            sharedTypes = sharedTypes.filter(e => e !== type);
        }
        return sharedTypes;
    }

    append(line) {
        this.lines.push(line);
    }

    export () {
        return {
            "text": this.textWithLineBreaks,
            "address": this.address,
            "firstIndex": this.firstIndex,
            "lastIndex": this.lastIndex
        };
    }
}

class TextLine {
    constructor(text, firstIndex = 0, lastIndex = 0, fullText = "") {
        this.text = text;
        this.potential_types = this.getPotential_types();
        this.firstIndex = firstIndex;
        this.lastIndex = lastIndex;
        this.isValid = this.text == fullText.slice(this.firstIndex, this.lastIndex);
    }

    // Is there a zip code in the line
    get potentialZipCode() {
        const match_case = /(?:^|\s)\b\d{5}(?:-\d{4})?(?:$|\s)/igm;
        return this.evalRegex(this.text, match_case);
    }

    // Is there a state abbreviations in the line
    get potentialStateAbbr() {
        const match_case = new RegExp("(?:^|\\s|\\,)(" + LOOKUP_CONSTANTS.state_abbreviations.join("|") + ")(?:\\s|\\.|$)", "gm");
        return this.evalRegex(this.text, match_case);
    }

    // Is there a state name in the line
    get potentialStateName() {
        const match_case = new RegExp("(?:^|\\s)(" + LOOKUP_CONSTANTS.state_names.join("|") + ")(?:$|\\s)", "igm");
        return this.evalRegex(this.text, match_case);
    }

    // Has something like 34 Elm
    get numberedProperWord() {
        const match_case = /(?:^|\s)\d+ [A-Z]\w{2,}/gm;
        return this.evalRegex(this.text, match_case);
    }

    // FIXME: This is very loose and offers little value
    get potentialCityName() {
        //var match_case = /(?:[A-Z][a-z.-]+[ ]?)+/igm
        const match_case = new RegExp("(?:[A-Z][a-z.-]+[ ]?)+", "igm");
        return this.evalRegex(this.text, match_case);
    }

    get potentialStreetType() {
        // https://regex101.com/r/oT1akp/1
        const match_case = new RegExp("[a-zA-Z] +(" + LOOKUP_CONSTANTS.street_types_matches.join("|") + ")(?:\\s|\\.|,|$)", "igm");
        return this.evalRegex(this.text, match_case);
    }

    get potentialPOBox() {
        const match_case = new RegExp("(\\bP(ost|ostal)?([\\/ \\.-]*(O|0)(ffice)?)?([ \\.-]*(b|Box))?[\\. ]?[ :#]*\\d+[,. ]?)(\\w+[ ,-]*)*(?!\\S)|\\b(P[. ]*O?[\\. ]*(B|Box)?[., ]*(?!\\S))", "igm");
        return this.evalRegex(this.text, match_case);
    }

    get potentialCountryAbbr() {
        const match_case = new RegExp("(?:^|\\s)(" + LOOKUP_CONSTANTS.country_abbreviations.join("|") + ")(?:\\s|\\.|$)", "g");
        return this.evalRegex(this.text, match_case);
    }

    get has_potential_types() {
        return this.potential_types.length > 0;
    }

    get is_not_to_long() {
        return this.text.length < 70;
    }

    // Get array of short names of potential types
    getPotential_types() {
        const typesWhitelisted = {
            "potentialZipCode": "zipcode",
            //"potentialCityName": "city", // no value is added by this check
            "potentialStateAbbr": "state-abbr",
            "numberedProperWord": "street-number",
            "potentialStateName": "state-name",
            "potentialStreetType": "street-type",
            "potentialPOBox": "po-box",
            "potentialCountryAbbr": "country-abbr",
        };

        const types = [];

        for (var key in typesWhitelisted) {
            if (eval("this." + key)) {
                types.push(typesWhitelisted[key]);
            }
        }

        return types;
    }

    // Shared Regex evaluator, return matches so index positions can be found
    evalRegex(str, regex) {
        const matches = [];
        let match;

        while ((match = regex.exec(str)) !== null) {
            matches.push({
                match: match[0].replace(/(\r\n|\n|\r)/gm, "").trim(),
                firstIndex: match.index,
                lastIndex: regex.lastIndex
            });
        }

        return (matches.length > 0) ? matches : null;
    }

}

class AddressParser {
    constructor(lines) {
        this.lines = lines;
        this.addresses = [];

        this.lineGroups = [];
        let lineGroup = null;
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i];
            // console.log('ON line number:', i);
            // console.log(line);
            // console.log('  LINE GROUP:', lineGroup)

            if (line.is_not_to_long && line.has_potential_types) {
                // console.log("  Has potential types");
                if (lineGroup === null) {
                    lineGroup = new GroupedAddressLines(line);
                } else if (lineGroup.canBeAppendedWithLine(line)) {
                    lineGroup.append(line);
                } else {
                    this.lineGroups.push(lineGroup);
                    lineGroup = new GroupedAddressLines(line);
                }
            } else {
                // console.log("  DOES NOT Have potential types");
                // Append current line group if it exists
                if (lineGroup) {
                    this.lineGroups.push(lineGroup);
                }
                // Create and append new line group
                this.lineGroups.push(new GroupedAddressLines(line, true));
                lineGroup = null;
            }
        }
        if (lineGroup !== null) {
            this.lineGroups.push(lineGroup);
        }

        // Collect line groups with addresses
        for (let i = 0; i < this.lineGroups.length; i++) {
            var lGroup = this.lineGroups[i];
            if (lGroup.hasValidAddress) {
                this.addresses.push(lGroup.export());
            }
        }
    }

    debug() {
        /* eslint-disable */
        console.log("\n*********\n DEBUG \n*********");
        console.log("\n*** LINES ***");
        for (var textLine of this.lines) {
            console.log(textLine);
        }
        console.log("\n*** lineGroups ***");
        for (var lineGroup of this.lineGroups) {
            console.log(lineGroup);
            console.log("  IsPotentialAddress:", lineGroup.isPotentialAddress);
            console.log("  Types:", lineGroup.potential_types);
        }
        console.log("\n*** Addresses ***");
        for (var address of this.addresses) {
            console.log(address);
        }
        console.log("Kansas City MO 64106-3686");
        console.log(parser.parseLocation("Kansas City MO 64106-3686"));
        console.log("\n********* END DEBUG *********");
        /* eslint-enable */
    }

    static fromText(text) {
        let lines = [];
        let textLines = text.split("\n");
        var firstIndex = 0;
        var lastIndex;

        for (var textLine of textLines) {
            lastIndex = firstIndex + textLine.length;
            if (textLine.replace("\n", "").trim() !== "") {
                var line = new TextLine(textLine, firstIndex, lastIndex, text);
                if (line.isValid) {
                    lines.push(line);
                } else {
                    console.warn("WARNING: LINE MATCH MISMATCH", this); // eslint-disable-line
                }
            }
            firstIndex = lastIndex + 1;
        }
        return new this(lines);
    }
}

exports.TextLine = TextLine;
exports.GroupedAddressLines = GroupedAddressLines;
exports.AddressParser = AddressParser;
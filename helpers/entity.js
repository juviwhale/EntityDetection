class Entity {
    constructor(data) {
        if (data.match) this.match = data.match; //data.match.replace(/^[a-zA-Z0-9!@#$&()\\-`:.+,/\"]*$/g, '')
        else throw new Error("Entity must have a match in order to be initialized");

        this.firstIndex = data.firstIndex || 0;
        this.lastIndex = data.lastIndex || 0;

        // Normalize index for whitespace
        if (this.firstIndex != null && this.match && this.match.match(/^\s+/)) {
            let numberOfLeadWhiteCharacters = this.match.match(/^\s+/)[0].length;
            this.firstIndex = this.firstIndex + numberOfLeadWhiteCharacters;
        }
        if (this.lastIndex && this.match && this.match.match(/\s+$/)) {
            let numberOfTailWhiteCharacters = this.match.match(/\s+$/)[0].length;
            this.lastIndex = this.lastIndex - numberOfTailWhiteCharacters;
        }
        // Trim up match
        if (this.match && (this.match.match(/^\s+/) || this.match.match(/\s+$/))) {
            this.match = this.match.trim();
        }

        const lead_tail_size = 10;
        
        // Set Lead Text
        if (this.firstIndex != null && data.text) {
            let leadIndex = Math.max(this.firstIndex - lead_tail_size, 0);
            let leadLength = this.firstIndex - leadIndex;
            this.leadText = data.text.substr(leadIndex, leadLength);
        } else {
            this.leadText = "";
        }

        // Set Tail Text
        this.tailText = this.lastIndex && data.text ? data.text.substr(this.lastIndex, lead_tail_size) : "";

        // Set Ranged Text Match
        if (this.firstIndex != null && this.lastIndex && data.text) {
            this._rangedText = data.text.slice(this.firstIndex, this.lastIndex);
        }

        // Set Lead Label
        if (this.firstIndex != null && data.text) {
            //https://regex101.com/r/33USDu/2
            let textBeforeMatch = data.text.substr(0, data.firstIndex);
            //this.textBeforeMatch = textBeforeMatch;
            let labelMatch = textBeforeMatch.match(/([\w -]+)[:#]\s*$/m);
            if (labelMatch) {
                this.label = labelMatch[1].trim();
            }
        }

        this.meta = data.meta || {}; // Need some default validation
        this.type = this.entity_type;

        this.format();

        if (this.potential)
            this.isPotential = true;

        this.isValid = this.validate();
    }

    // required to be overridden by child class
    get entity_type() {
        throw new Error("Entity must have defined type!");
    }

    // return the match in the context of the text. This can be used to better validate.
    get in_context() {
        return this.leadText + this.match + this.tailText;
    }

    // Is this item a potential
    get potential() {
        return false;
    }

    validate() {
        let errors = [];
        if (this.isValid === undefined || this.isValid === null) {
            this.isValid = true;
        }

        // Verify index have correct gap
        if (this.isValid && this.lastIndex && this.firstIndex != null) {
            let rangedTextMatches = this._rangedText && (this._rangedText == this.match);
            let lengthMatchesIndex = this.match.length == this.lastIndex - this.firstIndex;
            this.isValid = lengthMatchesIndex && rangedTextMatches;

            if (!lengthMatchesIndex) {
                console.warn("WARNING: ENTITY HAS LENGTH MISMATCH", this); // eslint-disable-line
                errors.push("LENGTH_MISMATCH");
            }
            if (!rangedTextMatches) {
                console.warn("WARNING: ENTITY HAS TEXT RANGE MISMATCH", this); // eslint-disable-line
                errors.push("TEXT_RANGE_MISMATCH");
            }

        }
        if (errors.length > 0) {
            this.errors = errors;
        }
        return this.isValid;
    }

    // Format results if needed before export
    format() { }

    export() {
        let exportObj = JSON.parse(JSON.stringify(this));
        // remove private keys
        Object.keys(exportObj).forEach(key => {
            if (key.match(/^_/))
                delete exportObj[key];
        });
        return exportObj;
    }

    // HELPERS

    /**
     * Does this entity contain the bounds of another entity or have the same bounds
     *   *---THIS---*
     *   *-YES-*
     *      *-YES-*
     *   *---YES----*
     *           *-NO-*
     * *-NO-*
     *
     * @param {Entity Object} entity
     */
    contains(entity) {
        return (
            this.firstIndex <= entity.firstIndex &&
            entity.lastIndex <= this.lastIndex
        );
    }

    /**
     * Does this entity intersect the bounds of another entity
     *   *---THIS---*
     * *-YES-*
     *      *-YES-*
     *   *---YES----*
     *          *-YES-*
     *
     * @param {Entity Object} entity
     */
    intersects(entity) {
        return (
            ((this.firstIndex <= entity.firstIndex) && (entity.firstIndex < this.lastIndex)) ||
            ((this.firstIndex < entity.lastIndex) && (entity.lastIndex <= this.lastIndex))
        );
    }


    // STATIC regex(s) for find potentials with
    static potentials_regex() {
        return null;
    }

    // STATIC custom naming if subclassed
    static type_name() {
        return this.name;
    }

    /**
     *
     * @param {Text to match against} text
     */

    static getPotentialsForText(text, custom) {
        const potentials = [];
        const regex = this.potentials_regex(custom);

        if (Array.isArray(regex))
            potentials.push(...this.getPotentialsForTextAndMultipleRegex(text, regex));
        else if (regex)
            potentials.push(...this.getPotentialsForTextAndRegex(text, regex));

        return potentials;
    }

    /**
     *
     * @param {Text to match against} text
     * @param {Single REGEX to match against} regex
     */

    static getPotentialsForTextAndRegex(text, regex) {
        const potentials = [];
        let match;

        regex.lastIndex = 0; // reset pointer before the loop
        
        while ((match = regex.exec(text)) !== null) {

            if (match[0].split("\n").length > 1)
                continue;

            if (match.index === 0 && regex.lastIndex === 0)
                break;

            const entity = new this({
                text: text,
                match: match[0],
                firstIndex: match.index,
                lastIndex: regex.lastIndex
            });

            if (entity && entity.isValid)
                potentials.push(entity);
        }

        return potentials;
    }

    /**
     *
     * @param {Text to match against} text
     * @param {ARRAY of REGEX to match against} regexes
     */

    static getPotentialsForTextAndMultipleRegex(text, regexes) {
        let potentials = [];

        regexes.forEach(regex => {
            potentials.push(...this.getPotentialsForTextAndRegex(text, regex));
        });

        //console.log(potentials)

        /*
         4              50
          5    10
             6      15
               10   15
        */
        potentials.forEach((existingPotential, existingPotentialIndex) => {
            potentials.forEach((potential, index) => {
                var existingEntity = potentials[existingPotentialIndex];
                var potentialEntity = potentials[index];

                if (index != existingPotentialIndex && existingEntity && potentialEntity) {
                    if (existingEntity.contains(potentialEntity)) {
                        delete potentials[index];
                    } else if (existingEntity.intersects(potentialEntity)) {
                        delete potentials[existingPotentialIndex];
                    }
                }
            });
        });

        potentials = potentials.filter(x => !!x); // comact array, remove empty array elements
        return potentials;
    }
}

module.exports = Entity;
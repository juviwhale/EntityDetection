class EntityGrouping {
    constructor(entity) {
        // Entities contains all the overlapping entity objects
        this.entities = [];
        this.child_types = [];
        if (entity) {
            // SET ROOT PROPERTIES FROM CONTAINER ENTITY (meaning it has the largest bounds)
            this.entities = [entity];
            // Set match
            this.match = entity.match;
            // Set type
            this.type = entity.type; // LEGACY
            // Set types
            this.types = [entity.type];
            // Set firstIndex
            this.firstIndex = entity.firstIndex;
            // Set lastIndex
            this.lastIndex = entity.lastIndex;
            // Set leadText
            this.leadText = entity.leadText;
            // Set tailText
            this.tailText = entity.tailText;
            // Set tailText
            this.tailText = entity.tailText;
            // Set label
            if (entity.label)
                this.label = entity.label;
            // Set meta
            this.meta = {};
            this.meta[entity.type] = entity.meta;
            // Set valid to true as a default
            this.isValid = true;
        }
    }

    add(entity) {
        // Add entity to array container
        this.entities.push(entity);
        // If enity matches the start/stop of the main entity then add to types
        if (entity.firstIndex == this.firstIndex && entity.lastIndex == this.lastIndex) {
            // Add to types array
            this.types.push(entity.type);
            // Add to meta
            this.meta[entity.type] = entity.meta;
        } else if (!this.child_types.includes(entity.type)) {
            this.child_types.push(entity.type);
        }
    }

    export () {
        return JSON.parse(JSON.stringify(this));
    }
}

module.exports = EntityGrouping;